Welcome
-------

.. image:: https://joss.theoj.org/papers/10.21105/joss.02831/status.svg
   :target: https://doi.org/10.21105/joss.02831

**Bvpy** is a python library, based on `FEniCS <https://fenicsproject.org/>`_ and `GMSH <http://gmsh.info/>`_, to easily implement and study numerically Boundary Value Problems (BVPs) as well as Initial Boundary Value Problems (IBVPs) through the Finite Element Method (FEM).

Initially built up in the context of developmental biology and morphomechanics, **Bvpy** proposes an intuitive API as close as possible to the formal definition of BVPs. Its purpose is to enable users to quickly and efficiently estimate the behavior of a wide variety of fields (scalars, vectors, tensors) on biologically relevant structures (Riemannian and non-Rieamannian manifolds), inspired by biophysical and biochemical processes (morphogene patterning, active matter mechanics, diffusion-reaction processes, active transports...).

Despite this biological motivation, the **Bvpy** library has been implemented in an *agnostic* manner that makes it suitable for many other scientific context.


.. sidebar:: **Bvpy**

  :Lead Development: Florian Gacon

  :Coordination: Olivier Ali

  :Active team: Inria project team `Mosaic <https://team.inria.fr/mosaic/>`_

  :Institutes: `Inria <http://www.inria.fr>`_

  :Language: Python

  :Supported OS: Linux, MacOS

  :Licence: `LGPL`

  :Funding: Inria ADT Gnomon (Christophe Godin)


Documentation
-------------

The documentation can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/>`_.

A quick introduction to boundary-value problems and Initial-Boundary-value problems, as well as a the general philosophy behind the **Bvpy** library development can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/general_introduction.html>`_.

A detailed description of the main classes and models of the library can be found `here <https://mosaic.gitlabpages.inria.fr/bvpy/library_description.html>`_.

Some tutorials explaining basic manipulations are gathered `here <https://mosaic.gitlabpages.inria.fr/bvpy/tutorials.html>`_.


Requirements
------------

* Python 3.7+
* FEniCS
* GMSH


Installation
------------

You will need conda in order to install ``bvpy``, you can download it  `here <https://docs.conda.io/en/latest/miniconda.html>`_.

- From sources:

.. code-block:: bash

  git clone https://gitlab.inria.fr/mosaic/bvpy.git
  cd bvpy
  conda env create -f conda/env.yaml -n bvpy-dev
  conda activate bvpy-dev
  python setup.py develop --prefix=$CONDA_PREFIX


- From anaconda:

.. code-block:: bash

	conda install -c conda-forge bvpy


- From docker

.. code-block:: bash

	docker pull registry.gitlab.inria.fr/mosaic/bvpy:<TAG>

Where <TAG> is the version of bvpy you want. You can find all the available version on the `repository <https://gitlab.inria.fr/mosaic/bvpy/container_registry/>`_.

To run the docker container and launch jupyter use:

.. code-block:: bash

  docker run -it -p 8888:8888 registry.gitlab.inria.fr/mosaic/bvpy:<TAG>
  jupyter notebook --ip 0.0.0.0 --no-browser --allow-root --port 8888

And use the URL on the terminal to open the tutorials.

Test
----

If you install ``bvpy`` from anaconda install ``pytest`` and ``pytest-cov`` inside your environment:

.. code-block:: bash

	conda install -c conda-forge pytest pytest-cov

And then run at the root of the project:

.. code-block:: bash

	pytest -v

Support
-------

If you encounter an error or need help, please `raise an issue <https://gitlab.com/oali/bvpy/-/issues>`_.

Contributing
------------

Bvpy is a is a collaborative project and contributions are welcome. If you want to contribute, please contact the `coordinator <mailto:olivier.ali@inria.fr>`_ prior to any `merge request <https://gitlab.com/oali/bvpy/-/merge_requests>`_ and
check the `gitlab merge request guidelines <https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html>`_ if needed.

Citation
--------

If you are using Bvpy in a published work, please use this bibtex entry as reference:

.. code-block::

  @article{Gacon2021,
  doi = {10.21105/joss.02831},
  url = {https://doi.org/10.21105/joss.02831},
  year = {2021},
  publisher = {The Open Journal},
  volume = {6},
  number = {59},
  pages = {2831},
  author = {Florian Gacon and Christophe Godin and Olivier Ali},
  title = {BVPy: A FEniCS-based Python package to ease the expression and study of boundary value problems in Biology.},
  journal = {Journal of Open Source Software}}
