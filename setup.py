# {# pkglts, pysetup.kwds
# format setup arguments

from setuptools import setup, find_packages


short_descr = "A package implementing Boundary Value Problem."
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')


setup_kwds = dict(
    name='bvpy',
    version="1.0.1",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="Florian Gacon",
    author_email="florian.gacon@inria.fr",
    url='',
    license='cecill-c',
    zip_safe=False,

    packages=pkgs,

    package_dir={'': 'src'},
    setup_requires=[],
    install_requires=[
        ],
    tests_require=[],
    entry_points={},
    keywords='',
    )
# #}
# change setup_kwds below before the next pkglts tag

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
