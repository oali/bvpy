.. _general_introduction:

.. math::
  \newcommand{\euclid}[1]{\mathbb{R}^{#1}}
  \newcommand{\pos}{\boldsymbol{x}}
  \newcommand{\field}{u}
  \newcommand{\source}{g}
  \newcommand{\body}{\Omega}
  \newcommand{\surf}{\partial\body}
  \newcommand{\grad}[1]{\boldsymbol{\nabla}(#1)}
  \newcommand{\tline}{[t_0, t_{\infty}]}


General Introduction
####################

BVP & IBVP in a nutshell
========================

By definition, a *BVP* encompasses three elements:

*  A (set) differential equation(s), :math:`F(\dots)`  in :ref:`eq.(1)<eq_bvp>` below.
* An integration domain (:math:`\body`).
* A set of constrains defined on the boundaries of the integration domain (:math:`\surf_N\cup\surf_D`).

.. _eq_bvp:

.. math::
  :label: _eq_bvp

  \begin{cases}
  F(\field, \grad{\field} \dots) = 0 & \text{in} \quad \body \\
  \field(\pos) = u_N(\pos) & \text{on} \quad \surf_N \\
  \grad{\field(\pos)} = \boldsymbol{j}(\pos) & \text{on} \quad \surf_D
  \end{cases}

In the case of dynamic systems, the integration domain can be split into a one-dimension line and a spatial hyperplane: :math:`\body = \tline\times\body_{t_0}`. Where the spatial hyperplane :math:`\body_{t_0}` is amused not to evolve with time[^1]. In the case of differential equation featuring only first-order time derivatives, this variable separation yields the following formalization:

.. _eq_ibvp:

.. math::
  :label: ibvp

  \begin{cases}
  \partial_t u = F(\field, \grad{\field} \dots) & \text{in} \quad \body \\
  \field(t_0, \pos) = u_0(\pos) & \text{on} \quad \body_{t_0}\\
  \field(t,\pos) = u_N(t,\pos) & \text{on} \quad \surf_N\\
  \grad{\field(t,\pos)} = \boldsymbol{j}(t,\pos) & \text{on} \quad \surf_D
  \end{cases}

where :math:`\surf_N` and :math:`\surf_D` now stand respectively for :math:`\tline\times\surf_{t_0N}` and :math:`\tline\times\surf_{t_0D}`.

**Scope and restriction:** In its current version, **Bvpy** only encompass first and second-order BVPs and, as depicted by :ref:`eq.(2)<eq_ibvp>`, first-order time derivative in the case of IBVPs.




**Bvpy** general philosophy
===========================

Inspired by **FEniCS** approach and principle, the **Bvpy** library has been organized as close as possible to the *BVP*  and *IBVP* mathematical formulations given in eqs. :ref:`(1)<eq_bvp>` & :ref:`(2)<eq_ibvp>`, see :ref:`Fig.1<fig_library_organization>`. The goal is to provide users, and especially non-experienced ones, with the most intuitive *API* possible.

To that end, the library is built around two main classes, named `BVP` and `IBVP`, that respectively encapsulate **FEniCS** implementations of eqs. :ref:`(1)<eq_bvp>` & :ref:`(2)<eq_ibvp>`. These classes are parametrized by three main arguments that echoe the three *BVPs* components mentioned above. Instanciation of these arguments is handle through corresponding modules: `bvpy.domains`, `bvpy.vforms` and `bvpy.boundary_condition`. These two main classes combined with these three main modules constitute the backbone of **Bvpy**.

Numerical estimation of the instanced *BVPs* and *IBVPs* requires solver, those are handled by the `bvpy.solvers` module. Finally, a `bvpy.utils` module gathers housekeeping methods and minor classes organized into thematic sub-modules (*i.e.* `bvpy.utils.io`, `bvpy.utils.visu`...).

.. _fig_library_organization:

.. figure:: _static/library_organization.png

  Fig.1: Representation of the main modules and submodules forming the Bvpy library.
