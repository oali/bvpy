.. image:: https://gitlab.inria.fr/mosaic/bvpy/badges/develop/coverage.svg
  :target: https://gitlab.inria.fr/mosaic/bvpy

.. image:: https://gitlab.inria.fr/mosaic/bvpy/badges/master/pipeline.svg
  :target: https://gitlab.inria.fr/mosaic/bvpy/pipelines

.. image:: https://anaconda.org/mosaic/bvpy/badges/platforms.svg
  :target: https://anaconda.org/conda-forge/bvpy

.. image:: https://img.shields.io/conda/vn/conda-forge/bvpy.svg
  :target: https://anaconda.org/conda-forge/bvpy

.. image:: https://anaconda.org/mosaic/bvpy/badges/license.svg
  :target: https://www.gnu.org/licenses/lgpl-3.0.txts

.. image:: https://anaconda.org/conda-forge/bvpy/badges/latest_release_date.svg
  :target: https://anaconda.org/conda-forge/bvpy

.. image:: https://anaconda.org/conda-forge/bvpy/badges/downloads.svg
  :target: https://anaconda.org/conda-forge/bvpy

.. toctree::
   :caption: Table of contents
   :maxdepth: 2
   :hidden:

   Home <self>
   general_introduction
   library_description
   tutorials
   Sources <_dvlpt/modules>


.. include:: README.rst
