# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe
import numpy as np
import plotly.io as pio
import plotly.graph_objects as go

from bvpy.domains.abstract import AbstractDomain
from bvpy.utils.post_processing import SolutionExplorer


def set_renderer(renderer):
    pio.renderers.default = renderer


def _cone_plot(points, vectors, size=10, showscale=True):
    points = np.asarray(points)
    if len(points[0, :]) == 2:
        points = np.c_[points, np.zeros(len(points[:, 0]))]
    vectors = np.array(vectors)
    if vectors.shape[1] == 2:
        vectors = np.c_[vectors, np.zeros(len(vectors[:, 0]))]
    cones = go.Cone(x=points[:, 0],
                    y=points[:, 1],
                    z=points[:, 2],
                    u=vectors[:, 0],
                    v=vectors[:, 1],
                    w=vectors[:, 2],
                    sizemode="absolute",
                    sizeref=size,
                    showscale=showscale)

    return cones


def _wireframe_plot_mesh(mesh):
    coord = mesh.coordinates()
    triangle = mesh.cells()
    if len(coord[0, :]) == 2:
        coord = np.c_[coord, np.zeros(len(coord[:, 0]))]

    tri_points = coord[triangle]
    Xe = []
    Ye = []
    Ze = []
    for T in tri_points:
        Xe.extend([T[k % 3][0] for k in range(4)] + [None])
        Ye.extend([T[k % 3][1] for k in range(4)] + [None])
        Ze.extend([T[k % 3][2] for k in range(4)] + [None])

    # define the trace for triangle sides
    lines = go.Scatter3d(
                       x=Xe,
                       y=Ye,
                       z=Ze,
                       mode='lines',
                       name='',
                       line=dict(color='rgb(70,70,70)', width=2),
                       hoverinfo="none")

    return lines


def _surface_plot_function(function, colorscale, showscale=True):
    mesh = function.function_space().mesh()
    val = function.compute_vertex_values()
    coord = mesh.coordinates()
    triangle = mesh.cells().T
    hoverinfo = ["val:"+'%.5f' % item for item in val]

    if len(coord[0, :]) == 2:
        coord = np.c_[coord, np.zeros(len(coord[:, 0]))]

    surface = go.Mesh3d(
        x=coord[:, 0],
        y=coord[:, 1],
        z=coord[:, 2],
        i=triangle[0, :],
        j=triangle[1, :],
        k=triangle[2, :],
        flatshading=True,
        intensity=val,
        colorscale=colorscale,
        lighting=dict(ambient=1),
        name='',
        hoverinfo="all",
        text=hoverinfo,
        showscale=showscale
        )

    return surface


def _surface_plot_mesh(mesh, color, opacity=0.9):
    coord = mesh.coordinates()
    triangle = mesh.cells().T

    if len(coord[0, :]) == 2:
        coord = np.c_[coord, np.zeros(len(coord[:, 0]))]

    surface = go.Mesh3d(x=coord[:, 0],
                        y=coord[:, 1],
                        z=coord[:, 2],
                        i=triangle[0, :],
                        j=triangle[1, :],
                        k=triangle[2, :],
                        flatshading=True,
                        color=color,
                        opacity=opacity,
                        lighting=dict(ambient=1))

    return surface


def _plot_dofs(functionspace, size):
    dofs_coord = functionspace.tabulate_dof_coordinates()
    if len(dofs_coord[0, :]) == 2:
        dofs_coord = np.c_[dofs_coord, np.zeros(len(dofs_coord[:, 0]))]

    points = go.Scatter3d(
                    x=dofs_coord[:, 0],
                    y=dofs_coord[:, 1],
                    z=dofs_coord[:, 2],
                    mode='markers',
                    marker=dict(size=size)
                    )

    return points


def _plot_dirichlet_bc(diri, size, colorscale='inferno'):
    values = diri.get_boundary_values()
    dofs = diri.function_space().tabulate_dof_coordinates()
    if len(dofs[0, :]) == 2:
        dofs = np.c_[dofs, np.zeros(len(dofs[:, 0]))]
    coords_diri = np.array([dofs[i] for i in values.keys()])
    vals_diri = np.array(list(values.values()))

    points = go.Scatter3d(x=coords_diri[:, 0],
                          y=coords_diri[:, 1],
                          z=coords_diri[:, 2],
                          mode='markers',
                          marker=dict(size=size,
                                      color=vals_diri,
                                      colorscale=colorscale,
                                      colorbar=dict(thickness=20)))

    return points


def _surface_plot_meshfunc(meshfunc, colorscale):
    assert meshfunc.dim() == 2
    mesh = meshfunc.mesh()
    array = meshfunc.array()
    coord = mesh.coordinates()
    if len(coord[0, :]) == 2:
        coord = np.c_[coord, np.zeros(len(coord[:, 0]))]
    triangle = mesh.cells().T
    hoverinfo = ["val:"+'%d' % item for item in array]

    surface = go.Mesh3d(
        x=coord[:, 0],
        y=coord[:, 1],
        z=coord[:, 2],
        i=triangle[0, :],
        j=triangle[1, :],
        k=triangle[2, :],
        flatshading=True,
        intensity=array,
        colorscale=colorscale,
        lighting=dict(ambient=1),
        name='',
        hoverinfo="all",
        text=hoverinfo,
        intensitymode='cell'
        )

    return surface


def plot(obj, colorscale="inferno", wireframe=True, size=10, norm=False,
         color='gray', opacity=0.9, show_grid=False, size_frame=None,
         background=(242, 242, 242)):

    data = []
    if issubclass(type(obj), AbstractDomain):
        if obj.mesh is None:
            obj.discretize()
        surf = _surface_plot_mesh(obj.mesh, color=color, opacity=opacity)
        data.append(surf)

        if wireframe:
            data.append(_wireframe_plot_mesh(obj.mesh))

    elif isinstance(obj, fe.Mesh):
        surf = _surface_plot_mesh(obj, color=color, opacity=opacity)
        data.append(surf)

        if wireframe:
            data.append(_wireframe_plot_mesh(obj))

    elif isinstance(obj, fe.Function):
        if len(obj.ufl_shape) == 0:
            surface = _surface_plot_function(obj, colorscale=colorscale)
            data.append(surface)

        elif len(obj.ufl_shape) == 1:
            if norm:
                V = obj.function_space().split()[0].collapse()
                norm = fe.project(fe.sqrt(fe.inner(obj, obj)), V)
                surface = _surface_plot_function(norm, colorscale=colorscale)
                data.append(surface)

            func = SolutionExplorer(obj)
            pos = list()
            value = list()
            [[pos.append(p),value.append(v)] for p, v in zip(func.get_vertex_position(), func.to_vertex_values())]
            if norm:
                cones = _cone_plot(pos, value, size=size, showscale=False)
            else:
                cones = _cone_plot(pos, value, size=size)
            data.append(cones)

        if wireframe:
            lines = _wireframe_plot_mesh(obj.function_space().mesh())
            data.append(lines)

    elif isinstance(obj, fe.cpp.mesh.MeshFunctionSizet):
        surf = _surface_plot_meshfunc(obj, colorscale)
        data.append(surf)

        if wireframe:
            lines = _wireframe_plot_mesh(obj.mesh())
            data.append(lines)

    elif isinstance(obj, fe.FunctionSpace):
        points = _plot_dofs(obj, size=size)
        data.append(points)

        if wireframe:
            lines = _wireframe_plot_mesh(obj.mesh())
            data.append(lines)

    elif isinstance(obj, fe.DirichletBC):
        points = _plot_dirichlet_bc(obj, size)
        data.append(points)

        if wireframe:
            lines = _wireframe_plot_mesh(obj.function_space().mesh())
            data.append(lines)

    layout = go.Layout(scene_xaxis_visible=show_grid,
                       scene_yaxis_visible=show_grid,
                       scene_zaxis_visible=show_grid,
                       paper_bgcolor='rgb'+str(background),
                       margin=dict(l=80, r=80, t=50, b=50),
                       scene=dict(aspectmode="data"))

    if size_frame is not None:
        layout.update(width=size_frame[0], height=size_frame[1])

    fig = go.FigureWidget(data=data, layout=layout)
    fig.update_layout(hovermode='closest')
    fig.show()
