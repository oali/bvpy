# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import numpy as np
import fenics as fe
from bvpy.domains.abstract import AbstractDomain


def numerize(x):
    """Converts a string into an integer if possible otherwise a float.

    Parameters
    ----------
    x : str
        a number given as a string.

    Returns
    -------
    int if possible float else.
    """
    try:
        return int(x)
    except ValueError:
        try:
            return float(x)
        except ValueError:
            return x


def save(data, filename):
    """Save a fenics mesh or function as pvd or xml.

    Parameters
    ----------
    data : BVP or Domain
        the simulation or the domain to save.
    filename : str
        the path and the name of the recording file.

    Returns
    -------
    None

    """
    from bvpy import BVP

    if filename[-5:] != '.xdmf':
        filename += '.xdmf'

    with fe.XDMFFile(fe.MPI.comm_world, filename) as f:
        if isinstance(data, BVP):
            f.write(data.solution)
        elif issubclass(type(data), AbstractDomain):
            f.write(data.mesh)
        elif isinstance(data, fe.Function):
            f.write(data)
        elif isinstance(data, fe.cpp.mesh.MeshFunctionSizet):
            f.write(data)
        else:
            print(f'WARNING - bvpy.utils.io (line 35): {type(data)}\
                    is not supported by the save function.')


def write_ply(data, path):
    """Writes a polygonal mesh in the `.ply` format.

    Parameters
    ---------
    points : list of list of float
        list of the vertices position vectors (in 3D).
    cells : list of list of int
        list of lists of vertex ids surrounding each cell.
    path : str
        The location where to write the mesh.

    Returns
    -------
    None

    """

    if path[-4:] != '.ply':
        path += '.ply'

    if isinstance(data, fe.Mesh):
        mesh = data
        cdata = ['' for c in mesh.cells()]

    elif issubclass(type(data), AbstractDomain):
        mesh = data.mesh
        cdata = data.cdata.array()

    else:
        print('WARNING - utils.write_ply(): Not supported data type.')

    try:
        points = list([list(pts) for pts in mesh.coordinates()])

        cells = list([list(cell) for cell in mesh.cells()])

        labels = list([lbl for lbl in cdata])

        with open(path, 'w') as file:
            file.write('ply\n')
            file.write('format ascii 1.0\n')
            file.write(f'element vertex {len(points)}\n')
            file.write('property float x\n')
            file.write('property float y\n')
            file.write('property float z\n')
            file.write(f'element face {len(cells)}\n')
            file.write('property list int int vertex_index\n')
            if any([lbl != '' for lbl in labels]):
                file.write('property int label\n')
            file.write('end_header\n')
            for pts in points:
                file.write(' '.join([str(x) for x in pts])+'\n')

            for cell, labl in zip(cells, labels):
                file.write(str(len(cell)) + ' '
                           + ' '.join([str(v) for v in cell]) + ' '
                           + str(labl) + ' \n')

    except UnboundLocalError:
        pass


def read_polygonal_data(path):
    """Reader for '.txt', '.ply' and '.obj' files.

    Parameters
    ----------
    path : str
        Disc location of the file to read.

    Returns
    -------
    points : list of list of float
        list of the vertices position vectors (in 3D).
    cells : list of list of int
        list of lists of vertex ids surrounding each cell.
    properties : dict
        - keys : int.
            Topological degrees within the mesh (0, 1, 2, 3)
        - values : dict
            - keys : str
                Names of the properties attached to the mesh.
            - values : list
                Values of these properties of each element
                of the considered topolgocial degree.

    """
    try:
        assert path[-4:] in ['.txt', '.ply', '.obj']
        if path[-4:] == '.ply':
            return read_polygonal_ply(path)

        elif path[-4:] == '.obj':
            return read_polygonal_obj(path)

        elif path[-4:] == '.txt':
            return read_polygonal_txt(path)

    except AssertionError:
        print('WARNING - utils.read():'
              + 'the format of the given file is not supported.')


def read_polygonal_ply(path):
    """Reader for '.ply' files.

    Parameters
    ----------
    path : str
        Disc location of the file to read.

    Returns
    -------
    points : list of list of float
        list of the vertices position vectors (in 3D).
    cells : list of list of int
        list of lists of vertex ids surrounding each cell.
    properties : dict
        - keys : int.
            Topological degrees within the mesh (0, 1, 2, 3)
        - values : dict
            - keys : str
                Names of the properties attached to the mesh.
            - values : list
                Values of these properties of each element
                of the considered topolgocial degree.

    Notes
    -----
    The vertex labels defining a cell must, ideally, be oriented.

    """

    assert path[-4:] == '.ply'

    data = {0: None, 1: None, 2: None, 3: None}
    property_infos = {0: [], 1: [], 2: [], 3: []}

    # -- Reading the data
    with open(path, 'r') as file:
        content = file.readlines()
        header = content[:content.index('end_header\n')]

        elmt_deg = None
        for line in header:
            words = line.split(' ')
            if words[0] == 'element':
                if words[1] == 'vertex':
                    elmt_deg = 0
                    pts_nbr = int(words[-1])
                    data[elmt_deg] = content[content.index('end_header\n')+1:
                                             content.index('end_header\n')+1
                                             + pts_nbr]

                elif words[1] == 'face':
                    elmt_deg = 2
                    cell_nbr = int(words[2])
                    data[elmt_deg] = content[content.index('end_header\n')+1
                                             + pts_nbr:
                                             content.index('end_header\n')+1
                                             + pts_nbr + cell_nbr]

                elif words[1] == 'edge':
                    elmt_deg = 1
                    edge_nbr = int(words[2])
                    data[elmt_deg] = content[content.index('end_header\n')+1
                                             + pts_nbr + cell_nbr:
                                             content.index('end_header\n')+1
                                             + pts_nbr + cell_nbr + edge_nbr]

                elif words[1] == 'volume':
                    elmt_deg = 3
                    vol_nbr = int(words[2])
                    data[elmt_deg] = content[content.index('end_header\n')+1
                                             + pts_nbr + cell_nbr + edge_nbr:
                                             content.index('end_header\n')+1
                                             + pts_nbr + cell_nbr + edge_nbr
                                             + vol_nbr]

                else:
                    print(f"WARNING - elements of type {words[1]}"
                          + " defined within the mesh"
                          + "but not supported by this reader.")

            elif words[0] == 'property':
                p_name = words[-1][:-1]
                p_type = words[1]
                property_infos[elmt_deg].append((p_name, p_type))

    # -- Gathering properties
    properties = {}
    for deg in range(4):
        properties[deg] = {}

        if data[deg] is not None:
            shift = [int(z) for z in np.zeros(len(data[deg]))]
            # Above: A trick to deal with list properties.

        for i, (name, p_type) in enumerate(property_infos[deg]):
            if p_type != 'list':
                properties[deg][name] = [numerize(line[:-1].split(' ')[i+shft])
                                         for shft, line in zip(shift,
                                                               data[deg])]
            else:
                properties[deg][name] = [line[:-1].split(' ')[i+1:
                                         i+1+int(line[:-1].split(' ')[i])]
                                         for line in data[deg]]

                properties[deg][name] = [[numerize(elmt) for elmt in line]
                                         for line in properties[deg][name]]

                # below: Shift the index by the size of the list if another
                # property has to be read after.
                shift = [int(line[:-1].split(' ')[i+shft])
                         for shft, line in zip(shift, data[deg])]

    points = np.array([properties[0]['x'],
                       properties[0]['y'],
                       properties[0]['z']]).transpose()
    points = list([list(line) for line in points])
    cells = properties[2]['vertex_index']
    labels = list(range(cell_nbr))

    for deg, pro in [(0, 'x'), (0, 'y'), (0, 'z'), (2, 'vertex_index')]:
        del properties[deg][pro]
    for deg in [2, 3]:
        if 'label' in properties[deg].keys():
            labels = properties[deg]['label']
            break

    return points, cells, labels


def read_polygonal_txt(path):
    '''Extracts a list of vertex positions and of cells from a .txt file.

    Parameters
    ----------
    path : str
        The path to the file to read.

    Returns
    -------
    list(list(float))
        The list of 3D position vectors, one for every vertex.
    list(int)
        The list of indices of all the vertices surrounding each cell.
    list(int) optional
        If it exists in the data, it also returns the list of labels per cell.
    '''

    assert path[-4:] == '.txt'

    points = []
    cells = []
    labels = []

    with open(path, 'r') as file:
        content = file.readlines()

        point_nbr = int(content[0][:-8])
        cell_nbr = int(content[point_nbr+1].split(' ')[0])

        for line in content[1: point_nbr+1]:
            points.append([float(coord) for coord in line.split(' ')])

        for line in content[point_nbr+2:point_nbr+cell_nbr+2]:
            cells.append([int(pidx) for pidx in line.split(' ')])

        try:
            if content[point_nbr+cell_nbr+2][-8:-2] == 'labels':
                for line in content[point_nbr+cell_nbr+3:]:
                    labels.append(int(line))

        except IndexError:
            print('No label defined within this .txt file.')

        return points, cells, labels


def read_polygonal_obj(path):
    '''Extracts a list of vertex positions and of cells from a .obj file.

    Parameters
    ----------
    path : str
        The path to the file to read.

    Returns
    -------
    list(list(float))
        The list of 3D position vectors, one for every vertex.
    list(int)
        The list of indices of all the vertices surrounding each cell.
    list(int) optional
        If it exists in the data, it also returns the list of labels per cell.
    '''

    assert path[-4:] == '.obj'

    points = []
    cells = []
    labels = []

    with open(path, 'r') as file:
        content = file.readlines()
        for line in content:
            if line[:2] == 'v ':
                points.append([float(coord) for coord in line[2:-2].split(' ')
                               if coord != ''][:3])
            elif line[:2] == 'f ':
                cells.append([int(coord.split('/')[0]) - 1
                              for coord in line[2:-2].split(' ')
                              if coord != ''])

    return points, cells, labels
