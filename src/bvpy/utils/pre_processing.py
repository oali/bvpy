# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe
import numpy as np
from sympy import Function, Symbol
from sympy.parsing.sympy_parser import parse_expr

from types import FunctionType
from bvpy import logger


def _expression_from_string(source, functionspace, degree, **kwargs):
    _XYZ = {'x': Symbol('x[0]'),
            'y': Symbol('x[1]'),
            'z': Symbol('x[2]'),
            'border': Symbol('on_boundary'),
            'near': Function('near')}

    if isinstance(source, (tuple, list)):
        expr = []
        for src in source:
            expr.append(str(parse_expr(src, local_dict=_XYZ, evaluate=False)))
    else:
        expr = str(parse_expr(source, local_dict=_XYZ, evaluate=False))

    if degree is not None:
        expr = fe.Expression(expr, degree=degree, **kwargs)
    else:
        expr = fe.Expression(expr, element=functionspace.ufl_element(), **kwargs)

    if functionspace is not None:
        u = fe.Function(functionspace)
        u.interpolate(expr)
        return u
    else:
        return expr


def _expression_from_list(source, functionspace, degree):
    source = np.asarray(source)
    dim = source[0].shape
    if len(source.shape) == 1:
        source = np.array([[i] for i in source])

    class UserExpr(fe.UserExpression):
        def eval_cell(self, value, x, ufc_cell):
            val = source[ufc_cell.index]
            for i, v in enumerate(val):
                value[i] = v

        def value_shape(self):
            return dim

    if degree is None:
        expr = UserExpr()
    else:
        expr = UserExpr(degree=degree)

    if functionspace is not None:
        u = fe.Function(functionspace)
        u.interpolate(expr)
        return u
    else:
        return expr

def _expression_from_function(source, functionspace, degree):
    x = [0, 0, 0]
    dim = np.array(source(x)).shape

    if dim == ():

        class UserExpr(fe.UserExpression):
            def eval(self, value, x):
                val = [source(x)]
                for i, v in enumerate(val):
                    value[i] = v

            def value_shape(self):
                return dim

    elif len(dim) == 1:

        class UserExpr(fe.UserExpression):
            def eval(self, value, x):
                val = source(x)
                for i, v in enumerate(val):
                    value[i] = v

            def value_shape(self):
                return dim

    elif len(dim) == 2:

        class UserExpr(fe.UserExpression):
            def eval(self, value, x):
                flat_list = [item for sublist in source(x) for item in sublist]
                for i, v in enumerate(flat_list):
                    value[i] = v
                # val = np.asarray(source(x))
                # for i, arr in enumerate(val):
                #     for j, v in enumerate(arr):
                #         value[i][j] = v

            def value_shape(self):
                return dim

    if degree is None:
        expr = UserExpr()
    else:
        expr = UserExpr(degree=degree)

    if functionspace is not None:
        u = fe.Function(functionspace)
        u.interpolate(expr)
        return u
    else:
        return expr


def create_expression(source, functionspace=None, degree=None, **kwargs):
    if isinstance(source, str):
        return _expression_from_string(source, functionspace, degree, **kwargs)
    elif isinstance(source, FunctionType):
        return _expression_from_function(source, functionspace, degree)
    elif isinstance(source, (list, tuple)):
        if any([isinstance(i, str) for i in source]):
            return _expression_from_string(source,
                                           functionspace,
                                           degree,
                                           **kwargs)
        else:
            return _expression_from_list(source, functionspace, degree)
    else:
        logger.error(str(source)+' is not a string, tuple, list or a function')


class HeterogeneousParameter(fe.UserExpression):
    """Defines a parameter values for cells given a specific labelling.

    Attributes
    ----------
    cdata : :class:<MeshFunctionSizet`dolfin.cpp.MeshFunctionSizet`>
        The container encapsulating the cell labels to use.
    values : dict
        - keys : int
            The label values.
        - values : float
            The corresponding parameter values to use.

    Methods
    -------
    eval_cell :
        Returns the parameter value given a cell.
    value_shape :
        Specifies the shape of the parameter field.

    Notes
    -----
    This is a daughter class of
    :class:`UserExpression<dolfin.function.expression.UserExpression>`,
    checking the doc might be insightful.

    """

    def __init__(self, cdata, values, **kwargs):
        """Generates an instance of HeterogeneousParameter

        Parameters
        ----------
        cdata : :class:<MeshFunctionSizet`dolfin.cpp.MeshFunctionSizet`>
            The container encapsulating the cell labels to use.
        values : dict
            - keys : int
                The label values.
            - values : float
                The corresponding parameter values to use.

        Notes
        -----
        The **kwargs is mandatory in the signature,
        this is a fenics requirement.

        """
        assert set(cdata.array()) == values.keys()

        super().__init__(**kwargs)

        self.cdata = cdata
        self.values = values

    def eval_cell(self, value, x, ufc_cell):
        """Returns the parameter value given a cell.

        Parameters
        ----------
        values : dict
            - keys : int
                The label values.
            - values : float
                The corresponding parameter values to use.
        x : list of lists of floats
            The list of position vectors.
        ufl_cell : :class:`Cell<ufl.cell.Cell>`
            Representation of a finite element cell.

        """
        idx = ufc_cell.index
        value[0] = self.values[self.cdata[idx]]

    def value_shape(self):
        """Specifies the shape of the parameter field.

        Returns
        -------
        tuple
            the shape of the parameter.
            e.g. : () = scalar, (n,) = vector, (n,m) = tensor...

        """
        return ()
