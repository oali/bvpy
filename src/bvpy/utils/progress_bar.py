# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
from math import floor


class ProgressBar(object):
    def __init__(self, text='Run', size_bar=20, item='■'):
        self._start = None
        self._stop = None
        self._range = None
        self._current = None

        self._text = None
        self._size = None
        self._item = None

        self._progress_bar = None

        self.set_text(text)
        self.set_size_bar(size_bar)
        self.set_item(item)

    def set_text(self, text):
        self._text = text

    def set_size_bar(self, size):
        self._size = size

    def set_item(self, item):
        self._item = item

    def set_range(self, start, stop, current=None):
        self._start = start
        self._stop = stop
        if current is None:
            self._current = start
        else:
            self._current = current
        self._range = stop - start
        self.update(self._current)

    def set_current(self, current):
        self._current = current

    def update(self, current):
        self._current = current
        cur = self._current/self._range*self._size
        nb_hash = floor(cur)
        perc = round(current/self._range*100)

        prog = self._item*nb_hash+' '*(self._size-nb_hash)
        self._progress_bar = '\r'+self._text+' |{item}| {perc} %'.format(item=prog,perc=perc)

    def show(self):
        print(self._progress_bar, end='', flush=True)
