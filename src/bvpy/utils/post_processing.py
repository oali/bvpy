# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe
import numpy as np
import operator

from bvpy.utils.pre_processing import create_expression


class SolutionExplorer(object):
    """Enable the exploration of a FEniCS Function with numpy array

    Parameters
    ----------
    function : Function
        FEniCS Function to explore.

    Attributes
    ----------
    _fenics : Function
        FEniCS Function.
    _functionSpace : FunctionSpace
        The FunctionSpace associated to the Function.
    _mesh : Mesh
        The Mesh associated to the Function.
    shape : tuple
        Shape of the Function
    geometric_dimension : int
        The geometric dimension of the Function
    _dof_per_nodes : int
        Number of dof per nodes.
    nb_dofs : int
        Number of dofs.
    nb_nodes : int
        Number of nodes.
    _type : string
        Type of solution (scalar, vector, tensor)

    """

    def __init__(self, function):
        self._fenics = function

        self._functionSpace = self._fenics.function_space()
        self._mesh = self._functionSpace.mesh()
        self.shape = self._fenics.ufl_shape
        self.geometric_dimension = self._fenics.geometric_dimension()

        self._dof_per_nodes = np.prod(self.shape)
        if self._dof_per_nodes == 0:
            self._dof_per_nodes = int(1)  # for scalar Function

        self.nb_dofs = len(self._functionSpace.dofmap().dofs())
        self.nb_nodes = int(self.nb_dofs/self._dof_per_nodes)

        if isinstance(self._functionSpace.ufl_element(), fe.FiniteElement):
            self._type = 'scalar'
        elif isinstance(self._functionSpace.ufl_element(), fe.VectorElement):
            self._type = 'vector'
        elif isinstance(self._functionSpace.ufl_element(), fe.TensorElement):
            self._type = 'tensor'

    def __call__(self, *x):
        return self._fenics(*x)

    def get_vertex_position(self):
        """Return the verttex position.

        Returns
        -------
        numppy.ndarray
            Array containing the vertex position

        """
        return self._mesh.coordinates()

    def get_dofs_position(self):
        """Generates a Fenics object containing the dofs positions.

        Returns
        -------
        ???
            Description of returned object.

        """
        return self._functionSpace.tabulate_dof_coordinates()

    def get_dofs_value(self):
        """Return an array of the value of the Function at the dofs.

        Returns
        -------
        numpy.ndarray
            The array of the dofs' values

        """
        return self._fenics.vector().get_local()

    def to_fenics(self):
        """Returns a fenics.Function from the considered function.

        Returns
        -------
        Fenics.Function
            The fenics version of the function.

        """
        return self._fenics

    def to_vertex_values(self):
        """Computes the function values on the mesh vertices.

        Returns
        -------
        numpy.ndarray

        """
        if self._type == 'scalar':
            return self._fenics.compute_vertex_values()
        else:
            size_n = self.shape[0]
            if self._type == 'vector':
                res = np.zeros((self._mesh.num_entities(0), self.shape[0]))
                for i in range(size_n):
                    res[:, i] = self._fenics.sub(i, deepcopy=True).compute_vertex_values()

            if self._type == 'tensor':
                res = np.zeros((self._mesh.num_entities(0), self.shape[0], self.shape[1]))
                count = 0
                for i in range(size_n):
                    for j in range(size_n):
                        res[:, i, j] = self._fenics.sub(count+j, deepcopy=True).compute_vertex_values()
                    count += size_n
            return res

    def geometric_filter(self, axe='x', threshold=0, comparator=">", return_position=False, sort=None):
        """Return the value of the Function at the vertices of the mesh with a
        geometric filter.

        Parameters
        ----------
        axe : string
            The axe to apply the filter (could be 'x', 'y' or 'z')
        threshold : float or int
            The value of the threshold
        comparator : string
            The comparison to apply (could be '<', '<=', '>', '>=' or '==')
        sort : string
            Sort the coordinates and the Function on the specify axe
            (could be 'x', 'y' or 'z').
            Do not sort if None.

        Returns
        -------
        numpy.ndarray
            The value of the Function at the vertex with respect to the filter

        """
        d = dict(x=0, y=1, z=2)
        c = {'>': operator.gt, '>=': operator.ge, '<': operator.lt,
             '<=': operator.le, '==': operator.eq}
        assert comparator in c.keys()
        assert axe in d.keys()
        mask = c[comparator](self.get_vertex_position()[:, d[axe]], threshold)

        pos = self.get_vertex_position()[mask]
        val = self.to_vertex_values()[mask]

        if sort is not None:
            mask = np.argsort(pos[:,d[sort]])
            pos = pos[mask]
            val= val[mask]

        if return_position:
            return pos, val
        else:
            return val

    def cell_label_filter(self, label_func, index, return_position=False):
        """Return the value of the Function at the vertices of the mesh mark by the label_func.

        Parameters
        ----------
        label_func : MeshFunction
            MeshFunction on the cell of the mesh
        index : int
            The label of the MeshFunction to evaluate the Function

        Returns
        -------
        numpy.ndarray
            The value of the Function at the vertex with respect to the filter

        """
        self._mesh.init()
        assert self._mesh == label_func.mesh()
        arr = label_func.array()
        cells = (fe.Cell(self._mesh, c) for c in np.argwhere(arr==index))
        v = [list(c.entities(0)) for c in cells]
        mask = np.unique(v)
        if return_position:
            return self.to_vertex_values()[mask], self.get_vertex_position()[mask]
        else:
            return self.to_vertex_values()[mask]




def relative_error(fct_1, fct_2, mesh):
    """Computes the relative error between two fenicsFunctions.

    Parameters
    ----------
    fct_1 : :class:`Function<Dolfin.functions.function.Function>`
        One of the two function we want to compare.
    fct_2 : :class:`Function<Dolfin.functions.function.Function>`
        The othe function we want to use for comparison.
    mesh : :class:`Mesh<Dolfin.cpp.mesh.Mesh>`
        The fenicsMesh we want to compute the error on.

    Returns
    -------
    list of floats
        The list of the relative error values at each vertex of the mesh.

    Notes
    -----
    The relative error between two functions is here defined as the absolute
    value of the difference between these two functions divided by
    their mean value:
                        2 * abs(fct_1 - fct_2 / (fct_1 + fct_2))
    This expression is evaluated on each vertex of the mesh.
    """
    assert all([(fct_1(x) + fct_2(x)) != 0 for x in mesh.coordinates()])

    return [2 * abs((fct_1(x) - fct_2(x))
                    / (fct_1(x) + fct_2(x)))
            for x in mesh.coordinates()]


def estimate_precision(prblm, prediction, output='array', **kwargs):
    """Compares the computed solution of a given problem to a prediction.

    Parameters
    ----------
    prblm : :class:`bvp<bvpy.bvp>`
        The boundary-value problem we solved and we want to assess.
    prediction : str or list or
                 :class:`Function<Dolfin.functions.function.Function>`
        The expression/function/values we take as reference for the comparison.
    output : str
        The type of output we want. should be either 'array' or 'expression'.
        The default is 'array'.

    Other parameters
    ----------------
    The ones that can be used as **kwargs for the create_expression method.

    Notes
    -----
    The reference function is computed thanks to the create_expression method
    from the bvpy.utils.pre_processing module.
    For more info, see the corresponding documentation:
    :class:`create_epxression<bvp.utils.pre_processing.create_expression>`
    """

    computed_sol = prblm.solution
    func_space = prblm.functionSpace
    degree = prblm.vform._elements[0].degree
    mesh = prblm.domain.mesh

    expected_sol = create_expression(prediction, func_space, degree, **kwargs)

    if output == 'array':
        return relative_error(expected_sol, computed_sol, mesh)

    elif output == 'expression':
        diff = expected_sol - computed_sol
        mean = .5 * (expected_sol + computed_sol)
        error = abs(diff / mean)

        return fe.project(error, func_space)
