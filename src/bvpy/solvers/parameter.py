# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.solvers.parameter
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

class SolverParameter():
    """Encapsulates parameters used by the solver.

    Attributes
    ----------
    _param : dict
        - keys : str
            paramter names.
        - values : multiple
            implemented values.

    """

    def __init__(self):
        """Generates an instance of the class.

        Returns
        -------
        None

        """
        self._param = {'linear_solver': 'default',
                       'preconditioner': 'none',
                       'krylov_solver': {'absolute_tolerance': 1e-13,
                                         'relative_tolerance': 1e-13,
                                         'maximum_iterations': 1000,
                                         'monitor_convergence': False}}

    def __getitem__(self, item):
        """Gets the value of a recorded parameter.

        Parameters
        ----------
        item : str
            Name of the desired parameter.

        Returns
        -------
        multiple
            The seeked value.

        """
        try:
            return self._param[item]
        except KeyError:
            raise AttributeError(item)

    def __setitem__(self, item, value):
        """Sets the value of a parameter.

        Parameters
        ----------
        item : str
            name of the parameter to record.
        value : multiple
            the value to store.

        Returns
        -------
        None

        """
        self._param[item] = value
