# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.solvers.linear
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe
import time

from bvpy.solvers.parameter import SolverParameter
from bvpy import logger


class LinearSolver():
    """Class implementing linear solver.

    Attributes
    ----------
    _u : :class:`Function<dolfin.cpp.function.Function>`
        Solution of the problem.
    _a : :class:`Form<dolfin.cpp.fem.Form>`
        Bilinear form, lhs of the weak formulation.
    _L : :class:`Form<dolfin.cpp.fem.Form>`
        Linear form, rhs of the weak formulation.
    _bc : list of :class:`DirichletBC<dolfin.cpp.fem.DirichletBC>`
        List of dirichlet boundary conditions.
    parameters : :class:`SolverParameter<bvpy.solver.parameter.SolverParameter>`
        A container for all the simulation parameter.

    """

    def __init__(self):
        self._u = None
        self._a = None
        self._L = None
        self._bc = None
        self._ps = None

        self.parameters = SolverParameter()

    def set_problem(self, u, a, L, bc, ps):
        """Sets the problem.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            Solution of the problem.
        a : :class:`Form<dolfin.cpp.fem.Form>`
            Bilinear form, lhs of the weak formulation.
        L : :class:`Form<dolfin.cpp.fem.Form>`
            Linear form, rhs of the weak formulation.
        bc : list of :class:`DirichletBC<dolfin.cpp.fem.DirichletBC>`
            list with dirichlet boundary conditions.
        ps : :class:`dolfin.cpp.fem.PointSource<dkolfin.cpp.fem.PointSource>`,
             optional
             point source term (the default is None).

        Returns
        -------
            None

        """
        self._a = a
        self._L = L
        self._bc = bc
        self._u = u
        self._ps = ps

        prm = fe.parameter.parameters['krylov_solver']
        prm.update(self.parameters['krylov_solver'])

    def solve(self):
        """Solves the problem.

        Returns
        -------
            None
        """
        logger.info(' Backend       : petsc')
        logger.info(' Linear Solver : '
                    + str(self.parameters['linear_solver']))
        logger.info(' Preconditioner: '
                    + str(self.parameters['preconditioner']))
        logger.info(' Size          : '
                    + str(self._u.function_space().dim()))
        logger.info(' Solving LinearProblem ...')

        A = fe.assemble(self._a)
        b = fe.assemble(self._L)
        if self._bc is not None:
            for c in self._bc:
                c.apply(A, b)
        if self._ps is not None:
            for ps in self._ps:
                ps.apply(b)
        start = time.time()
        fe.solve(A,
                 self._u.vector(),
                 b,
                 self.parameters['linear_solver'],
                 self.parameters['preconditioner'])
        end = time.time()
        logger.info(' ... Done ['+str(end-start)+' s]')
