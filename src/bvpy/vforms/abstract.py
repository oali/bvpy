# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.utils.interface
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe
from abc import ABC, abstractmethod
from ufl.classes import Zero
import numpy as np

import ufl

from collections import namedtuple
from types import FunctionType

from bvpy.utils.pre_processing import create_expression

Element = namedtuple('Element', 'family degree dim')


class AbstractVform(ABC):
    """Defines common attributes and methods for variational forms.

    Attributes
    ----------
    _elements : list of :class:`Element<bvpy.vforms.abstract.Element>`
        The list of all the various types of finite element we are going
        to use in the vform.
    lhs : :class:`Form<ufl.form.Form>`
        The bilinear form of the variational formulation of the considered
        differential problem.
    rhs : :class:`Form<ufl.form.Form>`
        The linear form of the variational formulation of the considered
        differential problem.
    ds : :class:`Measure<ufl.measure.Measure>`
        A measure defined on the boundary of the integration domain.
    dx : :class:`Measure<ufl.measure.Measure>`
        A measure defined on the integration domain.
    _bterm : list of
        Description of attribute `_bterm`.

    """

    def __init__(self):
        """Generates a vform instance.

        """
        self._expression = 'Undisclosed by the developer'
        self._elements = []
        self._parameters = {}
        self.lhs = Zero()
        self.rhs = Zero()

        self.ds = fe.ds
        self.dx = fe.dx

        self._pointsources = []

    def __repr__(self):
        """Returns a concise description of the considered vform.

        Returns
        -------
        str
            A compact printable expression of the vform.

        """
        return '<'+str(self.vform_name)+', at '+str(hex(id(self)))+'>'

    def __str__(self):
        """Returns a detailed description of the considered vform.

        Returns
        -------
        str
            An extended printable expression of the vform.

        """
        return self._details

    def add_point_source(self, coordinates, val):
        self._pointsources.append(dict(point=fe.Point(coordinates), value=val))

    @property
    def vform_name(self):
        """str: Name of the variational form class.

        """
        return self.__class__.__name__

    @property
    def elements(self):
        """list of :class:`Element<bvpy.vforms.abstract.Element>`: list of the
           types of Finite Elements we consider to model this problem.
        """
        return list(self._elements)

    @property
    def degree(self):
        """int or list of int: The degree(s) of the element(s) used within
           this vform.

        """
        if len(self._elements) == 1:
            return self._elements[0].degree
        else:
            return [elmt.degree for elmt in self._elements]

    @property
    def _details(self):
        """str: A detailed printable description of the domain.

        """

        description = '''Finite Elements
---------------
'''
        for elmt in self._elements:
            description += f'    * Family: {elmt.family}\n'
            description += f'    * Order: {elmt.degree}\n'
            description += f'    * dimensionality: {elmt.dim}\n'

        description += f'''\n Variational form
-----------------
    * Name: {self.vform_name}
    * Expression: {self._expression}
    * Parameters:
'''
        for name, value in self._parameters.items():
            if isinstance(value, fe.Constant):
                description += f"           - {name}: {value.values()}\n"
            elif isinstance(value, fe.Expression):
                val = value._cppcode
                val = val.replace("x[0]", "x")
                val = val.replace("x[1]", "y")
                val = val.replace("x[2]", "z")
                description += f"           - {name}: {val}\n"
            elif isinstance(value, fe.UserExpression):
                description += f"           - {name}: {value.values}\n"
            elif value is None:
                description += f"           - {name}: {value}\n"
            else:
                description += f"           - {name}: unknown value...\n"
        return description

    def set_element_order(self, degree, ind=0):
        """Sets the order of the implemented finite elements.

        Parameters
        ----------
        degree : int
            The new degree value we want to set.
        ind : int, optional
            The incide of the element we want to change (the default is 0).

        """
        self._elements[ind] = self._elements[ind]._replace(degree=degree)

    def set_element_family(self, family, ind=0):
        """Sets the family of the used finite elements.

        Parameters
        ----------
        family : str
            The name of the family type of finite element we want to consider.
        ind : int, optional
            The incide of the element we want to change (the default is 0).

        Notes
        -----
        The FE family name can be chosen amongst all possibilities covered by
        FEniCS see the doc for the FunctionSpace class
        (:class:`FunctionSpace<dolfin.functions.functionspace.FunctionSpace>`)
        to get an exhaustive list. But the most common should be:
        * "CG" or "P" for Continuous Lagrange
        * "DG" for Discontinuous Lagrange
        """
        supported_FE_families = ["ARG", "AW", "BDFM", "BDM", "B", "CR", "DG",
                                 "DRT", "HER", "CG", "MTW", "MOR", "N1curl",
                                 "N2curl", "P", "Q", "RT", "R"]
        if family in supported_FE_families:
            self._elements[ind] = self._elements[ind]._replace(family=family)
        else:
            print(f"WARNING, AbstractDomain - set_element_family: {family}\
                    is not a supported type of finite element.")

    def _set_measures(self, dx, ds):
        """Sets the integration elements (measures) on and around the domain.

        Parameters
        ----------
        dx : :class:`Measure<ufl.measure.Measure>`
            A measure defined on the integration domain.
        ds : :class:`Measure<ufl.measure.Measure>`
            A measure defined on the boundary of the integration domain.

        """
        self.dx = dx
        self.ds = ds

    def set_parameters(self, **kwargs):
        """Sets a new value for a given parameters.

        Parameters
        ----------
        None

        Other parameters
        ----------------
        int or float or fenics.Expression
            The value or function that defines the parameter we want to change.

        Returns
        -------
        None

        """
        for name, val in kwargs.items():
            if isinstance(val, (int, float, list, tuple, np.ndarray)):
                self._parameters[name] = fe.Constant(val, name=name)
            elif isinstance(val, (str)):
                deg = max([elmt.degree for elmt in self._elements])
                self._parameters[name] = create_expression(val, degree=deg)
            elif isinstance(val, (fe.Expression, ufl.core.expr.Expr)):
                self._parameters[name] = val
            elif isinstance(val, FunctionType):
                self._parameters[name] = val
            elif val is None:
                self._parameters[name] = val
            else:

                print("WARNING"
                      + " - AbstractVform class, set_parameters method:"
                      + f" type {type(val)} is not handled.")

        self.set_expression()

    @abstractmethod
    def construct_form(self, u, v, sol):
        """Abstract method that constructs the Vform.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        """
        pass

    def get_form(self, u, v, sol):
        """Returns the lhs and the rhs elements of the Vform.

        Parameters
        ----------
        u : :class:`Function<dolfin.cpp.function.Function>`
            The trialFunction.
        v : :class:`Function<dolfin.cpp.function.Function>`
            The testFunction.
        sol : :class:`Function<dolfin.cpp.function.Function>`
            The solution.

        Returns
        -------
        tuple of two :class:`Form<ufl.form.Form>`
            Respectively the bilinear and the linear forms of
            the variational formulation.

        """
        self.construct_form(u, v, sol)
        return (self.lhs, self.rhs)

    @abstractmethod
    def set_expression(self, *args):
        pass

    def info(self):
        """Prints a detailed descrition of the Vform.

        """
        print(self._details)
