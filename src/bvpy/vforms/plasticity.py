# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.bvp
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
from .elasticity import ElasticForm
import numpy as np

import fenics as fe


class PlasticForm(ElasticForm):
    """Plastic vform, implement the decomposition of the deformation gradient.

    Parameters
    ----------
    F_plastic : list or :class:`Expression<dolfin.cpp.function.Expression>`
        The plastic part of the deformation gradient.

    """

    def __init__(self, source=None, young=1, poisson=.3, plane_stress=False,
                 F_plastic=np.diag([1, 1, 1]), thickness=1):
        super().__init__(source, young, poisson, plane_stress, thickness)

        self.set_parameters(F_plastic=F_plastic)

    def set_f_plastic(self, F_plastic):
        self.set_parameters(F_plastic=F_plastic)

    def construct_form(self, u, v, sol):
        d = sol.geometric_dimension()
        Id = fe.Identity(d)
        F_tot = Id + fe.nabla_grad(sol)
        F_e = F_tot*fe.inv(self._parameters['F_plastic'])
        C_e = F_e.T * F_e
        E_e = 0.5 * (C_e - Id)

        W = self._parameters['lmbda'] / 2 * (fe.tr(E_e)) ** 2 + self._parameters['mu'] * fe.tr(E_e * E_e)

        self.lhs = self._parameters['thickness'] * W * self.dx
        self.lhs -= fe.dot(self._parameters['source'], sol) * self.dx

        self.lhs = fe.derivative(self.lhs, sol, v)

    def set_expression(self):
        self._expression = f'psi(u)dx - Budx - Tudx '
