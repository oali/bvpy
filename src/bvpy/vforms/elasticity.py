# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.templates.vforms.linear_elasticity
#
#       File author(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File contributor(s):
#           Olivier Ali <olivier.ali@inria.fr>
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import fenics as fe

from ufl import nabla_grad, nabla_div, ln

from bvpy.vforms.abstract import AbstractVform, Element
from bvpy.utils.tensor import applyElementwise

__all__ = ['ElasticForm', 'LinearElasticForm', 'HyperElasticForm']


class ElasticForm(AbstractVform):
    """Base class for elastic vform.

    Parameters
    ----------
    source : list or :class:`Expression<dolfin.cpp.function.Expression>`
        The body forces.
    young : int or float
        Young Modulus.
    poisson : int or float
        Poisson ratio.
    plane_stress : bool
        If considering plane stress or not.
    thickness : int or float
        Thickness of the material.

    Attributes
    ----------
    _elements : :class:`Element<bvpy.vforms.abstract.Element>`
        Description of the Finite Element.
    _plane_stress : bool
        If considering plane stress or not.

    """

    def __init__(self, source=[0, 0, 0], young=1, poisson=.5,
                 plane_stress=False, thickness=1):
        super().__init__()
        self._elements = [Element('P', 1, 'vector')]

        self._plane_stress = plane_stress

        self.set_parameters(young=young,
                            poisson=poisson,
                            lmbda=None,
                            mu=None,
                            source=source,
                            thickness=thickness)

        self._update_mecha_parameters()

    def _update_mecha_parameters(self):
        self._parameters['mu'] = .5 * self._parameters['young']\
                                / (1 + self._parameters['poisson'])

        if self.plane_stress:
            self._parameters['lmbda'] = self._parameters['young']\
                                      * self._parameters['poisson']\
                                      / (1 - self._parameters['poisson'] ** 2)
        else:
            self._parameters['lmbda'] = self._parameters['young']\
                                      * self._parameters['poisson']\
                                      / ((1 + self._parameters['poisson'])
                                         * (1-2*self._parameters['poisson']))

    @property
    def plane_stress(self):
        return self._plane_stress

    @plane_stress.setter
    def plane_stress(self, value):
        assert isinstance(value, bool)
        self._plane_stress = value
        self._update_mecha_parameters()

    @property
    def young(self):
        return self._parameters['young']

    @young.setter
    def young(self, value):
        self._parameters['young'] = value
        self._update_mecha_parameters()

    @property
    def poisson(self):
        return self._parameters['poisson']

    @poisson.setter
    def poisson(self, value):
        self._parameters['poisson'] = value
        self._update_mecha_parameters()

    def strain(self, displacement):
        """Computes the strain field as a fenics.Function.

        Parameters
        ----------
        displacement : `Function<dolfin.function.function.Function>`
            The displacement field computed as a solution of a BVP
            built on a vform derived from the ElasticForm Class.

        Returns
        -------
        `Function<dolfin.function.function.Function>`
            A tensor function representing the strain field.

        """
        tnsr_func_space =\
            fe.TensorFunctionSpace(displacement.function_space().mesh(),
                                   'DG', 0)

        return fe.project(self._strain(displacement), tnsr_func_space)

    def stress(self, displacement):
        """Computes the stress field as a fenics.Function.

        Parameters
        ----------
        displacement : `Function<dolfin.function.function.Function>`
            The displacement field computed as a solution of a BVP
            built on a vform derived from the ElasticForm Class.

        Returns
        -------
        `Function<dolfin.function.function.Function>`
            A tensor function representing the stress field.

        """
        tnsr_func_space =\
            fe.TensorFunctionSpace(displacement.function_space().mesh(),
                                   'DG', 0)

        return fe.project(self._stress(displacement), tnsr_func_space)


class LinearElasticForm(ElasticForm):
    """Linear elastic vform in the small deformation approximation.
    """

    def _strain(self, u=None):
        """Computes the linear strain defined as the symmetric part of the
        deformation gradient.

        Parameters
        ----------
        u : :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            Displacement field.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed strain.

        """
        return 0.5 * (nabla_grad(u) + nabla_grad(u).T)

    def _stress(self, u=None):
        """Computes the stress as the energetic conjugate of the linear strain
        tensor field.

        Parameters
        ----------
        u : :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            Displacement field.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed stress.

        """
        d = u.geometric_dimension()
        return self._parameters['lmbda'] * nabla_div(u) * fe.Identity(d) +\
            2 * self._parameters['mu'] * self._strain(u)

    def construct_form(self, u, v, sol):
        self.lhs = self._parameters['thickness']\
                   * fe.inner(self._stress(u), self._strain(v)) * self.dx

        self.rhs = fe.dot(self._parameters['source'], v)\
            * self.dx(domain=v.function_space().mesh())

    def set_expression(self):
        self._expression = "thickness*stress(u)*strain(v)*dx "
        self._expression += "= source*v*dx"


class HyperElasticForm(ElasticForm):
    """Hyper Elastic vform.

    Attributes
    ----------
    elastic_potential : type
        Description of attribute `elastic_potential`.
    model : type
        Description of attribute `model`.

    """

    def __init__(self, source=None, young=1, poisson=.5,
                 material_model='st_venant', thickness=1, plane_stress=False):
        super().__init__(source, young, poisson, plane_stress, thickness)

        self.elastic_potential = None
        self.model = material_model

    def _strain(self, u):
        """Computes the Green Lagrange strain tensor field.

        Parameters
        ----------
        u : :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            Displacement field.

        type : str
            Optional. Name of the strain measure we want to compute.
            The default is 'GL' (Green-Lagrange).
            Available measures: 'GL', 'biot', 'natural' and 'almansi'.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed strain.

        """
        d = u.geometric_dimension()
        Id = fe.Identity(d)
        F = Id + fe.nabla_grad(u)
        C = F.T * F

        if type == 'GL':
            return 0.5 * (C - Id)

        elif type == 'biot':
            return applyElementwise(lambda x: x**0.5, C) - Id

        elif type == 'natural':
            return 0.5 * applyElementwise(lambda x: ln(x), C)

        elif type == 'almansi':
            return 0.5 * (Id - fe.inv(C))

        return 0.5 * (nabla_grad(u) + nabla_grad(u).T
                      + nabla_grad(u).T * nabla_grad(u))

    def _stress(self, u=None, model='st_venant'):
        """Computes the second Piola-Kirchhoff stress tensor field.

        Parameters
        ----------
        u : :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            Displacement field.

        model : str.
            The name of the considered hyperelastic model.
            Default value is 'st_venant'.

        Returns
        -------
        :class:`dolfin.cpp.function.Function<dolfin.cpp.function.Function>`
            The computed stress.

        """
        d = u.geometric_dimension()

        if model == 'st_venant':
            return self._parameters['lmbda'] * fe.tr(self._strain(u))\
                   * fe.Identity(d) + 2 * self._parameters['mu']\
                   * self._strain(u)
        else:
            print(f"WARNING: stress derivation from the {model} model is"
                  + "not implemented yet in the stress method.")

    def construct_form(self, u, v, sol):
        E = self._strain(sol)

        # -- Constitutive models
        if self.model == 'st_venant':
            W = fe.Constant(0.5) * self._parameters['lmbda']\
                * fe.tr(E) * fe.tr(E)
            W += self._parameters['mu'] * fe.tr(E * E)

        elif self.model == 'neo_hookean':
            d = u.geometric_dimension()
            Id = fe.Identity(d)
            F = Id + fe.nabla_grad(sol)
            C = F.T * F

            Ic = fe.tr(C)
            J = fe.det(F)
            W = (self._parameters['mu'] / 2) * (Ic - 3)\
                - self._parameters['mu'] * \
                fe.ln(J) + (self._parameters['lmbda'] / 2) * (fe.ln(J))**2

        self.lhs = self._parameters['thickness'] * W * self.dx
        self.lhs -= fe.dot(self._parameters['source'], sol) * self.dx

        self.lhs = fe.derivative(self.lhs, sol, v)

    def set_expression(self):
        self._expression = "psi(u)dx - Budx - Tudx "
