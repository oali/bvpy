# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.templates.vforms.helmholtz
#
#       File author(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
from math import pi
import fenics as fe
from bvpy.vforms.abstract import AbstractVform, Element


__all__ = ['HelmholtzForm']


class HelmholtzForm(AbstractVform):
    """Helmholtz vform.

    Parameters
    ----------
    source : list or :class:`Expression<dolfin.cpp.function.Expression>`
        The source term of the problem.
    linear_coef : int, float or :class:`Expression<dolfin.cpp.function.Expression>`
        A lineat coefficient.

    """
    def __init__(self, source=0, linear_coef=2*pi**2):
        super().__init__()
        self._elements = [Element('P', 1, 'scalar')]

        self.set_parameters(source=source, linear_coef=linear_coef)

    def construct_form(self, u, v, sol):
        self.lhs = fe.inner(fe.grad(u), fe.grad(v)) * self.dx
        self.lhs -= self._parameters['linear_coef'] * fe.inner(u, v) * self.dx
        self.rhs = self._parameters['source'] * v * self.dx

    def set_expression(self):
        self._expression = "grad(u)*grad(v)*dx - linear_coef*u*v*dx"
        self._expression += " = source*v*dx"
