# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.bvp
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

from types import FunctionType
from collections import OrderedDict

import fenics as fe
from bvpy.vforms.abstract import AbstractVform, Element
from bvpy.utils.pre_processing import create_expression


class TransportForm(AbstractVform):
    """Taken from: https://fenicsproject.org/pub/tutorial/html/._ftut1010.html#ftut1:reactionsystem.

    .. math::

        w\cdot\nabla u - \nabla\cdot(D\nabla u) = f

    where w=velocity
          D=diffusion coefficient
          f=source term

    Parameters
    ----------
    velocity : type
        Description of parameter `velocity`.
    diffusion_coef : type
        Description of parameter `diffusion_coef`.
    source : type
        Description of parameter `source`.

    Attributes
    ----------
    _elements : type
        Description of attribute `_elements`.
    _paramerters : dict
        - keys : str
            Names of the parameters of the variational form at stake.
        - values : fenics.Constant or fenics.Expression
            Values or functions defining these parameters.

    Notes
    -----
    In the present case, the parameter names are:
    'diffusion_coef', 'velocity', 'source' and 'reaction'.

    """

    def __init__(self,
                 velocity=None,
                 diffusion_coef=1,
                 source=0,
                 reaction=None):
        super().__init__()
        self._elements = [Element('P', 1, 'scalar')]

        self.set_parameters(source=source,
                            diffusion_coef=diffusion_coef,
                            velocity=velocity,
                            reaction=reaction)

    def construct_form(self, u, v, sol):

        # If no reaction, it's a linear problem
        if self._parameters['reaction'] is None:
            self.lhs = self._parameters['diffusion_coef']\
                * fe.dot(fe.grad(u), fe.grad(v)) * self.dx

            if self._parameters['velocity'] is not None:
                self.lhs += fe.dot(self._parameters['velocity'],
                                   fe.grad(u))*v*self.dx

            self.rhs = self._parameters['source'] * v * self.dx

        # If reaction it's a non-linear problem
        else:
            assert isinstance(self._parameters['reaction'], FunctionType)
            self.lhs = self._parameters['diffusion_coef']\
                * fe.dot(fe.grad(sol), fe.grad(v))\
                * self.dx - self._parameters['source'] * v * self.dx\
                - self._parameters['reaction'](sol) * v * self.dx

            if self._parameters['velocity'] is not None:
                self.lhs += fe.dot(self._parameters['velocity'], fe.grad(sol))
                self.lhs *= v * self.dx

    def set_expression(self):
        self._expression = "diffusion_coef*grad(u)*grad(v))*dx "
        if self._parameters['velocity'] is not None:
            self._expression += "+ velocity*grad(u)*v*dx "
        if self._parameters['reaction'] is not None:
            self._expression += " - reaction(u)*v*dx "
        self._expression += "= source*v*dx"


class CoupledTransportForm(AbstractVform):
    def __init__(self):
        super().__init__()
        self._elements = [Element('P', 1, 'scalar')]

        self.species = list()
        self._parameters['diffusion_coef'] = OrderedDict()
        self._parameters['reaction'] = OrderedDict()
        self._parameters['source'] = OrderedDict()

    def add_species(self, species, diffusion_coef, reaction=None, source=0):
        if len(self.species) > 0:
            self._elements.append(self._elements[0])
        self.species.append(species)

        self.set_multiple_parameters(species, diffusion_coef=diffusion_coef,
                                     reaction=reaction, source=source)

    def construct_form(self, u, v, sol):
        for i, spc in enumerate(self.species):
            self.lhs += self._parameters['diffusion_coef'][spc]\
                * fe.dot(fe.grad(sol[i]), fe.grad(v[i])) * self.dx

            if self._parameters['reaction'][spc] is not None:
                self.lhs -= self._parameters['reaction'][spc](*fe.split(sol))*v[i]*self.dx

            self.lhs -= self._parameters['source'][spc]*v[i]*self.dx

    def set_expression(self):
        for i, spc in enumerate(self.species):
            self._expression = "diffusion_coef *grad(u)*grad(v))*dx "

            if self._parameters['reaction'][spc] is not None:
                self._expression += " = reaction(u)*v*dx "

            self._expression = "source*v*dx"

    def set_multiple_parameters(self, species, **kwargs):
        """Sets the parameter values for a given species.

        Parameters
        ----------
        species : str
            Name of the species to add to the system

        Other parameters
        ----------------
        diffusion_coef : int, float, list, str
            values or expression for the diffusion coefficient
            of the considered species.

        source : int, float, list, str
            values or expression for the source term
            of the considered species.

        reaction : list, str, function
            values or expression for the reaction term
            of the considered species.

        Returns
        -------
        None

        """
        for name, val in kwargs.items():
            if isinstance(val, (int, float, list, tuple)):
                self._parameters[name][species] = fe.Constant(val, name=name)
            elif isinstance(val, str):
                deg = max([elmt.degree for elmt in self._elements])
                self._parameters[name][species] = create_expression(val,
                                                                    degree=deg)
            elif isinstance(val, fe.Expression):
                self._parameters[name][species] = val
            elif isinstance(val, FunctionType):
                self._parameters[name][species] = val
            elif val is None:
                self._parameters[name][species] = val
            else:
                print("WARNING"
                      + " - AbstractVform class, set_parameters method:"
                      + f" type {type(val)} is not handled.")
