"""
belle petite description
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}


import logging
from fenics import MPI as _MPI

logger = logging.getLogger('bvpy')
logger.setLevel(logging.INFO)
_formatter = logging.Formatter('%(levelname)s(bvpy): %(message)s')
_ch = logging.StreamHandler()
_ch.setLevel(logging.INFO)
_ch.setFormatter(_formatter)
logger.addHandler(_ch)
logger.propagate = False

# LOG_FMT = '%(levelname)s(femtk): %(message)s'
# - Set the logging format and level:
# logging.basicConfig(format=LOG_FMT, level=logging.INFO)

# logging FFC
_ffc_logger = logging.getLogger('FFC')
_ffc_logger.setLevel(logging.WARNING)

# logging UFL
_ufl_logger = logging.getLogger('UFL')
_ufl_logger.setLevel(logging.WARNING)

_dijitso_logger = logging.getLogger('dijitso')
_dijitso_logger.setLevel(logging.WARNING)


MPI_COMM_WORLD = _MPI.comm_world


from .bvp import BVP
from .ibvp import IBVP
