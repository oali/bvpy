# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.domains.abstract
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

import gmsh
import sys
import meshio as mio
from abc import ABC, abstractmethod
import tempfile

import fenics as fe
import numpy as np

_gmsh_algo = {'MeshAdapt': 1, 'Automatic': 2,
              'Delaunay': 5, 'Frontal-Delaunay': 6}


def _generate_default_boundary_data(mesh):
    """Sets the boundary of a mesh.

    Parameters
    ----------
    mesh : :class:`Mesh<Dolfin.cpp.mesh.Mesh>`
        The mesh on which we want to define boundaries.

    Returns
    -------
    :class:`MeshFunction<Dolfin.cpp.mesh.MeshFunction>`
        A fenics MeshFunction that marks (with the index 0)
        the boundary of the considered mesh.

    """
    mf = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    mf.set_all(sys.maxsize)
    subdomain = fe.CompiledSubDomain('on_boundary')
    subdomain.mark(mf, 0)
    return mf


def _generate_default_cell_data(mesh):
    """Sets the body (i.e. inner part) of a mesh.

    Parameters
    ----------
    mesh : :class:`Mesh<Dolfin.cpp.mesh.Mesh>`
        The mesh on which we want to mark the body.

    Returns
    -------
    :class:`MeshFunction<Dolfin.cpp.mesh.MeshFunction>`
        A fenics MeshFunction that marks (with the index 0 ?)
        the boundary of the considered mesh.

    Notes
    -----
    #TODO -> is it normal that the body and the boundary
    of the mesh are marked with the same label ? Or this could mean that I did
    not understand the purpose of these functions.

    """
    mf = fe.MeshFunction("size_t", mesh, mesh.topology().dim())
    mf.set_all(0)
    return mf


class AbstractDomain(ABC):
    """The most general class that defines an integration domain.

    Attributes
    ----------
    cdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunctionSizet>`
        Description of attribute `cdata`.
    bdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunctionSizet>`
        Description of attribute `bdata`.
    dx : :class:`Measure<ufl.measure.Measure>`
        An integral measure within the domain of interest.
    ds : :class:`Measure<ufl.measure.Measure>`
        An integral measure on the border of the domain of interest.
    mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
        A mesh representation (topology and geometry) of the considered domain.
    model : :class:`Model<gmsh.model>`
        A purely geometrical description of the domain.
    domain_type : str
        Name of the geometrical model of the domain.
    details : str
        A printable description of the domain main characteristics.

    """
    _model = gmsh.model
    _domain_instances = 0

    def __init__(self,
                 cell_type='triangle',
                 cell_size=1,
                 verbosity=False,
                 algorithm='Delaunay',
                 dim=3,
                 clear=False):
        """Generates an instance of the AbstractDomain class.

        Parameters
        ----------
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle').
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').

        Note
        ----
        The following cell types can be used to tile the considered domain:
            * 'line' (1D element)
            * 'triangle' (2D)
            * 'tetrahedron' (3D).
        """

        super().__init__(cell_type=cell_type, cell_size=cell_size,
                         verbosity=verbosity, algorithm=algorithm)

        AbstractDomain._domain_instances += 1
        if AbstractDomain._domain_instances == 1:
            gmsh.initialize()

        if clear:
            gmsh.clear()

        self._characteristic_size = None
        self._cell_type = cell_type
        self._cell_size = cell_size
        self._algorithm = algorithm

        self._mesh_dir = tempfile.TemporaryDirectory()
        self._dir_name = self._mesh_dir.name

        self.set_verbosity(verbosity)
        self.set_cell_size(cell_size)
        self.set_meshing_algorithm(algorithm)

        self.dim = dim
        self.mesh = None
        self.cdata = None
        self.bdata = None

        self.dx = fe.dx
        self.ds = fe.ds

        self.surfaces = dict()
        self.volumes = dict()

    def __del__(self):
        """Clears the temporary file containing the domain.

        """
        self._mesh_dir.cleanup()
        AbstractDomain._domain_instances -= 1
        if AbstractDomain._domain_instances == 0:
            gmsh.finalize()

    def __repr__(self):
        """Returns a short description of the considered domain.

        Returns
        -------
        str
            A printable short expression of the domain.

        """
        return '<'+str(self.domain_type)+', at '+str(hex(id(self)))+'>'

    def __str__(self):
        """Returns a detailed description of the considered domain.

        Returns
        -------
        str
            A printable detailed description of the domain.

        """
        return self._details

    @property
    def domain_type(self):
        """str: Name of the geometrical model of the domain.

        """
        return self.__class__.__name__

    @property
    def model(self):
        """:class:`Model<gmsh.model>`: The (gmsh) geometrical model used
                                       to define the domain.

        """
        return AbstractDomain._model

    @property
    def _details(self):
        """str: A detailed printable description of the domain.

        """

        description = f'''Domain
------
    * Shape: {self.domain_type}
    * Dimension: {self.dim}\n'''

        for name, val in self._characteristic_size.items():
            description += f'    * {name}: {val:.2e}\n'

        if self.mesh is not None:
            description += '\n' + f'''Meshing
-------
        * Algorithm: {self._algorithm}
        * Cell type: {self._cell_type}
        * Number: {len(self.mesh.cells())}
        * Resolution (i.e. prescribed element size): {self._cell_size:.2e}
        * Actual size (min, max): ({self.mesh.hmin():.2e},\
        {self.mesh.hmax():.2e})
    '''
            cells_quality = np.array([c.radius_ratio()
                                      for c in fe.cells(self.mesh)])
            mn_c_qual = cells_quality.mean()
            std_c_qual = cells_quality.std()

            description += "    * Cells quality:"
            description += f" {mn_c_qual:.2e} +/- {std_c_qual:.2e} \n"

        else:
            description = "Mesh not generate yet."
            description += " Use self.discretize() to to so."

        return description

    def synchronize(self):
        """synchronizes the CAD model with the gmsh model.

        """
        self.factory.synchronize()

    @abstractmethod
    def geometry(self, *args, **kwargs):
        """Computes the geometry of the domain.

        Parameters
        ----------
        None

        Returns
        -------
        None

        Note
        ----
        This abstract method must be defined within daughter class
        depending on the specific need.

        """
        pass

    def run_gui(self):
        """Generates a quick gmsh gui with the current model in it.

        """
        gmsh.fltk.run()

    def set_cell_size(self, cell_size):
        """Sets the characteristic size of the domain elements.

        Parameters
        ----------
        cell_size : float
            The cell_size value we want to use.

        """
        self._cell_size = cell_size
        gmsh.option.setNumber("Mesh.CharacteristicLengthMin", cell_size)
        gmsh.option.setNumber("Mesh.CharacteristicLengthMax", cell_size)

    def set_meshing_algorithm(self, algorithm):
        """Sets the algorithm to use to discretize the domain elements.

        Parameters
        ----------
        algorithm : str
            Name of the tiling algorithm to use.

        """
        self._algorithm = algorithm
        gmsh.option.setNumber("Mesh.Algorithm", _gmsh_algo[algorithm])

    def set_verbosity(self, verbosity):
        """Sets the verbosity of the considered domain.

        Parameters
        ----------
        verbosity : bool

        """
        if verbosity:
            gmsh.option.setNumber("General.Verbosity", 5)
        else:
            gmsh.option.setNumber("General.Verbosity", 0)

    def discretize(self):
        """Generates a fenics mesh representing the domain.

        Yields
        ------
        mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
            A fenics mesh corresponding to the domain.
        bdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunction>`
            A meshFunction that labels the borders (i.e. outermost facets)
            of the mesh of the domain.
        cdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunction>`
            A meshFunction that labels the inner elements (i.e. cells)
            of the mesh of the domain.
        ds : :class:`Measure<ufl.measure.Measure>`
            An integral measure on the border of the domain of interest.
        dx : :class:`Measure<ufl.measure.Measure>`
            An integral measure within the domain of interest.

        Notes
        -----
        * The method also generates a temporary gmsh mesh
        and stores it temporary on disc.
        * MeshFunctionSizet : `Sizet` means that the output of
        the function is a positive integer.

        """

        if self._cell_type == 'line':
            dim = 1
        elif self._cell_type == 'triangle':
            dim = 2
        elif self._cell_type == 'tetra':
            dim = 3

        if self.mesh is not None:
            self.model.mesh.clear()

        self._mesh_conversion(dim=dim)

        self.ds = fe.Measure('ds', domain=self.mesh, subdomain_data=self.bdata)
        self.dx = fe.Measure('dx', domain=self.mesh, subdomain_data=self.cdata)

    def _mesh_conversion(self, dim):
        """Converts a temporary gmsh mesh into a fenics (dolphin) one.

        Returns
        -------
        mesh : :class:`Mesh<dolfin.cpp.mesh.Mesh>`
            Fenics mesh corresponding to the domain.
        bdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunction>`
            MeshFunction that labels the borders (i.e. outermost facets)
            of the mesh of the domain.
        cdata : :class:`MeshFunctionSizet<dolfin.cpp.mesh.MeshFunction>`
            MeshFunction that labels the inner elements (i.e. cells)
            of the mesh of the domain.

        """

        self.model.mesh.generate(dim)

        gmsh.write(self._dir_name+'/mesh.msh')

        path = self._dir_name + '/mesh.msh'
        mesh_io = mio.read(path, file_format='gmsh')

        mesh_io.points = mesh_io.points[:, :self.dim]

        mesh = fe.Mesh()

        if ('gmsh:physical' in mesh_io.cell_data_dict.keys()
           and 'triangle' in mesh_io.cell_data_dict["gmsh:physical"].keys()):

            data = mesh_io.cell_data_dict["gmsh:physical"]['triangle']

            mio.write(self._dir_name+'/'+'mesh.xdmf',
                      mio.Mesh(points=mesh_io.points,
                               cells={self._cell_type:
                                      mesh_io.cells_dict[self._cell_type]},
                               cell_data={"triangle_data": [data]}))

            with fe.XDMFFile(self._dir_name+'/'+'mesh.xdmf') as f:
                f.read(mesh)

            mvc_tri = fe.MeshValueCollection("size_t", mesh, 2)
            with fe.XDMFFile(self._dir_name+'/'+'mesh.xdmf') as infile:
                infile.read(mvc_tri, 'triangle_data')
            mf_tri = fe.cpp.mesh.MeshFunctionSizet(mesh, mvc_tri)

            self.mesh = mesh
            self.cdata = mf_tri

        else:
            mio.write(self._dir_name+'/'+'mesh.xdmf',
                      mio.Mesh(points=mesh_io.points,
                               cells={self._cell_type:
                                      mesh_io.cells_dict[self._cell_type]}))

            with fe.XDMFFile(self._dir_name+'/'+'mesh.xdmf') as f:
                f.read(mesh)

            self.mesh = mesh
            self.cdata = _generate_default_cell_data(mesh)

        if ('gmsh:physical' in mesh_io.cell_data_dict.keys()
           and 'line' in mesh_io.cell_data_dict["gmsh:physical"].keys()):
            line_cells = []
            for cell in mesh_io.cells:
                if cell.type == "line":
                    if len(line_cells) == 0:
                        line_cells = cell.data
                    else:
                        line_cells = np.vstack([line_cells, cell.data])

            line_data = []
            for key in mesh_io.cell_data_dict["gmsh:physical"].keys():
                if key == "line":
                    if len(line_data) == 0:
                        line_data =\
                            mesh_io.cell_data_dict["gmsh:physical"][key]
                    else:
                        line_data = np.vstack([line_data, mesh_io.cell_data_dict["gmsh:physical"][key]])

            line_mesh = mio.Mesh(points=mesh_io.points,
                                 cells=[("line", line_cells)],
                                 cell_data={"boundary": [line_data]})
            mio.xdmf.write(self._dir_name+'/mesh_boundary.xdmf', line_mesh)

            mvc_line = fe.MeshValueCollection("size_t", mesh, 1)
            with fe.XDMFFile(self._dir_name+'/mesh_boundary.xdmf') as infile:
                infile.read(mvc_line, 'boundary')
            mf_line = fe.cpp.mesh.MeshFunctionSizet(mesh, mvc_line)
            self.bdata = mf_line

        else:
            self.bdata = _generate_default_boundary_data(mesh)

    def info(self):
        """Prints the descrition of the domain.

        Returns
        -------
        None

        """
        print(self._details)


class BuiltInModel(object):
    """A model based on the Gmsh `geo` factory.

    Attributes
    ----------
    factory : :class:`occ<gmsh.model.geo>`
        A gmsh model `geo` factory to discretize mesh from geometry.

    """

    def __init__(self, *args, **kwargs):
        """Sets the attribute factory to a specific type of gmsh factory.

        Returns
        -------
        factory : :class:`occ<gmsh.model.geo>`
            A gmsh model `geo` factory to discretize mesh from geometry.

        """
        super().__init__()
        self.factory = gmsh.model.geo


class OccModel(object):
    """A model based on the Gmsh `Occ` factory.

    Attributes
    ----------
    factory : :class:`occ<gmsh.model.occ>`
        A gmsh model `occ` factory to discretize mesh from geometry.

    """

    def __init__(self, *args, **kwargs):
        """Sets the attribute factory to a specific type of gmsh factory.

        Returns
        -------
        factory : :class:`occ<gmsh.model.occ>`
            A gmsh model `occ` factory to discretize mesh from geometry.

        """
        super().__init__()
        self.factory = gmsh.model.occ

    def __sub__(self, other):
        """Cutting the left hand side Domain with the right hand side domain.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)

        Returns
        -------
        CSGDomain
            Returns th result of the cut operation

        """

        csg = CSGDomain(cell_type=self._cell_type,
                        cell_size=self._cell_size,
                        algorithm=self._algorithm,
                        dim=self.dim)

        self.compute_new_size(other, csg)

        if not self.volumes:
            dim = 2
            objectdimtags = [(dim, tag) for tag in self.surfaces.values()]
            tooldimtags = [(dim, tag) for tag in other.surfaces.values()]
            outdimtag, _ = self.factory.cut(objectdimtags, tooldimtags)
            csg.surfaces[0] = outdimtag[0][1]
        else:
            dim = 3
            objectdimtags = [(dim, tag) for tag in self.volumes.values()]
            tooldimtags = [(dim, tag) for tag in other.volumes.values()]
            outdimtag, _ = self.factory.cut(objectdimtags, tooldimtags)
            csg.volumes[0] = outdimtag[0][1]

        self.factory.synchronize()

        return csg

    def __add__(self, other):
        """Adding the left hand side Domain with the right hand side domain.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)

        Returns
        -------
        CSGDomain
            Returns th result of the addition operation

        """

        csg = CSGDomain(cell_type=self._cell_type,
                        cell_size=self._cell_size,
                        algorithm=self._algorithm,
                        dim=self.dim)

        self.compute_new_size(other, csg)

        if not self.volumes:
            dim = 2
            objectdimtags = [(dim, tag) for tag in self.surfaces.values()]
            tooldimtags = [(dim, tag) for tag in other.surfaces.values()]
            outdimtag, _ = self.factory.fuse(objectdimtags, tooldimtags)
            csg.surfaces[0] = outdimtag[0][1]
        else:
            dim = 3
            objectdimtags = [(dim, tag) for tag in self.volumes.values()]
            tooldimtags = [(dim, tag) for tag in other.volumes.values()]
            outdimtag, _ = self.factory.fuse(objectdimtags, tooldimtags)
            csg.volumes[0] = outdimtag[0][1]

        self.factory.synchronize()

        return csg

    def __and__(self, other):
        """Compute the intersection of the two domains.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)

        Returns
        -------
        CSGDomain
            Return the result of the intersection.

        """

        csg = CSGDomain(cell_type=self._cell_type,
                        cell_size=self._cell_size,
                        algorithm=self._algorithm,
                        dim=self.dim)

        self.compute_new_size(other, csg)

        if not self.volumes:
            dim = 2
            objectdimtags = [(dim, tag) for tag in self.surfaces.values()]
            tooldimtags = [(dim, tag) for tag in other.surfaces.values()]
            outdimtag, _ = self.factory.intersect(objectdimtags, tooldimtags)
            csg.surfaces[0] = outdimtag[0][1]
        else:
            dim = 3
            objectdimtags = [(dim, tag) for tag in self.volumes.values()]
            tooldimtags = [(dim, tag) for tag in other.volumes.values()]
            outdimtag, _ = self.factory.intersect(objectdimtags, tooldimtags)
            csg.volumes[0] = outdimtag[0][1]

        self.factory.synchronize()

        return csg

    def compute_new_size(self, other, csg):
        """Computes a characteristic size for the new structure.

        Parameters
        ----------
        other : instance of subclass(AbstractDomain)
            The domain to combine with the considered one.
        csg : `CSGDomain<bvpy.domains.abstract.CSGDomain>`
            The combined domain that must be updated.

        Returns
        -------
        None

        """

        all_sizes = list(self._characteristic_size.values())\
            + list(other._characteristic_size.values())
        csg._characteristic_size = {'width': max(all_sizes)}


class CSGDomain(AbstractDomain, OccModel):
    def __init__(self, *args, **kwargs):
        """The resulting class of an add/sub or intersection of Domains.

        """
        super(CSGDomain, self).__init__(**kwargs)

    def geometry(self, *args, **kwargs):
        pass
