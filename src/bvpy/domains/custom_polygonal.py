# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.templates.custom_polygonal
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import numpy as np
import numpy.linalg as lng

from bvpy.domains.abstract import AbstractDomain, OccModel
from bvpy.utils.io import read_polygonal_data

__all__ = ['CustomPolygonalDomain']


class CustomPolygonalDomain(AbstractDomain, OccModel):
    """A Domain subclass to describe cellularized strucutres.

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    Notes
    -----
    We choose to describe the characteristic size of the structure here with
    three numbers: its maximal width, the number of polygons it contains and
    the average width of these polygons.
    """

    def __init__(self, points=[], cells=[],
                 markers=None, isoriented=True, **kwargs):
        """Generates a cellularized domain structure.

        Parameters
        ----------
        points : list of list of float
            List of the position vectors of the vertices (the default is []).
        cells : list of list of int
            Oriented lists of vertex indices
            surrounding each cell (the default is []).
        markers : list of int
            label values given to each cell (the default is None).
        isoriented : bool
            if True this means that the lists of vertex indices describing the
            cells are all sorted in a consistent order (either clockwise
            or anti-clockwize). If False, a routine in applied
            to orient them properly.

        Other parameters
        ----------------
        Corresponds to the parameters of the mother class
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        Yields
        ------
        :class:`CustomPolygonalDomain<bvpy.templates.domains.custom_polygonal.CustomPolygonalDomain>`
            A custom piecewise-polygonal domain instance.

        """
        super(CustomPolygonalDomain, self).__init__(**kwargs)

        cell_widths = [self.max_width([pts for vid, pts in enumerate(points)
                       if vid in cell]) for cell in cells]
        avg_cell_width = np.mean(cell_widths)

        self._characteristic_size = {'Maximal width': self.max_width(points),
                                     'Number of polygons': len(cells),
                                     'Averaged polygon width': avg_cell_width}

        if not isoriented:
            cells = self.orient_cell_contours(points, cells)

        self.geometry(points, cells, markers)

    def max_width(self, points):
        """Computes the maximal distance to the center in a point cloud.

        Parameters
        ----------
        points : list of float
            List of the position vectors of the vertices.

        Returns
        -------
        float
            Twice the maximal distance between any point within the considered
            list of points and the center of this list.
        """
        center = np.array(points).mean(axis=0)
        max_width = 2 * max([np.linalg.norm(pos - center)
                             for pos in np.array(points)])
        return max_width

    def geometry(self, points, cells, markers):
        """Adds a custom piecewise-polygonal structure to the Gmsh factory.

        Parameters
        ----------
        points : list of list of float
            List of the position vectors of the vertices.
        cells : list of list of int
            Oriented lists of vertex indices
            surrounding each cell.
        markers : list of int
            label values given to each cell.

        Returns
        -------
        None

        """
        pts = np.asarray(points)
        cells = np.asarray(cells)

        self.points = {id: self.factory.addPoint(*point, tag=id)
                       for id, point in enumerate(pts)}

        [self.factory.addSurfaceFilling(
         self.factory.addCurveLoop([self.factory.addLine(self.points[c[ind_p]],
                                    self.points[c[(ind_p + 1) % len(c)]])
                                    for ind_p, p in enumerate(c)]), tag=ind_c+1)
         for ind_c, c in enumerate(cells)]

        self.factory.removeAllDuplicates()

        self.synchronize()

        if markers is None:
            [self.model.addPhysicalGroup(2, [ind+1], ind)
             for ind in range(len(cells))]
        else:
            markers = np.asarray(markers)
            indexes = np.unique(markers)
            for i in indexes:
                tags = np.argwhere(markers==i).flatten()+1
                self.model.addPhysicalGroup(2, tags, i)


    def orient_cell_contours(self, points, cells):
        """Arranges the vertex labels surrounding cells in a coherent order.

        Parameters
        ----------
        points : list of list of float
            The list of the position vectors of the vertices.
        cells : list of list of int
            The lists of vertex indices surrounding each cell.

        Returns
        -------
        oriented_cells : list of list of int
            An oriented version of the `cells` argument: Within each list
            the vertices labels are arranged in a continuous manner.

        """
        # TODO: Adapt properly this algo !!!
        oriented_cells = []
        for i, cell in enumerate(cells):
            vrtcs = [points[vid] for vid in cell]
            vrtcs = [np.array(v) - np.array(vrtcs).mean(axis=0) for v in vrtcs]

            o_cell, o_vrtcs = [cell.pop(0)], [vrtcs.pop(0)]
            while len(cell) > 0:
                if len(o_cell) == 1:
                    orient = np.zeros(len(vrtcs))
                else:
                    last_nrml = np.cross(o_vrtcs[-2], o_vrtcs[-1])
                    nrmls = [np.cross(o_vrtcs[-1], v) for v in vrtcs]
                    orient = [np.sign(np.dot(n, last_nrml)) for n in nrmls]

                good_vrtcs = [(k, v) for k, (v, o)
                              in enumerate(zip(vrtcs, orient)) if o >= 0]

                dist = [np.dot(v / lng.norm(v),
                               o_vrtcs[-1] / lng.norm(o_vrtcs[-1]))
                        for _, v in good_vrtcs]

                vid_2_keep = good_vrtcs[dist.index(max(dist))][0]

                o_cell.append(cell.pop(vid_2_keep))
                o_vrtcs.append(vrtcs.pop(vid_2_keep))

            oriented_cells.append(o_cell)

        return oriented_cells

    @staticmethod
    def read(path, cell_size=.1, isoriented=True, clear=True, dim=3):
        """Instantiates a cellularized domain from a mesh file.

        Parameters
        ----------
        path : str
            location of the file to read.
        isoriented : bool optional
            if false, the vertex orientation around each cell is recomputed
            (the default is True).
        clear : bool
            Optional. If truen, clear all the previously defined Gmsh
            geometries. The default is False.
        dim : int
            Optional. Dimension of the problem. The default is 3.

        Returns
        -------
        :class: `CustomPolygonalDomain<bvpy.templates.domains.custom_polygonal.CustomPolygonalDomain>`
            A cellularized do5main.

        """
        points, cells, labels = read_polygonal_data(path)
        return CustomPolygonalDomain(points, cells,
                                     cell_size=cell_size,
                                     markers=labels,
                                     isoriented=isoriented,
                                     clear=clear,
                                     dim=dim)
