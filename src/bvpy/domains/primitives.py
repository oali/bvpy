# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.domains.primitives
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------

from bvpy.domains.abstract import AbstractDomain, BuiltInModel, OccModel

import numpy as np


class Rectangle(AbstractDomain, OccModel):
    """Instanciates a rectangular domain.

    Parameters
    ----------
    x : float
        Horizontal position of the lower left corder (the default is 0).
    y : float
        Vertical position of the lower left corder (the default is 0).
    length : float
        Horizontal extention of the rectangle (the default is 1).
    width : float
        Vertical extention of the rectangle (the default is 1).

    Other parameters
    ----------------
    Corresponds to the parameters of the mother class
    :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
    cell_type : str
        Type of element used to tile the domain
        (the default is 'triangle'). The accepted values can be:
        'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
    cell_size : float
        Characteristic size of the tiling elements (the default is 1).
    verbosity : bool
        If `True`, log is printed (the default is False).
    algorithm : str
        Name of the algorithm used to compute the elements
        (the default is 'Delaunay').
    dimension : float
        Dimension of the embedding space to consider.
        (the default is 3)

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    Methods
    -------
    geometry :
        Adds a rectangle element to the Gmsh factory
        in charge of computing the domain.
    discretize :
        Generates a fenics mesh representing the domain.
        See the doc of the mother class for more info:
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`

    Notes
    -----
    We choose to describe the characteristic size of the structure here with
    two numbers: its length and width parameters.
    """

    def __init__(self, x=0, y=0, length=1, width=1, **kwargs):
        """Generates an intance of a rectangular domain.

        Parameters
        ----------
        x : float
            Horizontal position of the lower left corder (the default is 0).
        y : float
            Vertical position of the lower left corder (the default is 0).
        length : float
            Horizontal extention of the rectangle (the default is 1).
        width : float
            Vertical extention of the rectangle (the default is 1).

        Other parameters
        ----------------
        Corresponds to the parameters of the mother class
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        Yields
        -------
        :class:`Rectangle<bvpy.templates.domains.primitives.Rectangle>`
            A rectangular domain instance.

        """
        super(Rectangle, self).__init__(**kwargs)
        self._characteristic_size = {'Length': length,
                                     'Width': width}
        self.geometry(x, y, length, width)

    def geometry(self, x, y, length, width):
        """Adds a rectangle to the Gmsh factory computing the domain.

        Parameters
        ----------
        x : float
            Horizontal position of the lower left corder (the default is 0).
        y : float
            Vertical position of the lower left corder (the default is 0).
        length : float
            Horizontal extention of the rectangle (the default is 1).
        width : float
            Vertical extention of the rectangle (the default is 1).

        Returns
        -------
        None

        """

        self.surfaces[0] = self.factory.addRectangle(x, y, 0, length, width)
        self.synchronize()


class Disk(AbstractDomain, OccModel):
    """Instanciates a circular domain.

    Parameters
    ----------
    radius : float
        The radius of the disk (the default is 1).
    angle_x : float
        Rotation angle around the Ox axis (the default is 0).
    angle_y : float
        Rotation angle around the Oy axis (the default is 0).
    angle_z : float
        Rotation angle around the Oz axis (the default is 0).

    Other parameters
    ----------------
    Corresponds to the parameters of the mother class
    :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
    cell_type : str
        Type of element used to tile the domain
        (the default is 'triangle'). The accepted values can be:
        'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
    cell_size : float
        Characteristic size of the tiling elements (the default is 1).
    verbosity : bool
        If `True`, log is printed (the default is False).
    algorithm : str
        Name of the algorithm used to compute the elements
        (the default is 'Delaunay').
    dimension : float
        Dimension of the embedding space to consider.
        (the default is 3)

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    Methods
    -------
    geometry :
        Adds a rectangle element to the Gmsh factory
        in charge of computing the domain.
    discretize :
        Generates a fenics mesh representing the domain.
        See the doc of the mother class for more info:
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`

    Notes
    -----
    We choose to describe the characteristic size of the structure here with
    one number: its radius parameter.

    """

    def __init__(self, center=[0, 0, 0],
                 radius=1, angle_x=0, angle_y=0, angle_z=0, **kwargs):
        """Generates a disk instance.

        Parameters
        ----------
        radius : float
            The radius of the disk (the default is 1).
        angle_x : float
            Rotation angle around the Ox axis (the default is 0).
        angle_y : float
            Rotation angle around the Oy axis (the default is 0).
        angle_z : float
            Rotation angle around the Oz axis (the default is 0).

        Other parameters
        ----------------
        Corresponds to the parameters of the mother class
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        Yields
        ------
        :class:`Rectangle<bvpy.templates.domains.primitives.Disk>`
            A disk domain instance.

        """

        super(Disk, self).__init__(**kwargs)
        self._characteristic_size = {'Radius': radius}
        self.geometry(center, radius)

    def geometry(self, center, radius):
        """Adds a disk to the Gmsh factory computing the domain.

        Parameters
        ----------
        radius : float
            The radius of the disk (the default is 1).
        angle_x : float
            Rotation angle around the Ox axis (the default is 0).
        angle_y : float
            Rotation angle around the Oy axis (the default is 0).
        angle_z : float
            Rotation angle around the Oz axis (the default is 0).

        Returns
        -------
        None

        """

        circle = self.factory.addCircle(*center, radius)
        curve_loop = self.factory.addCurveLoop([circle])
        self.surfaces[0] = self.factory.addPlaneSurface([curve_loop])
        self.synchronize()


class HemiSphere(AbstractDomain, BuiltInModel):
    """Instanciates an hemispherical domain.

    Parameters
    ----------
    radius : float
        The radius of the hemisphere (the default is 1).
    center : list of floats
        The position vector of the center of the sphere,
        the hemisphere is extracted from (the default is [0, 0, 0]).

    Other parameters
    ----------------
    Corresponds to the parameters of the mother class
    :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
    cell_type : str
        Type of element used to tile the domain
        (the default is 'triangle'). The accepted values can be:
        'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
    cell_size : float
        Characteristic size of the tiling elements (the default is 1).
    verbosity : bool
        If `True`, log is printed (the default is False).
    algorithm : str
        Name of the algorithm used to compute the elements
        (the default is 'Delaunay').
    dimension : float
        Dimension of the embedding space to consider.
        (the default is 3)

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    Methods
    -------
    geometry :
        Adds a rectangle element to the Gmsh factory
        in charge of computing the domain.
    discretize :
        Generates a fenics mesh representing the domain.
        See the doc of the mother class for more info:
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`

    Notes
    -----
    We choose to describe the characteristic size of the structure here with
    one number: its radius parameter.

    """

    def __init__(self, radius=1, center=[0, 0, 0], **kwargs):
        """Generates an instance of hemisphere domain.

        Parameters
        ----------
        radius : float
            The radius of the hemisphere (the default is 1).
        center : list of floats
            The position vector of the center of the sphere,
            the hemisphere is extracted from (the default is [0, 0, 0]).

        Other parameters
        ----------------
        Corresponds to the parameters of the mother class
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        Yields
        ------
        :class:`HemiSphere<bvpy.templates.domains.primitives.HemiSphere>`
            An hemispherical domain instance.

        """
        super(HemiSphere, self).__init__(**kwargs)
        self._characteristic_size = {'Radius': radius}
        self.geometry(radius, center)

    def geometry(self, radius, center):
        """Adds an hemisphere to the Gmsh factory computing the domain.

        Parameters
        ----------
        radius : float
            The radius of the hemisphere (the default is 1).
        center : list of floats
            The position vector of the center of the sphere,
            the hemisphere is extracted from (the default is [0, 0, 0]).

        Returns
        -------
        None

        """
        center = np.array(center)

        points = dict()
        surfaces = dict()
        lines = dict()

        points['center'] = self.factory.addPoint(*center)
        points['radius+x'] = self.factory.addPoint(center[0]+radius,
                                                   center[1],
                                                   center[2])
        points['radius-x'] = self.factory.addPoint(center[0]-radius,
                                                   center[1],
                                                   center[2])
        points['radius+y'] = self.factory.addPoint(center[0],
                                                   center[1]+radius,
                                                   center[2])
        points['radius-y'] = self.factory.addPoint(center[0],
                                                   center[1]-radius,
                                                   center[2])
        points['radius_z'] = self.factory.addPoint(center[0],
                                                   center[1],
                                                   center[2]+radius)

        lines['circle_1'] = self.factory.addCircleArc(points['radius+x'],
                                                      points['center'],
                                                      points['radius+y'])
        lines['circle_2'] = self.factory.addCircleArc(points['radius-x'],
                                                      points['center'],
                                                      points['radius+y'])
        lines['circle_3'] = self.factory.addCircleArc(points['radius-x'],
                                                      points['center'],
                                                      points['radius-y'])
        lines['circle_4'] = self.factory.addCircleArc(points['radius+x'],
                                                      points['center'],
                                                      points['radius-y'])

        lines['circle_5'] = self.factory.addCircleArc(points['radius_z'],
                                                      points['center'],
                                                      points['radius+x'])
        lines['circle_6'] = self.factory.addCircleArc(points['radius_z'],
                                                      points['center'],
                                                      points['radius-x'])
        lines['circle_7'] = self.factory.addCircleArc(points['radius+y'],
                                                      points['center'],
                                                      points['radius_z'])
        lines['circle_8'] = self.factory.addCircleArc(points['radius-y'],
                                                      points['center'],
                                                      points['radius_z'])

        val = self.model.addPhysicalGroup(1, [lines['circle_1'],
                                              lines['circle_2'],
                                              lines['circle_3'],
                                              lines['circle_4']], 100)
        self.model.setPhysicalName(1, val, 'boundary')

        lineloops = self.factory.addCurveLoop([lines['circle_1'],
                                               lines['circle_7'],
                                               lines['circle_5']])
        surfaces['quarter_1'] = self.factory.addSurfaceFilling([lineloops])

        lineloops = self.factory.addCurveLoop([lines['circle_7'],
                                               lines['circle_6'],
                                               lines['circle_2']])
        surfaces['quarter_2'] = self.factory.addSurfaceFilling([lineloops])

        lineloops = self.factory.addCurveLoop([lines['circle_6'],
                                               lines['circle_3'],
                                               lines['circle_8']])
        surfaces['quarter_3'] = self.factory.addSurfaceFilling([lineloops])

        lineloops = self.factory.addCurveLoop([lines['circle_8'],
                                               lines['circle_5'],
                                               lines['circle_4']])
        surfaces['quarter_4'] = self.factory.addSurfaceFilling([lineloops])

        self.synchronize()

        for key, val in surfaces.items():
            self.model.addPhysicalGroup(2, [val], val+1000)
            self.model.setPhysicalName(2, val+1000, key)

        self.synchronize()


class Cube(AbstractDomain, OccModel):
    def __init__(self, x=0, y=0, z=0, dx=1, dy=1, dz=1, **kwargs):
        super(Cube, self).__init__(**kwargs)
        self._characteristic_size = {}
        self.geometry(x, y, z, dx, dy, dz)

    def geometry(self, x, y, z, dx, dy, dz):
        self.volumes[0] = self.factory.addBox(x, y, z, dx, dy, dz)
        self.factory.synchronize()


class Cylinder(AbstractDomain, OccModel):
    def __init__(self, x=0, y=0, z=0,
                 dx=0, dy=0, dz=1, r=0.5, open=False, **kwargs):
        super(Cylinder, self).__init__(**kwargs)
        self._characteristic_size = {}
        self.geometry(x, y, z, dx, dy, dz, r, open)

    def geometry(self, x, y, z, dx, dy, dz, r, open):
        self.volumes[0] = self.factory.addCylinder(x, y, z, dx, dy, dz, r)
        self.factory.synchronize()

        if open:
            self._cell_type = "triangle"
            bnd = self.model.getBoundary([(3, self.volumes[0])])
            self.model.removeEntities([(3, self.volumes[0])])
            self.model.removeEntities([bnd[1], bnd[2]])


class Sphere(AbstractDomain, OccModel):
    """Instanciates an spherical domain.

    Parameters
    ----------
    radius : float
        The radius of the sphere (the default is 1).
    center : list of floats
        The position vector of the center of the sphere
        (the default is [0, 0, 0]).

    Other parameters
    ----------------
    Corresponds to the parameters of the mother class
    :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
    cell_type : str
        Type of element used to tile the domain
        (the default is 'triangle'). The accepted values can be:
        'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
    cell_size : float
        Characteristic size of the tiling elements (the default is 1).
    verbosity : bool
        If `True`, log is printed (the default is False).
    algorithm : str
        Name of the algorithm used to compute the elements
        (the default is 'Delaunay').
    dimension : float
        Dimension of the embedding space to consider.
        (the default is 3)

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    Methods
    -------
    geometry :
        Adds a rectangle element to the Gmsh factory
        in charge of computing the domain.
    discretize :
        Generates a fenics mesh representing the domain.
        See the doc of the mother class for more info:
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`

    Notes
    -----
    We choose to describe the characteristic size of the structure here with
    one number: its radius parameter.

    """

    def __init__(self, radius=1, center=[0, 0, 0], **kwargs):
        """Generates an instance of spherical domain.

        Parameters
        ----------
        radius : float
            The radius of the sphere (the default is 1).
        center : list of floats
            The position vector of the center of the sphere
            (the default is [0, 0, 0]).

        Other parameters
        ----------------
        Corresponds to the parameters of the mother class
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        Yields
        ------
        :class:`Sphere<bvpy.templates.domains.primitives.Sphere>`
            A spherical domain instance.

        """
        super(Sphere, self).__init__(**kwargs)
        self._characteristic_size = {'Radius': radius}
        self.geometry(radius, center)

    def geometry(self, radius, center):
        """Adds a sphere to the Gmsh factory computing the domain.

        Parameters
        ----------
        radius : float
            The radius of the sphere (the default is 1).
        center : list of floats
            The position vector of the center of the sphere
            (the default is [0, 0, 0]).

        Returns
        -------
        None

        """
        self.volumes[0] = self.factory.addSphere(*center, radius)
        self.synchronize()


class Ellipsoid(AbstractDomain, OccModel):
    """Instanciates an Ellispoid domain.

    Parameters
    ----------
    radius_xy : float
        The radius of the ellipsoid within the Oxy plane (the default is 1).
    radius_z : float
        The radius of the ellipsoid along the Oz axis (the default is 2).
    center : list of floats
        The position vector of the center of the sphere
        (the default is [0, 0, 0]).

    Other parameters
    ----------------
    Corresponds to the parameters of the mother class
    :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
    cell_type : str
        Type of element used to tile the domain
        (the default is 'triangle'). The accepted values can be:
        'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
    cell_size : float
        Characteristic size of the tiling elements (the default is 1).
    verbosity : bool
        If `True`, log is printed (the default is False).
    algorithm : str
        Name of the algorithm used to compute the elements
        (the default is 'Delaunay').
    dimension : float
        Dimension of the embedding space to consider.
        (the default is 3)

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    Methods
    -------
    geometry :
        Adds an ellipsoid element to the Gmsh factory
        in charge of computing the domain.
    discretize :
        Generates a fenics mesh representing the domain.
        See the doc of the mother class for more info:
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`

    Notes
    -----
    We choose to describe the characteristic size of the structure here with
    two numbers: its two radius parameters.

    """

    def __init__(self, radius_xy=1, radius_z=2, center=[0, 0, 0], **kwargs):
        """Generates an instance of ellipsoid domain.

        Parameters
        ----------
        radius_xy : float
            Radius of the ellipsoid within the Oxy plane (the default is 1).
        radius_z : float
            Radius of the ellipsoid along the Oz axis (the default is 2).
        center : list of floats
            Position vector of the center of the sphere
            (the default is [0, 0, 0]).

        Other parameters
        ----------------
        Corresponds to the parameters of the mother class
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        Yields
        ------
        :class:`Ellipsoid<bvpy.templates.domains.primitives.Ellipsoid>`
            An ellipsoid domain instance.

        """
        super(Ellipsoid, self).__init__(**kwargs)
        self._characteristic_size = {'Oxy radius': radius_xy,
                                     'Oz radius': radius_z}
        self.geometry(radius_xy, radius_z, center)

    def geometry(self, radius_xy, radius_z, center=[0, 0, 0]):
        """Adds an ellipsoid to the Gmsh factory computing the domain.

        Parameters
        ----------
        radius_xy : float
            Radius of the ellipsoid within the Oxy plane (the default is 1).
        radius_z : float
            Radius of the ellipsoid along the Oz axis (the default is 2).
        center : list of floats
            Position vector of the center of the sphere
            (the default is [0, 0, 0]).

        Returns
        -------
        None

        """
        assert len(center) == 3
        ratio = radius_xy/radius_z
        self.volumes[0] = self.factory.addSphere(*center, radius_z)
        self.factory.dilate([(3, self.volumes[0])], *center, ratio, ratio, 1)
        self.synchronize()


class Torus(AbstractDomain, OccModel):
    """Instanciates a toroid domain.

    Parameters
    ----------
    main_radius : float
        The radius of circle around which the tube is wrapped
        (the default is 2).
    tube_radius : float
        The tube radius (the default is 1).
    center : list of floats
        The position vector of the center of the torus,
        i.e. the center of the main circle (the default is [0, 0, 0]).

    Other parameters
    ----------------
    Corresponds to the parameters of the mother class
    :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
    cell_type : str
        Type of element used to tile the domain
        (the default is 'triangle'). The accepted values can be:
        'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
    cell_size : float
        Characteristic size of the tiling elements (the default is 1).
    verbosity : bool
        If `True`, log is printed (the default is False).
    algorithm : str
        Name of the algorithm used to compute the elements
        (the default is 'Delaunay').
    dimension : float
        Dimension of the embedding space to consider.
        (the default is 3)

    Attributes
    ----------
    _characteristic_size : dict
        - keys : str
            The name of the characteristics (i.e. length, width, radius...)
        - values : float
            The corresponding value.

    Methods
    -------
    geometry :
        Adds an ellipsoid element to the Gmsh factory
        in charge of computing the domain.
    discretize :
        Generates a fenics mesh representing the domain.
        See the doc of the mother class for more info:
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`

    Notes
    -----
    We choose to describe the characteristic size of the structure here with
    two numbers: the main_radius and the tube_radius parameters.

    """

    def __init__(self, main_radius=2, tube_radius=1, center=[0, 0, 0],
                 **kwargs):
        """Generates an instance of toroidal domain.

        Parameters
        ----------
        main_radius : float
            The radius of circle around which the tube is wrapped
            (the default is 2).
        tube_radius : float
            The tube radius (the default is 1).
        center : list of floats
            The position vector of the center of the torus,
            i.e. the center of the main circle (the default is [0, 0, 0]).

        Other parameters
        ----------------
        Corresponds to the parameters of the mother class
        :class:`AbstractDomain<bvpy.domains.abstract.AbstractDomain>`:
        cell_type : str
            Type of element used to tile the domain
            (the default is 'triangle'). The accepted values can be:
            'line' (1D element), 'triangle' (2D) or 'tetra' (3D).
        cell_size : float
            Characteristic size of the tiling elements (the default is 1).
        verbosity : bool
            If `True`, log is printed (the default is False).
        algorithm : str
            Name of the algorithm used to compute the elements
            (the default is 'Delaunay').
        dimension : float
            Dimension of the embedding space to consider.
            (the default is 3)

        Yields
        ------
        :class:`Torus<bvpy.templates.domains.primitives.Torus>`
            A toroidal domain instance.

        """
        super(Torus, self).__init__(**kwargs)
        self._characteristic_size = {'Main radius': main_radius,
                                     'Tube radius': tube_radius}
        self.geometry(center, main_radius, tube_radius)

    def geometry(self, center, main_radius, tube_radius):
        """Adds a torus to the Gmsh factory computing the domain.

        Parameters
        ----------
        center : list of floats
            The position vector of the center of the torus,
            i.e. the center of the main circle (the default is [0, 0, 0]).
        main_radius : float
            The radius of circle around which the tube is wrapped
            (the default is 2).
        tube_radius : float
            The tube radius (the default is 1).

        Returns
        -------
        None

        """
        self.factory.addTorus(*center, main_radius, tube_radius)
        self.synchronize()
