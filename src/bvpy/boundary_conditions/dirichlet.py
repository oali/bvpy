# -*- python -*-
# -*- coding: utf-8 -*-
#
#       bvpy.boundary_conditions.dirichlet
#
#       File author(s):
#           Florian Gacon <florian.gacon@inria.fr>
#
#       File contributor(s):
#           Florian Gacon <florian.gacon@inria.fr>
#           Olivier Ali <olivier.ali@inria.fr>
#
#       File maintainer(s):
#           Olivier Ali <olivier.ali@inria.fr>
#
#       Copyright © by Inria
#       Distributed under the LGPL License..
#       See accompanying file LICENSE.txt or copy at
#           https://www.gnu.org/licenses/lgpl-3.0.en.html
#
# -----------------------------------------------------------------------
import fenics as fe
from bvpy import logger

import numpy as np
from sympy import Symbol
from sympy.parsing.sympy_parser import parse_expr

from .boundary import Boundary
from bvpy.domains.geometry import boundary_normal

_XYZ = {'x': Symbol('x[0]'),
        'y': Symbol('x[1]'),
        'z': Symbol('x[2]')}


class ConstantDirichlet(object):
    """Defines a constant Dirichlet condition on a domain.

    """

    def __init__(self, val,
                 boundary="all", method='topological', subspace=None):
        """Generator of the ConstantDirichlet class.

        Parameters
        ----------
        val : float, int, list, tuple
            The value(s) to impose on the boundary domain.
        boundary : str or
            :class:`Boundary<bvpy.boundary_conditions.boundary.Boundary>`
            Optional. Defines where the condition must be applied
            (the default is "all").
        method : str
            Name of the method to use  (the default is 'topological').
        subspace : int
            Defines a subspace of the functionspace where
            the condition must be applied (the default is None).

        Yields
        ------
        :class:`ConstantDirichlet<bvpy.boundary_conditions.dirichlet.ConstantDirichlet>`
            A constrain applied to the function space
            in witch the solution will be looked for.

        """

        assert isinstance(val, (float, int, list, tuple, np.ndarray))
        if isinstance(boundary, Boundary):
            self._boundary = boundary
        else:
            self._boundary = Boundary(boundary)
        self._val = val
        self._method = method
        self._subspace = subspace

    def apply(self, functionSpace):
        """Applies the specified condition on the actual function space.

        Parameters
        ----------
        functionSpace :
            :class:`DOLFIN FunctionSpace<dolfin.cpp.function.FunctionSpace>`
            Function space on which the boundary condition must be enforced.

        Returns
        -------
        :class:`DOLFIN DirichletBC<Dolfin.cpp.fem.DirichletBC>`
            The boundary condition in a fenics-readable format.

        """
        if self._subspace is None:
            dir = fe.DirichletBC(functionSpace,
                                 fe.Constant(self._val), self._boundary(),
                                 method=self._method)
        else:
            dir = fe.DirichletBC(functionSpace.sub(self._subspace),
                                 fe.Constant(self._val),
                                 self._boundary(), method=self._method)

        if not dir.get_boundary_values():
            logger.warning('No nodes are marked for this domain: '
                           + str(self._boundary._string))

        return dir

    def __repr__(self):
        """Gives a representation of a ConstantDirichlet instance.

        Returns
        -------
        str
            a printable description of the ConstantDirichlet instance.

        """
        return "<ConstantDirichlet object, location: "\
               + str(self._boundary._string) + ", value: " + str(self._val)+">"


class VariableDirichlet(object):
    """Defines a variable Dirichlet condition on a domain.

    """

    def __init__(self, val, boundary="all",
                 method='topological', subspace=None, degree=None):
        """Generator of the VariableDirichlet class.

        Parameters
        ----------
        val : str
            A expression that specify the mathematical formulation
            of the boundary condition to consider.
        boundary : str or
            :class:`Boundary<bvpy.boundary_conditions.boundary.Boundary>`
            Optional. Defines where the condition must be applied
            (the default is "all").
        method : str
            Name of the method to use  (the default is 'topological').
        subspace : int
            Defines a subspace of the functionspace where
            the condition must be applied (the default is None).
        degree : int
            Description .... (the default is None).

        Yields
        ------
        :class:`VariableDirichlet<bvpy.boundary_conditions.dirichlet.VariableDirichlet>`
            A constrain applied to the function space
            in witch the solution will be looked for.

        """
        assert isinstance(val, str)
        if isinstance(boundary, Boundary):
            self._boundary = boundary
        else:
            self._boundary = Boundary(boundary)
        self._val = val
        self._method = method
        self._subspace = subspace
        self._degree = degree

    def apply(self, functionSpace):
        """Applies the specified condition on the actual function space.

        Parameters
        ----------
        functionSpace :
            :class:`DOLFIN FunctionSpace<Dolfin.cpp.function.FunctionSpace>`
            Function space on which the boundary condition must be enforced.

        Returns
        -------
        :class:`DOLFIN DirichletBC<Dolfin.cpp.fem.DirichletBC>`
            The boundary condition in a fenics-readable format.

        """
        parse = parse_expr(self._val, local_dict=_XYZ, evaluate=False)
        if self._degree is None:
            expr = fe.Expression(str(parse),
                                 element=functionSpace.ufl_element())
        else:
            expr = fe.Expression(str(parse), degree=self._degree)

        if self._subspace is None:
            dir = fe.DirichletBC(functionSpace, expr,
                                 self._boundary(),
                                 method=self._method)
        else:
            dir = fe.DirichletBC(functionSpace.sub(self._subspace),
                                 expr,
                                 self._boundary(),
                                 method=self._method)

        if not dir.get_boundary_values():
            logger.warning('No nodes are marked for this domain: '
                           + str(self._boundary._string))

        return dir

    def __repr__(self):
        """Gives a representation of a VariableDirichlet instance.

        Returns
        -------
        str
            a printable description of the considered object.

        """
        return "<VariableDirichlet object, location: "\
               + str(self._boundary._string) + ", value: "\
               + str(self._val) + ">"


# ------------------------------------------------------------------------------


def dirichlet(val, boundary, method='topological', subspace=None, degree=None):
    """Generates boundary conditions.

    Parameters
    ----------
    val : float, int, list, tuple, str
        The value(s), or expression, to impose on the boundary domain.
    boundary : str or
        :class:`Boundary<bvpy.boundary_conditions.boundary.Boundary>`
        Defines where the condition must be applied (the default is None).
    method : str
        Name of the method to use  (the default is 'topological').
    subspace : int
        Defines a subspace of the functionspace where
        the condition must be applied (the default is None).
    degree : ???
        Description .... (the default is None).

    Returns
    -------
    :class:`VariableDirichlet<bvpy.boundary_conditions.dirichlet.VariableDirichlet>`
    or
    :class:`ConstantDirichlet<bvpy.boundary_conditions.dirichlet.ConstantDirichlet>`
        The Dirichlet boundary condition to use as argument in the BVP class.

    """
    if isinstance(val, (float, int, list, tuple, np.ndarray)):
        return ConstantDirichlet(val, boundary, method, subspace)
    elif isinstance(val, str):
        return VariableDirichlet(val, boundary, method, subspace, degree)
    else:
        return


class ZeroDirichlet(ConstantDirichlet):
    """Implement a zero Dirichlet condition on a Domain.

    Parameters
    ----------
    boundary : str or
        :class:`Boundary<bvpy.boundary_conditions.boundary.Boundary>`
        Class definig the domain of the boundary condition.
    shape : int or tuple of int
        Shape of the value for the Dirichlet condition (the default is 0).

    """

    def __init__(self, boundary='all', shape=0, **kwargs):
        """Generates a zero Dirichlet boundary condition on a Domain.

        Parameters
        ----------
        domain : :class:`Domain<bvpy.boundary_conditions.boundary.Boundary>`
            Class definig the domain of the boundary condition.
        shape : int or tuple of int
            Shape of the value for the Dirichlet condition (the default is 0).

        Other parameters
        ----------------
        method : str
            Name of the method to use  (the default is 'topological').
        subspace : int
            Defines a subspace of the functionspace where
            the condition must be applied (the default is None).

        """
        if isinstance(boundary, str):
            boundary = Boundary(boundary)

        if shape == 0:
            super().__init__(0, boundary, **kwargs)
        else:
            super().__init__(np.zeros(shape), boundary, **kwargs)


class NormalDirichlet(object):

    def __init__(self, val=1, boundary='all',
                 method='topological'):
        """Generator of the VariableDirichlet class.

        Parameters
        ----------
        val : float optional
            amplitude of the normal vector field (the default is 1).
        boundary : str or
            :class:`Boundary<bvpy.boundary_conditions.boundary.Boundary>`
            optional
            defines where the condition must be applied (the default is 'all').
        method : str
            Name of the method to use  (the default is 'topological').

        Returns
        -------
        :class:`VariableDirichlet<bvpy.boundary_conditions.dirichlet.VariableDirichlet>`
            A constrain applied to the function space
            in witch the solution will be looked for.

        """
        assert isinstance(val, (int, float))

        if isinstance(boundary, Boundary):
            self._boundary = boundary
        else:
            self._boundary = Boundary(boundary)

        self._val = val
        self._method = method

    def __repr__(self):
        """Gives a representation of a NormalDirichlet instance.

        Returns
        -------
        str
            a printable description of the NormalDirichlet instance.

        """
        return "<NormalDirichlet object, location: "\
               + str(self._boundary._string) + ", value: " + str(self._val)+">"

    def apply(self, functionSpace):
        return fe.DirichletBC(functionSpace,
                              boundary_normal(functionSpace.mesh(),
                                              scale=self._val),
                              self._boundary(),
                              method=self._method)
