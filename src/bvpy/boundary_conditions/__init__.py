from .dirichlet import dirichlet, ZeroDirichlet, NormalDirichlet
from .boundary import Boundary
from .neumann import ConstantNeumann, VariableNeumann, neumann, NormalNeumann
