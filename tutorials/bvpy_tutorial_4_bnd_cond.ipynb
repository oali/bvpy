{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Boundary conditions\n",
    "\n",
    "\n",
    "This this tutorial, we will look a bit more into details of the `bvpy.boundary_conditions` module. The goal is to discover what kind of boundary conditions can be implemented within a *BVP* or *IBVP*.\n",
    "\n",
    "\n",
    "**Covered topics:**\n",
    "\n",
    "- Description and instantiation of the various available types of boundary conditions: `ConstantDirichlet`, `VariableDirichlet`, `ConstantNeumann` ...\n",
    "\n",
    "- Use of the `dirichlet()` and `neumann()` functions.\n",
    "\n",
    "- Basic  manipulations: combining several bc into one.\n",
    "\n",
    "- Basic presentation of the `SquarePeriodicBoundary` class.\n",
    "\n",
    "- Example of visualization of boundary conditions when a bvp is defined.\n",
    "\n",
    "Download the notebook: [bvpy_tutorial_4](https://gitlab.inria.fr/mosaic/publications/bvpy/-/raw/develop/tutorials/bvpy_tutorial_4_bnd_cond.ipynb?inline=false)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General manipulation\n",
    "The two main types of boundary conditions (*i.e.* Dirichlet's  and Neumann's boundary conditions) are available within the library."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each can be instanciated with a dedicated function `dirichlet()` (resp. `neumann()`) from the corresponding sub-modules within the `bvpy.boundary_conditions` module. Each of these function has two mandatory arguments: `val` and `boundary`, the latter defines the region of the border constrained by the boundary condition and the former corresponds to the actual value one wants to impose on it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions import dirichlet, neumann\n",
    "\n",
    "diri_1 = dirichlet(1, \"all\")\n",
    "neum_1 = neumann(val=[0, 0, 0], boundary=\"all\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The `val` argument can either be a constant (scalar, vector or tensor depending on the considered problem) or be defined by an expression (string encoding for a function of the position). Depending on the case, the `dirichlet` (resp. `neumann`) function will either implement a `ConstantDirichlet` or `VariableDirichelt` class (resp. `ConstantNeumann` or `VariableNeumann`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diri_2 = dirichlet(val=\"x*x\", boundary=\"all\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The `boundary` argument can accept string expression to define a sub-domain of the border. As shown above, the keyword `\"all\"` means naturally that the condition is applied to the entire border. The keyword `\"near(expression, value, precision)\"` can be a powerful tool to select a border sub-domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "neum_2 = neumann(0, boundary=\"near(x, 0)\")\n",
    "diri_3 = dirichlet(val=[1, 0, 0], \n",
    "                   boundary=\"near(x*x+y*y, 1, 1e-2)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unlike *domains* and *vforms*, boundary conditions do not have a `.info()` method to display properties but can be simply displayed with the general `print()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for bc in [diri_1, neum_1, diri_2, neum_2, diri_3]:\n",
    "    print(bc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Different boundary conditions applied to various sub-parts of a domain border can be combined together easily:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "ex = np.array([1, 0])\n",
    "\n",
    "neum_left = neumann(-ex, \"near(x,0)\")\n",
    "neum_right = neumann(+ex, \"near(x,0)\")\n",
    "\n",
    "bc_tot = [neum_left, neum_right]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Periodic boundary conditions\n",
    "The `bvpy.boundary_conditions.periodic` sub-module enables the definition of periodic boundary conditions on a simple rectangular domain."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is done with the `SquarePeriodicBoundary` class. This class defines a mapping that connects the values near one border to values near another one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions.periodic import SquarePeriodicBoundary\n",
    "\n",
    "pbc = SquarePeriodicBoundary(x=0, y=0, width=1, length=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization\n",
    "when implemented within a *bvp*, boundary conditions can be visualized thanks to the `plot()` function from the `bvpy.utils.visu` sub-module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy import BVP\n",
    "from bvpy.domains import Disk\n",
    "from bvpy.vforms import PoissonForm\n",
    "from bvpy.utils.visu import plot\n",
    "\n",
    "ring = Disk(radius=2, dim=2, clear=True) - Disk(radius=1, dim=2)\n",
    "\n",
    "ring.set_cell_size(.05)\n",
    "pfrm = PoissonForm()\n",
    "diri = dirichlet(\"x*x\", \"near(x*x + y*y, 4, 1e-3)\")\n",
    "\n",
    "prblm = BVP(domain=ring, vform=pfrm, bc=diri)\n",
    "\n",
    "plot(prblm.dirichletCondition[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
