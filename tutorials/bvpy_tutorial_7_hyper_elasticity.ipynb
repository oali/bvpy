{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More advanced examples of use\n",
    "\n",
    "In this tutorial we are going to show how one can use of **BVPy** to tackle less obvious problems than Poisson's or linear elasticity. To that end, we propose two examples:\n",
    "* A quick comparison between linear-elastic and hyperelastic behaviors\n",
    "* Modelization of the coupling between a reacto-diffusive scalar field and the elastic modulus of the considered medium.\n",
    "\n",
    "\n",
    "\n",
    "**Covered topics:**\n",
    "\n",
    "- Hyper elasticity variational form manipulation: `HyperElasticVform()`, `set_parameters()`, `info()`, `stress()`.\n",
    "\n",
    "- ...\n",
    "\n",
    "\n",
    "\n",
    "Download the notebook: [bvpy_tutorial_7](https://gitlab.inria.fr/mosaic/publications/bvpy/-/raw/develop/tutorials/bvpy_tutorial_7_hyper_elasticity.ipynb?inline=false)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Comparison between Linear elastic and hyper-elastic models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Basic notions about hyperelasticity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** The following notes are by no means an exhaustive description of the hyperelastic theory. Those are just quick remarks and comments gathered to provide some context to the following developments."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The theory of hyperelasticity can be seen as the generalization of linear elasticity. It relies on two key ingredients: \n",
    "* The formalization of the elastic behavior of a medium as a potential energy functional. \n",
    "* The quantification of the deformation (between a resting and a loaded configuration) through a strain tensor fields.\n",
    "\n",
    "The general idea is to build from these ingredients a mathematical expression of the potential energy as a function of invariants of the chosen strain tensor field. The combinaison of a particular expression of the energy functional and a strain measure corresponds to a specific hyperelastic model.\n",
    "\n",
    "For instance, two common hyperelastic models frequently encounter within the litterature are the **Saint-Venant Kirchhoff** and the **Neo-Hookean** models, which correspond respectively to the following elastic potentials:\n",
    "$$\n",
    "\\mathcal{E}_{\\text{SVK}} = \\frac{\\lambda}{2} \\text{tr}(\\boldsymbol{E})^2 + \\mu\\text{tr}(\\boldsymbol{E}^2),\n",
    "$$\n",
    "and\n",
    "$$\n",
    "\\mathcal{E}_{\\text{NH}} = C_1 \\big(\\text{det}(\\boldsymbol{F})  -3\\big)\n",
    "$$\n",
    "where $\\lambda, \\mu, C_1$ are coefficients, $\\boldsymbol{E}$ stands for the **Euler-Lagrange** strain tensor and $\\boldsymbol{F}$ the deformation gradient.\n",
    "\n",
    "> **Note:** Both of these models are implemented within the `HyperElasticForm` class from the `bvpy.vforms.elasticity` sub-module."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### The Euler-Lagrange strain measure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider a deformation field $\\varphi: \\boldsymbol{X} \\to \\boldsymbol{x}$ between two manifolds, describing a continuum in its resting and loaded configurations. \n",
    "\n",
    "A small vector $\\boldsymbol{dX}$ from the resting configuration is streched into $\\boldsymbol{dx} = \\boldsymbol{F}\\cdot \\boldsymbol{dX}$ in the loaded configuration. $\\boldsymbol{F}$ is a tensor field, called the *deformation gradient*: $\\boldsymbol{F} = \\boldsymbol{\\nabla}(\\varphi)$.\n",
    "\n",
    "From here, one can compute the (squared) length of any streched incremental element:\n",
    "$\n",
    "dx^2 = ||\\boldsymbol{dx}||^2 = \\boldsymbol{dX}^T\\cdot\\boldsymbol{F}^T\\cdot \\boldsymbol{F}\\cdot \\boldsymbol{dX}.\n",
    "$\n",
    "The streching, *i.e.* the difference between the size of the same incremental vector in both configurations, is then quantified by a symmetric second order tensor field:\n",
    "$$\n",
    "dl^2 = dx^2 - dX^2 = 2 \\boldsymbol{dX}^T\\cdot\\boldsymbol{E}\\cdot \\boldsymbol{dX}.\n",
    "$$\n",
    "\n",
    "The tensor within the right hand-side term: \n",
    "$$\n",
    "\\boldsymbol{E} = \\frac{1}{2}(\\boldsymbol{F}^T\\cdot \\boldsymbol{F} - \\boldsymbol{I}_{\\text{d}}),\n",
    "$$\n",
    "is called the **Euler-Lagrange** strain tensor and is a central strain measure in hyperelasticity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### How is it related to the linear elasticity theory ?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we make the assumption that the deformation between the resting and loaded configuations is small, the gradient $\\boldsymbol{F}$ can be taylor-expanded as follow:\n",
    "$$\n",
    "\\boldsymbol{F} \\approx \\boldsymbol{I}_{\\text{d}} + \\boldsymbol{\\nabla}(\\boldsymbol{u}),\n",
    "$$\n",
    "where $\\boldsymbol{u}$ is the displacement field presented in tutorial #6. Injecting this decomposition into the **Euler-Lagrange** strain expression above yields:\n",
    "$$\n",
    "\\boldsymbol{E} = \\boldsymbol{\\varepsilon} + \\frac{1}{2}\\boldsymbol{\\nabla}(\\boldsymbol{u})^T\\cdot\\boldsymbol{\\nabla}(\\boldsymbol{u}),\n",
    "$$\n",
    "with:\n",
    "$$\n",
    "\\boldsymbol{\\varepsilon} = \\frac{1}{2} \\big(\\boldsymbol{\\nabla}(\\boldsymbol{u})^T + \\boldsymbol{\\nabla}(\\boldsymbol{u}) \\big)\n",
    "$$\n",
    "the linear strain measure.\n",
    "\n",
    "**Morality:** From the quick developments above we see that in linear elasticity, the quadradic term $\\boldsymbol{\\nabla}(\\boldsymbol{u})^T\\cdot\\boldsymbol{\\nabla}(\\boldsymbol{u})$ is missing. In the following excercice, we are going to assess the importance of this term by comparing the response of a linear elastic and an hyperelastic model to the same loading."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### Domain definition\n",
    "Let's consider a simple circular patch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "from bvpy.domains import Disk\n",
    "from bvpy.utils.visu import plot, set_renderer\n",
    "\n",
    "patch = Disk(radius=2, cell_size= .1, clear=True, dim=2)\n",
    "\n",
    "set_renderer('notebook')\n",
    "plot(patch)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### Boundary conditions\n",
    "Let's assume that this domain is deformed by an imposed displacement field appliedto its border."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions import NormalDirichlet\n",
    "\n",
    "prescribed_boundary_displacement = 1\n",
    "\n",
    "traction = NormalDirichlet(val=prescribed_boundary_displacement, boundary='all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Two different elastic responses\n",
    "Let's know consider to types of elastic responses:\n",
    "* A *classic* linear response as the one depicted in the previous tutorial.\n",
    "* A non-linear response, often called hyperelasticity.\n",
    "\n",
    "While the former response will be implemented with the `LinearElasticForm` already presented in tutorial 6; the latter will be rely on a dedicated class `HyperElasticForm` from the `bvpy.vforms.elasticity` sub-module.\n",
    "\n",
    "Both will use the same elastic rheological parameters: Young's modulus and Poisson's ratio, with the same values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Young = 1e3\n",
    "Poisson = .5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Linear elastic response"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.vforms import LinearElasticForm\n",
    "\n",
    "linear_response = LinearElasticForm(young=Young, poisson=Poisson, source=[0., 0.], plane_stress=True)\n",
    "\n",
    "linear_response.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Hyper elastic response"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.vforms import HyperElasticForm\n",
    "\n",
    "non_linear_response = HyperElasticForm(young=Young, poisson=Poisson, source=[0., 0.], plane_stress=True)\n",
    "\n",
    "non_linear_response.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** Hyperelasticiy is a general term that encompasses various rheological models. In the current version of the library, we implemented *only* two of them: The **Saint-Venant Kirchhoff** model and the **Neo-Hookean** model. Selection between is controled by the `material_model` argument that sets the `self.model` attribute of the class. By default the chosen model is the Saint-Venant Kirchhoff one. Other models could of course be either added to the classes or could even be implemented in dedicated classes deriving from this one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Models comparison"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The idea now is to compare the strain values computed by these two models for various displacements prescribed on the boundary of the structure.\n",
    "\n",
    "To that end, we are going to interate over prescribed displacement values, compute the corresponding strain fields for both models and compare them.\n",
    "\n",
    "> **Note:** We are going to use the domain `patch` previously defined as well as the two elastic *vform* classes `linear_response` and `non_linear_response`, we therefore do not need to re-implement them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Running the simulations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from bvpy import BVP\n",
    "\n",
    "from bvpy.utils.tensor import intensity\n",
    "from bvpy.utils.post_processing import SolutionExplorer\n",
    "\n",
    "boundary_displacements = np.arange(.05, 1.05, .05)\n",
    "\n",
    "strn_int = {'ln': [], 'nl': []}\n",
    "    \n",
    "for bnd_displ in boundary_displacements:\n",
    "    \n",
    "    # -- Defining the boundary loading conditions\n",
    "    traction = NormalDirichlet(val=bnd_displ, boundary='near(x*x + y*y, 4, 1e-2)')\n",
    "    \n",
    "    for name, response in {'ln': linear_response, 'nl': non_linear_response}.items():\n",
    "        # -- Setting the problem\n",
    "        stretching = BVP(patch, response, traction)\n",
    "    \n",
    "        # -- Solving the problems\n",
    "        stretching.solve()\n",
    "        \n",
    "        # -- Computing the corresponding strain field\n",
    "        fe_strain = response.strain(stretching.solution)\n",
    "        np_strain = SolutionExplorer(fe_strain).to_vertex_values()\n",
    "        \n",
    "        strain_intensity = np.array(list(map(lambda x: intensity(x), np_strain)))\n",
    "        \n",
    "        strn_int[name].append((strain_intensity.mean(), strain_intensity.std()))     "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** We used the `SolutionExplorer` class to convert the strain field initially defined as a `Fenics.Function` (`fe_strain` in the code above) into a `numpy.nDarray` version (`np_strain` above). The idea behind this move is to facilitate the analysis of the simulation outputs by providing them in the very common format of `numpy.nDarray`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Quick visualization of the results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since the models are completely isotropic (and homogeneous) is shape, loading and properties, we expect the resulting strain fields to be also isotropic and homogeneous. We aim therefore to compare their intensity values averaged over the whole domain.\n",
    "\n",
    "To that end, for each value of the prescribed boundary displacement, we are going to compute the average value of the *linear* and the *non-linear* strain field intensity. We will plot one againts the other and see how the resulting curve shifts away from the first bisector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.lines as mlines\n",
    "\n",
    "threshold = .05 # Comparison threshold\n",
    "\n",
    "ln_strain = np.array(strn_int['ln'])\n",
    "nl_strain = np.array(strn_int['nl'])\n",
    "\n",
    "patch_radius = patch._characteristic_size['Radius']\n",
    "relat_prescr_displ = []\n",
    "\n",
    "fig = plt.figure(figsize=(15,10))\n",
    "plt.plot([0, 60], [0, 60], ls=':',c='k', alpha=.5)\n",
    "\n",
    "for bnd_displ, ln_strn, nl_strn in zip(boundary_displacements, ln_strain, nl_strain):\n",
    "\n",
    "    if nl_strn[0]/ln_strn[0]-1 < threshold:\n",
    "        marker, color = 'o', 'g'\n",
    "    else:\n",
    "        marker, color = '^','r'\n",
    "        relat_prescr_displ.append([ln_strn[0], bnd_displ/patch_radius])\n",
    "\n",
    "    plt.scatter(ln_strn[0]*100, nl_strn[0]*100, c=color, marker=marker)\n",
    "\n",
    "plt.plot([relat_prescr_displ[0][0]*100, relat_prescr_displ[0][0]*100], [0, 20], c='k', alpha=.5)\n",
    "\n",
    "# -- legend formatting\n",
    "red_triangles = mlines.Line2D([], [], ls='', color='r', marker='^',\n",
    "                              label=f\"Above {threshold:.0%} discrepency\")\n",
    "green_dots = mlines.Line2D([], [], ls='', color='g', marker='o',\n",
    "                           label=f\"Below {threshold:.0%} discrepency\")\n",
    "gray_line = mlines.Line2D([], [], color='k', alpha=.5,\n",
    "                          label=f\"Boundary displacement = {relat_prescr_displ[0][1]:.0%}\\n of initial radius\")\n",
    "plt.legend(handles=[red_triangles, green_dots, gray_line], loc=\"lower right\", fontsize=16)\n",
    "\n",
    "plt.xlabel('Linear elasticity strain intensity (in %)', fontsize=24)\n",
    "plt.ylabel('Hyper elasticity strain intensity (in %)', fontsize=24)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Morality:** Hyperelastic models and non-linear strain measures (such as the Green-Lagrange one we used here) are usually considered when the studied displacement field is large. A rule of thumb says that when the strain values exceed 10%, one would better use an hyperelastic model to describe its system. The figure above illustrate this by showing that above 10% the strain intensities of both models deviate for more than 5%. The quadratic term ($\\boldsymbol{\\nabla}(\\boldsymbol{u})^T\\cdot\\boldsymbol{\\nabla}(\\boldsymbol{u})$) discharded in the linear approximation is not negligeable anymore.\n",
    "\n",
    "> **Note:** Beside this illustration of basic continuum mechanics facts, let's remark that the implementation of the simulation loop generating the data, within the **BVPy** formalism, is smaller than the code snippet required to visualize the data (resp. 30 and 36 lines). ;)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Toward Multiphysics modeling: Combining BVPs and IBVPs into \"mixted models\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The goal now is to illustrate how we can use the **BVPy** library to combine *simple* *atomic* models into more complex ones. To that end, we are going to consider two models presented in previous tutorials:\n",
    "* The linear elastic model of a 2D curved domain loaded with a normal force field, introduced in tutorial #5.\n",
    "* The *Turing-like* Lengyel-Epstein model, introduced in tutorial #6.\n",
    "\n",
    "The resulting *toy system* we want to build up will feature the following properties:\n",
    "* The domain is the surface of a sphere.\n",
    "* The Young modulus quantifying the elasticity of the domain is function of the concentration of one of the two chemical species forming the reaction-diffusion model.\n",
    "* From a mechanical perspective, the system is assumed to be in a quasi-static evolution; *i.e.* chemical reacitons and diffusion processes are far slower than mechanical relaxation.\n",
    "\n",
    "The latter point means that, in terms of modeling strategy we are going to discretize time into steps upon which chemicals will evolve according to an *IBVP*. In parallel, the mechanical state of the structure will be computed as a *BVP* depending on the concentrations computed at the previous time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Domain definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider a sphere."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import Sphere\n",
    "\n",
    "globe = Sphere(radius=3, cell_size=.1, clear=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definition of the mechanics of the system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The particularity of the considered system is that the Young modulus of the material is a function of a chemical compound concentration. The first thing to do is then to write the corresponding function that takes a concentration as an inputs and returns the corresponding Young's modulus value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     2
    ]
   },
   "outputs": [],
   "source": [
    "import fenics as fe\n",
    "\n",
    "def compute_young(concentrations, Young_iso=1e3, domain=globe, component=0):\n",
    "    \"\"\"Computes Young's modulus given a concentration field.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    concentrations : Fenics.UserExpression or Fenics.Function.\n",
    "        The two concentrations of the interacting compounds from the Turing model.\n",
    "    Young_iso : float.\n",
    "        The basal value of the Young modulus without influence of the chemical field. \n",
    "        Optional, the default value is 1e3.\n",
    "    domain : Bvpy.Domains.\n",
    "        The domain to consider.\n",
    "        Optional, the default is globe (defined above).\n",
    "    component : int.\n",
    "        The index of the chemical compound to consider (either 0 or 1).\n",
    "        Optional, the default is 0.\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    Young : Fenics.Function\n",
    "        The scalar field of the Young modulus to consider in the linear elastic model.\n",
    "    \n",
    "    \"\"\"\n",
    "    \n",
    "    vct_func_space = fe.VectorFunctionSpace(domain.mesh, \"P\", 1, dim=2)\n",
    "    \n",
    "    if isinstance(concentrations, fe.UserExpression):\n",
    "        c_func = fe.interpolate(concentrations, vct_func_space).sub(component)\n",
    "    \n",
    "    elif isinstance(concentrations, fe.Function):\n",
    "        c_func = fe.project(concentrations, vct_func_space).sub(component)\n",
    "    \n",
    "    else:\n",
    "        print(\"WARNING: the type of the input is not supported\")\n",
    "    \n",
    "    c_max = c_func.vector().max()\n",
    "    c_min = c_func.vector().min()\n",
    "\n",
    "    Young = fe.Constant(1)\n",
    "    Young += fe.Constant(100 / (c_max - c_min)) * (fe.Constant(c_max) - c_func)\n",
    "    Young *= fe.Constant(Young_iso)\n",
    "    \n",
    "    return Young"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Notes:** \n",
    "> * We are considering two types of input for the function `compute_young`, either `Fenics.UserExpression` or `Fencis.Function`. This comes from the fact that initial conditions for the *Turing-like* model are defined with *user expression* while the model itself ouputs *functions*.\n",
    "> * The `concentrations` variable contains the concentrations of the two chemical species. One can choose one or the other as the *stiffening agent* by setting the `component` variable either to 0 or 1.\n",
    "\n",
    "**Inter-operability with FEniCS:**\n",
    "The function above is a good example of how **FEniCS** objects and methods can easily be used within **BVPy**. Such functions can be implemented by users already familiar with **FEniCS** and used by everyone."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now instanciate the mechanical model.\n",
    "\n",
    "To that end, we are going to define a function that takes concentrations as inputs and return a stress field as outputs. This stress field will correspond to the distribution of mechanical constrains within the considered domain, when mechanical equilibrium between pressure forces and elastic response is reached. The elastic response, parametrized by the Young modulus of the structure is dependent upon the concentration field via the function defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     3
    ]
   },
   "outputs": [],
   "source": [
    "from bvpy.domains.geometry import normal\n",
    "from bvpy.boundary_conditions import dirichlet\n",
    "\n",
    "def compute_mecha_equi(concentrations, **kwargs):\n",
    "    \"\"\"Simulates the loading of an linear elastic domain by a normal force field.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    concentrations : Fenics.UserExpression or Fenics.Function.\n",
    "        The two concentrations of the interacting compounds from the Turing model.\n",
    "    \n",
    "    Other parameters\n",
    "    ----------------\n",
    "    domain : bvpy.Domain\n",
    "        Optional. The default is globe.\n",
    "    pressure : float\n",
    "        The value of the pressure loading the structure.\n",
    "        Optional. The default is 1e2.\n",
    "    Young_iso : float\n",
    "        Basal value of the structure Young's modulus.\n",
    "        Optional. The default is 1e3.\n",
    "    Poisson : float\n",
    "        Poisson's coefficient value for the structure.\n",
    "        Optional. The default is .5\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    Fenics.Function\n",
    "        The stress tensor field at mechanical equilibrium.\n",
    "\n",
    "    \"\"\"\n",
    "    # -- Parameters\n",
    "    domain = kwargs.get('domain', globe)\n",
    "    pressure = kwargs.get('pressure', 1e3)\n",
    "    Young_iso = kwargs.get('Young_iso', 1e2)\n",
    "    Poisson = kwargs.get('Poisson', .5)\n",
    "\n",
    "    # -- Setting boundary conditions\n",
    "    fixed_center = [dirichlet(0, boundary='near(x, 0, 1e-2)', subspace=0),\n",
    "                    dirichlet(0, boundary='near(y, 0, 1e-2)', subspace=1),\n",
    "                    dirichlet(0, boundary='near(z, 0, 1e-2)', subspace=2)]\n",
    "\n",
    "    # -- Setting the loading force field as a source term within the vform\n",
    "    pressure_force = normal(domain, scale=pressure)\n",
    "    \n",
    "    # -- Setting the elastic response\n",
    "    Young = compute_young(concentrations, Young_iso=Young_iso, domain=domain)\n",
    "    \n",
    "    elastic_response = LinearElasticForm(young=Young, poisson=Poisson, source=pressure_force,\n",
    "                                         plane_stress=True)\n",
    "\n",
    "    # -- Setting the problem\n",
    "    mecha_equi = BVP(domain, elastic_response, fixed_center)\n",
    "\n",
    "    # -- Solving the problem\n",
    "    mecha_equi.solve()\n",
    "    \n",
    "    displacement = mecha_equi.solution\n",
    "    \n",
    "    return elastic_response.stress(displacement)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definition of the chemistry of  the system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now define the same kind of function but to handle the chemical side of the system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": [
     5
    ]
   },
   "outputs": [],
   "source": [
    "from bvpy.ibvp import IBVP\n",
    "from bvpy.vforms import CoupledTransportForm\n",
    "\n",
    "def compute_turing_pattern(initial_concentrations, **kwargs):\n",
    "    \"\"\"Computes the time evolution of two chemicals interacting with one another in a Turing-like manner.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    concentrations : Fenics.UserExpression or Fenics.Function.\n",
    "        The two concentrations of the interacting compounds from the Turing model.\n",
    "    \n",
    "    Other parameters\n",
    "    ----------------\n",
    "    domain : bvpy.Domain\n",
    "        Optional. The default is globe.\n",
    "    Da : float\n",
    "        The value of diffusion coefficient of the first chemical compound (named \"a\").\n",
    "        Optional. The default is 1.\n",
    "    ka : float\n",
    "        The value of production rate of the first chemical compound (named \"a\").\n",
    "        Optional. The default is 9.\n",
    "    Db : float\n",
    "        The value of diffusion coefficient of the second chemical compound (named \"b\").\n",
    "        Optional. The default is 2e-2.\n",
    "    ka : float\n",
    "        The value of production rate of the second chemical compound (named \"b\").\n",
    "        Optional. The default is 11.\n",
    "    t_min : float\n",
    "        Initial time of the simulation.\n",
    "        Optional. The default is 0.\n",
    "    t_max : float\n",
    "        Final time of the simulation.\n",
    "        Optional. The default is 2.5.\n",
    "    dt : float\n",
    "        Time step increment.\n",
    "        Optional. The default is .1.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    Fenics.Function\n",
    "        The concentrations (a, b) vector field after a t_max - t_min time.\n",
    "\n",
    "    \"\"\"\n",
    "    # -- Parameters\n",
    "    domain = kwargs.get('domain', globe)\n",
    "    Da = kwargs.get('Da', 1)\n",
    "    Db = kwargs.get('Db', 2e-2)\n",
    "    ka = kwargs.get('ka', 9)\n",
    "    kb = kwargs.get('kb', 11)\n",
    "    \n",
    "    t_min = kwargs.get('t_min', 0)\n",
    "    t_max = kwargs.get('t_max',2.5)\n",
    "    dt = kwargs.get('dt',.1)\n",
    "    \n",
    "    save_path = kwargs.get('saving_path', '../data/tutorial_results/tuto_7_result_1.xdmf')\n",
    "    \n",
    "    # -- Instancing Turing model\n",
    "    LE_model = CoupledTransportForm()\n",
    "\n",
    "    LE_model.add_species('a', Da, lambda a, b: ka*(b-(a*b)/(1+b*b)))\n",
    "    LE_model.add_species('b', Db, lambda a, b: kb-b-(4*a*b)/(1+b*b))\n",
    "\n",
    "    # -- Building the problem\n",
    "    turing_system = IBVP(domain, LE_model, initial_state=initial_concentrations)\n",
    "\n",
    "    # -- Solving the problem\n",
    "    turing_system.integrate(t_min, t_max, dt, save_path)\n",
    "    \n",
    "    return turing_system.solution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let's combine both models into a big loop"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we need to define an initial state in term of chemical concentrations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.pre_processing import create_expression\n",
    "\n",
    "# -- Creating the initial concentration distribution\n",
    "ka = 9\n",
    "kb = 11\n",
    "\n",
    "np.random.seed(1093)\n",
    "globe.discretize()\n",
    "random = np.random.uniform(low=-1, high=1, size=globe.mesh.num_entities(2))\n",
    "\n",
    "random_pattern = [[1+0.04*ka**2+0.1*r, 0.2*kb+0.1*r] for r in random]\n",
    "\n",
    "concentrations = create_expression(random_pattern)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Notes:** \n",
    "> * The `create_expression` function, from the `bvpy.utils.pre_processing` sub-module is used to *translate* a *classical* `list` into a `Fenics.UserExpression`. \n",
    "> * Similarly, the `SolutionExplorer` class from the `bvpy.utils.post_processing` sub-module enables *translation* of `Fenics.Function` into more common objects, such as `numpy.nDarray`.\n",
    "> * The general idea is that the `pre_processing` and `post_processing` sub-modules aim at interfacing the ***FEniCS***-*based* core with more commun data representations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can combine both models together. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.io import save\n",
    "\n",
    "# -- Running the simulations\n",
    "step_nbr = 1\n",
    "for step in np.arange(1, 1+step_nbr):\n",
    "    \n",
    "    print(f\"\\n step #{step} over {step_nbr}\")\n",
    "        \n",
    "    stress = compute_mecha_equi(concentrations)\n",
    "    concentrations = compute_turing_pattern(concentrations)\n",
    "\n",
    "    save(stress, f\"../data/tutorial_results/tuto_7_result_{1+step}.xdmf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** For the sake of computational time (especially in the CI context), we only considered 1 time step in the above loop. We encourage the interested user to download the notebook and run the simulation on more steps. Below we show an animation of the simulation outputs computed over 10 steps."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look at the resulting stress field and its quasi-static evolution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "\n",
    "Image(filename=\"../data/tutorial_results/tuto_7_result_3_paraview_snapshot.gif.png\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The animation above, generated from the **paraview** software, shows the stress field quasi-static time evolution across the ten steps of the simulation. The colors code for its intensity (blue = low and red = high). The ellopsoid depicts its local orientation and anisotropy (their radii are aligned in the eigenvectors directions and proportional to the corresponding eigenvalues)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Concluding remark\n",
    "With this simple *artificial* example we wanted to show how the main `bvpy.bvp` and `bvpy.ibvp` classes can be combined into complex models.\n",
    "\n",
    "Models inspired by real life systems can sometimes feature interplay between fields of various natures, *e.g.* scalar, vector or tensor; each featuring its own dynamics happening putatively at different time scales.\n",
    "This example illustrates how one can build such a complex system in a rather intuitive manner within the **BVPy** framework.\n",
    "Moreover, the fact that **FEniCS** objects and methods are directly accessible enables fine tuning and manipulations of these models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
