{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reaction-diffusion systems\n",
    "\n",
    "This tutorial tackles a classic and yet complex example of *bvp* highly influencial in mathematical biology: Reaction-diffusion systems. These encompases any *PDE* system featuring a Laplacian (the diffusion term) and a source (the reaction term), fonction of the unknown field(s). Their general form reads:\n",
    "$$\n",
    "\\partial_t\\phi = D\\Delta\\phi + f(\\phi)\n",
    "$$\n",
    "The reaction term $f(\\phi)$ can be highly non-linear. As a matter of fact, non-linear reaction terms can generate very complex dynamics and non-trivial stationnary solutions.\n",
    "\n",
    "Through this tutorial we want to show how **bvpy** can be used to simulate such equations. In particular we are going to implement two classic reaction-diffusion system: A wave-front propagation and a Turing pattern.\n",
    "\n",
    "\n",
    "**Covered topics:**\n",
    "\n",
    "- The `TransportForm` an `CoupledTransportForm` classes from the `bvpy.vforms` module.\n",
    "\n",
    "- Basic use of the `ibvp` class to implement initial-boundary-value problems.\n",
    "\n",
    "- Basic use of the `SolutionExplorer` class from the `bvpy.utils.post_processing` sub-module to handle simulation results.\n",
    "\n",
    "\n",
    "\n",
    "Download the notebook: [bvpy_tutorial_5](https://gitlab.inria.fr/mosaic/publications/bvpy/-/raw/develop/tutorials/bvpy_tutorial_5_turing_system.ipynb?inline=false)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Wave-front propagation\n",
    "\n",
    "Let's consider the following reaction-diffusion equation:\n",
    "$$\n",
    "\\begin{array}{c}\n",
    "\\partial_t \\phi = D\\Delta\\phi + f(\\phi) \\\\\n",
    "\\text{with:} \\quad\\quad f(\\phi) =  -\\displaystyle\\prod_{i=0}^{2}(\\phi-\\phi_i)\n",
    "\\end{array}\n",
    "$$\n",
    "\n",
    "For a deep understanding of the importance of this type of equation in biology and biochemistry, the interested reader is directed towards the books **Mathematical Biology I & II** by *J.D. Murray*."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Problem implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Domain:** Let's consider a simple continuous flat band of aspect ration 5 in the x direction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import Rectangle\n",
    "\n",
    "domain = Rectangle(length=1, width=.2, cell_size=0.02, dim=2, clear=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Vform:** To implement the reaction-diffusion equation, we will use the `TransportForm` class, where we will simply set a value for the diffusion coefficient and implement the polynomial expression of the reaction function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.vforms import TransportForm\n",
    "\n",
    "reaction_diffusion = TransportForm(diffusion_coef=.01, reaction= lambda u: -u*(u-.2)*(u-1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Boundary conditions:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions.dirichlet import dirichlet\n",
    "\n",
    "fixed_boundary_values = [dirichlet(1, \"near(x, 0)\"),\n",
    "                         dirichlet(0, \"near(x, 1)\")]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Initial condition:** Since time is an integration variable in *ibvps*, we need to set an initial condition. Since we want to simulate the propagation of an interface between two regions "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.pre_processing import create_expression\n",
    "\n",
    "init = create_expression(\"1+tanh(-x/.1)\", degree=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**IBVP implementation:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy import IBVP\n",
    "\n",
    "wave_propagation = IBVP(domain, reaction_diffusion, bc=fixed_boundary_values, initial_state=init)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time integration\n",
    "Once the problem is set, we can integrate it over a time intervalle $\\Delta t = [t_{min}, t_{max}]$ with a time resolution $dt$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_min = 0\n",
    "t_max = 29\n",
    "dt = 0.1\n",
    "\n",
    "wave_propagation.integrate(t_min, t_max, dt, '../data/tutorial_results/tuto_5_result_1.xdmf', store_steps=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">**Notes:** \n",
    "> - For `ibvps` saving the solution with the `save()` function is not required anymore for recording is made *de facto* by the `.integrate()` method.\n",
    "> - When the `store_steps` argument is set to `True`, all integration steps are recorded, otherwise only the final state is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solution visualization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visualization of temporal dynamics is not possible in the current version of **Bvpy**, we therefore need an external software to assess the qualitatively the result of the simulation. This can be done, for instance, with the help of the **Paraview** software."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "\n",
    "Image(filename='../data/tutorial_results/tuto_5_result_1_paraview_snapshot.gif.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The animated gif above shows the time evolution of the solution $\\phi\\colon(t,\\mathbf{x})\\to\\phi(t,\\mathbf{x})$ of the `wave_propagation` problem above. The heatmap encodes the solution amplitude: red for 1 and blue for 0."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More quantitative visual representation can be achieved with tools from our library. For instance, one might be interesteed in ploting the amplitude profiles $\\phi(t_i, \\mathbf{x}(\\xi))$ for various time steps $t_i$ and with $\\xi$ the curvilinear abscisse along a specific curved within the considered domain. For instance, we can plot the $\\phi$ profile at various times along the horizontal axe, *i.e.* the family of curves:\n",
    "\n",
    "$$\n",
    "\\begin{array}{rl}\n",
    "\\phi_i\\colon & [0, 1] \\mapsto [0, 1] \\\\\n",
    "             & x \\to \\phi(t_i, [x, 0])\n",
    "\\end{array}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solution analysis can be done with the help of the `SolutionExplorer` class from the `bvpy.utils.post_processing` sub-module. This class roughly wraps the FEniCS technicity and enable a series of basic manipulations in a intuitive way.\n",
    "\n",
    "One of its useful method is the `.geometric_filter()` one. from a simple geometric rule on the domain (*e.g.* \"x>=0\" ) it returns an array of the solution values on all the vertices contained within the corresponding sub-domain. For instance, here below, the geometric filter returns the solution on all vertices on the line \"y=0\". The `sort=\"x\"` argument insures that the list is sorted by increasing x value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.post_processing import SolutionExplorer\n",
    "\n",
    "concentration_over_time = []\n",
    "for concentration in wave_propagation.solution_steps.values():\n",
    "    concentration_over_time.append(SolutionExplorer(concentration).geometric_filter(axe=\"y\",\n",
    "                                                                                    threshold=0,\n",
    "                                                                                    comparator=\"==\",\n",
    "                                                                                    sort=\"x\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once this projection over the abscissae axe is done for each time step, one can plot the resulting list of arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fig = plt.figure(figsize=(15, 5))\n",
    "for i, c in enumerate(concentration_over_time[::30]):\n",
    "    plt.plot(c, marker=\"o\", label=f\"{30*i}th\")\n",
    "\n",
    "\n",
    "plt.xlabel('Abscissae (A.U.)')\n",
    "plt.ylabel('Concentration (A.U.)')\n",
    "plt.title('Concentration horizontal distribution over time.', fontsize=16)\n",
    "fig.legend(loc='right', title=\"time steps:\")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Turing patterns\n",
    "\n",
    "Let's now try to implement another iconic example of *ibvp* in mathematical biology: Turing reaction-diffusion systems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Such system are usually described as a coupled set of two  *PDEs* depicting the spatio-temporal dynamics of two scalar fields. Their general form reads:\n",
    "$$\n",
    "\\begin{cases}\n",
    "\\partial_t a = D_a \\Delta a + r_a(a, b) \\\\\n",
    "\\partial_t b = D_b \\Delta b + r_b(a, b) \\\\\n",
    "\\end{cases}\n",
    "$$\n",
    "where the *reaction terms* $r_a(a,b)$ and $r_b(a,b)$ are usually highly non-linear smooth functions.\n",
    "\n",
    "\n",
    "In chemistry, biochemistry and developmental biology such systems can formalize chemical componds diffusing and reacting with each other, typically in a catalytic manner.\n",
    "\n",
    "In the present example, we are considering one specific case of Turing system: The **Lengyel-Epstein model** [(Lengyel & Epstein, Science (1991))](https://dx.doi.org/10.1126/science.251.4994.650) which has been recently used to investigate the zebra pattern formation [(Jeong *et al*, Statistical Mechanics and its Applications (2017))](https://dx.doi.org/10.1016/j.physa.2017.02.014).\n",
    "\n",
    "The particularity of the **Lengyel-Epstein model** is to use very specific forms for the reaction terms:\n",
    "$$\n",
    "r_a(a,b)=k_a b\\Big(1-\\frac{a}{1+b^2}\\Big) \\quad\\quad \\text{and} \\quad\\quad r_b(a,b) = k_b -b -\\frac{4a}{1+b^2}.\n",
    "\\label{eq:LE_model_source_terms}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Domain definition and boundary conditions\n",
    "\n",
    "Let's consider a simple square domain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import Rectangle\n",
    "\n",
    "skin_patch = Rectangle(length=10, width=10, cell_size=0.2, dim=2, clear=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But with periodic boundary conditions to ease the pattern formation.\n",
    "\n",
    "Such boundary conditions can be implemented thanks to the `SquarePeriodicBoundary` from the `bvpy.boundary_conditions.periodic` sub-module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions.periodic import SquarePeriodicBoundary\n",
    "\n",
    "periodic_bc=SquarePeriodicBoundary(length=10, width=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variational formulation of the Lengyel-Epstein model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As demonstrated in the previous section, reaction-diffusion problems can be implemented with the `TransportForm`. But in the present case, we are interested in the system of two reaction-diffusion equation that are coupled to each other. We derived a specific class to deal with such problems: The `CoupledTransportForm` from the `bvpy.vforms.transport` sub-module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.vforms import CoupledTransportForm\n",
    "\n",
    "LE_model = CoupledTransportForm()\n",
    "LE_model.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the `.info()` method above, we get that we need to define three things:\n",
    "- A diffusion coefficient.\n",
    "- A reaction term.\n",
    "- A source term.\n",
    "\n",
    "For the reaction terms we will use the functions given in expression ($\\ref{eq:LE_model_source_terms}$) and we will take a null source term.\n",
    "> **Note:** The difference that is usually made between a *source* and a *reaction* term is that the former is either constant or position dependent on the considered domain whereas the latter is explicitly a function of the unkown field, *i.e.* $s=s(\\mathbf{x})$ and $r=r(a,b)$.\n",
    "\n",
    "The particularity of the `CoupledTransportForm` is that it features a `.add_species()` method that enables to add as much equations (and the corresponding unknown fields) as desired. The arguments of this methods are:\n",
    "- The name of the unknown.\n",
    "- All the coefficients (*i.e.* diffusion coefficient, soure and reaction terms) of the corresponding equations that need to be set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Da, Db = 1, .02\n",
    "ka, kb = 9, 11\n",
    "\n",
    "LE_model.add_species('a', Da, lambda a, b: ka*(b-(a*b)/(1+b*b)))\n",
    "LE_model.add_species('b', Db, lambda a, b: kb-b-(4*a*b)/(1+b*b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Notes:** \n",
    "> - As shown above, we can pass any function that we want to define the required coefficients.\n",
    "> - The source term is set to 0 by default, see method documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initial conditions\n",
    "We will start here from two random distributions $a_0(\\mathbf{x})$ and $b_0(\\mathbf{x})$ for the unknown fields."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from bvpy.utils.pre_processing import create_expression\n",
    "\n",
    "np.random.seed(1093)\n",
    "skin_patch.discretize()\n",
    "random = np.random.uniform(low=-1, high=1, size=skin_patch.mesh.num_entities(2))\n",
    "\n",
    "random_pattern = [[1+0.04*ka**2+0.1*r, 0.2*kb+0.1*r] for r in random]\n",
    "\n",
    "initial_pattern = create_expression(random_pattern)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** to generate the random pattern we need a grid of points. These points correspond to the vertices of the meshed domain. But at this stage, the domain is not meshed yet. This meshing procedure is performed by the `.discretize()` method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Problem instanciation and integration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.ibvp import IBVP\n",
    "from bvpy.solvers.time_integration import ImplicitEuler\n",
    "\n",
    "turing_system = IBVP(skin_patch, LE_model, periodic_bc, initial_pattern)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t_min = 0\n",
    "t_max = 9\n",
    "dt = 0.1\n",
    "\n",
    "save_path = '../data/tutorial_results/tuto_5_result_2.xdmf'\n",
    "\n",
    "turing_system.integrate(t_min, t_max, dt, save_path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** With *ibvps* no need to call the `save()` function from the `bvpy.utils.io` sub-module for the `.integrate()` method can save  the results directly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Results visualization and analysis\n",
    "Let's have a look at the final patterns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.visu import plot\n",
    "final_pattern_a = turing_system.solution.sub(0)\n",
    "final_pattern_b = turing_system.solution.sub(1)\n",
    "\n",
    "plot(final_pattern_a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(final_pattern_b, colorscale=\"viridis\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look at the whole dynamics, recorded as a `gif` within **Paraview**:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "\n",
    "Image(filename='../data/tutorial_results/tuto_5_result_2_paraview_snapshot.gif.png')\n",
    "#from IPython.display import HTML\n",
    "#HTML('<img src=\"../data/tutorial_results/tuto_5_result_2_paraview_snapshot.gif\">')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
