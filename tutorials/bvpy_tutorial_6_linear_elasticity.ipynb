{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linear Elasticity\n",
    "\n",
    "\n",
    "This tutorial covers some basic example of linear elastic problems and their corresponding implementation within the **Bvpy** library. We will start, in the first part with some basic introduction to the theory of linear continuum mechanics. In a second part, we will how to implement 2D in-plane mechanical equilibria of continuous structures. In a third part, we will see how this can be extended to study equilibria of 2D structures bent in 3D.\n",
    "\n",
    "\n",
    "**Covered topics:**\n",
    "\n",
    "- Data visualization: `plot()` and `set_renderer()` from `bvpy.utils.visu`.\n",
    "\n",
    "- Data importation and exportation: `CustomDomain.read()`, `CustomPolygonalDomain.read()` and `bvpy.utils.io.save()`\n",
    "\n",
    "- Linear elasticity variational form manipulation: `LinearElasticVform()`, `set_parameters()`, `info()`, `stress()`.\n",
    "\n",
    "- Geometrical domain construction (CSG).\n",
    "\n",
    "- Boundary conditions definitions and combinaison: `NormalDirichlet` and `NormalNeumann`, `bvpy.boundary_condition.dirichlet.dirichlet()`\n",
    "\n",
    "\n",
    "\n",
    "Download the notebook: [bvpy_tutorial_6](https://gitlab.inria.fr/mosaic/publications/bvpy/-/raw/develop/tutorials/bvpy_tutorial_6_linear_elasticity.ipynb?inline=false)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Let's consider a $n$ dimensional (with $n \\in \\{2,3\\}$) smooth differential manifold representing a continuous piece of material. At rest (*i.e.* with no mechanical force applied on it}), this manifold is geometrically described by a *resting configuration* noted $\\Omega$.\n",
    "When loaded with a volumic force field $\\mathbf{b}(\\mathbf{X})$ , this material deforms and eventually reaches an *equilibrium configuration*, noted $\\omega$. A displacement field, noted $\\mathbf{u}(\\mathbf{X})$ maps both configurations:\n",
    "\n",
    "$$\n",
    "\\begin{array}{rl}\n",
    "    \\mathbf{u}: \\Omega &\\mapsto \\omega \\\\\n",
    "    \\mathbf{X} &\\to \\mathbf{x}\n",
    "\\end{array}\n",
    "$$\n",
    "\n",
    "The mechanical response of the material to loading is dictated by its instrinsic *rheological properties*. Such properties are mathematically described by *constitutive equations* that relates *kinematic* fields (*e.g.* displacement, deformation or deformation rate) to *dynamic* fields (*i.e.* mechanical forces, stresses or torques). In the case of elastic materials, we assume that de material *strain* field depends linearly on the *stress* field, this is known as Hooke's law: \n",
    "\n",
    "$$\n",
    "\\boldsymbol{\\sigma} = \\lambda\\textrm{tr}(\\boldsymbol{\\varepsilon})\\mathbf{I}_{\\text{d} }+2\\mu\\boldsymbol{\\varepsilon}\n",
    "\\label{eq:constitutive_equation}\n",
    "$$\n",
    "\n",
    "where $\\boldsymbol{\\sigma}(\\boldsymbol{x})$ represents the stress field undergone by the material in its equilibrium configuration $\\Omega$ and $\\boldsymbol{I}_{\\text{d}}$ stands for the identity tensor. $\\lambda$ and $\\mu$ corresponds to rheological parameters called *Lame's coefficients*. The strain field undergone by the material is noted $\\boldsymbol{\\varepsilon}(\\boldsymbol{x})$. In the context of linear elasticity, it is assumed to depend linearly on the deformation field gradient:\n",
    "\n",
    "$$\n",
    "\\boldsymbol{\\varepsilon} = \\frac{1}{2}(\\nabla \\mathbf{u} +\\nabla \\mathbf{u}^T)\n",
    "\\label{eq:linear_strain}\n",
    "$$\n",
    "\n",
    "The mechanical equilibrium configuration is characterized by the linear and angular momentum balances:\n",
    "\n",
    "$$\n",
    "\\left\\{\n",
    "\\begin{array}{rl}\n",
    "-\\nabla\\cdot\\boldsymbol{\\sigma} &= \\mathbf{b}  \\\\\n",
    "\\boldsymbol{\\sigma} &= \\boldsymbol{\\sigma}^t\n",
    "\\end{array}\n",
    "\\label{eq:mechanical_equilibrium}\n",
    "\\right.\n",
    "$$\n",
    "\n",
    "Finally, to complete the picture and set the corresponding *Boundary-Value Problem*, one has to specif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## In-plane mechanics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "###  Stretching of a rubber band\n",
    "Let's try first, one of the simplest linear elasticity problem: The stretching of a rubber band."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Domain definition\n",
    "We are considering a flat rectangular rubber band with homegeneous, isotropic and linear elastic properties. The idea is to apply a deformation to its border and compute the resulting equilibrium shape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import Rectangle\n",
    "\n",
    "rubber_band = Rectangle(length=3, cell_size=.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.visu import plot, set_renderer\n",
    "\n",
    "set_renderer('notebook')\n",
    "plot(rubber_band)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Vfom\n",
    "Let's assume this *rubber band* has some **homogeneous and isotropic linear elastic properties**, characterized by a Young's modulus $Y$ and a Poisson's ration $\\mu$. This rheological behavior can be encoded through the corresponding vform class: `LinearElasticForm` from the `bvpy.vform.elasticity` sub-module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.vforms import LinearElasticForm\n",
    "\n",
    "elastic_response = LinearElasticForm(young= 1000, poisson=.5, source=[0, 0, 0], plane_stress=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, the `.info()` method inherited from the `AbstractVform` class can be used to gain insight on the properties of the instanciated form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "elastic_response.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Boundary conditions\n",
    "The idea is to study how such material would react if stretched in the horizontal direction. To do so, we will apply the following boundary conditions to our system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions import dirichlet\n",
    "\n",
    "traction = [dirichlet([-.01, 0, 0], boundary='near(x, 0)'),\n",
    "            dirichlet([.01, 0, 0], boundary='near(x, 3)')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Problem assembly and resolution\n",
    "It is now time to combine the domain, vform and boundary conditions into a boundary-value problem:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy import BVP\n",
    "\n",
    "stretching = BVP(rubber_band, elastic_response, traction)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use the `info()` method from the `BVP()` class. It gathers informations from the `info()` methods of the `domain` and `vform` arguments of the class as well as information genuine to the problem:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stretching.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it is time to solve it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stretching.solve()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Solution visualization\n",
    "We can have a have a quick glance at the results with the `plot()` function from the `bvpy.utils.visu` sub-module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(stretching.solution, size=1e-2, norm=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The users with experience in any mesh visualization software, can also export the simulations in the `.xdmf` format and process it at will elsewhere. This can be done with the `save()` function from the `bvpy.utils.io` sub-module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.io import save\n",
    "\n",
    "path = '../data/tutorial_results/tuto_6_result_1.xdmf'\n",
    "save(stretching, path)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython.display import Image\n",
    "\n",
    "Image(filename='../data/tutorial_results/tuto_6_result_1_paraview_snapshot.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The image above shows the results of the simulation displayed within the **Paraview** software. The arrows depict the displacement field and the heatmap codes for its intensity. The triangular mesh is visible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Abstract \"wound healing\" simulation\n",
    "From a purely topological perspective, a wound in an epidermal tissue can be formalized by a hole in a otherwise continuous surface. Such topological change in a continuum can have drastic consequences  in terms of biomechanics. Let's see how a round hole modifies the stress distribution within a 2D tissue under tension."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Domain\n",
    "To construct a complex domain from simple geometrical primitives, one can make use of CSG (Constructive Solid Geometry) abilities of the `bvpy.domains.primitive` sub-module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import Disk\n",
    "\n",
    "big = Disk(radius=2, clear=True, dim=2)\n",
    "sml = Disk(radius=.5, dim=2)\n",
    "wound = big - sml\n",
    "wound.set_cell_size(.05)\n",
    "\n",
    "plot(wound)\n",
    "wound.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** You can see above that, contrary to the first example (*rubber band*), the dimension of the domain has been set to 2, thanks to the `dim=2` argument. Indeed, purely 2D problems can be implemented as such. This can be of interest when quantitative stress analysis is required."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### boundary conditions\n",
    "Let's assume that this structure is loaded with a tensile force field defined on its outermost boundary. This corresponds to the application of *Neumann boundary conditions*. This can be done thanks to the corresponding class in the `bvpy.boundary_conditions` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions import NormalDirichlet\n",
    "tensile_forces = NormalDirichlet(val=1, boundary='near(x*x + y*y, 4, 1e-2)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Vform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "elastic_response = LinearElasticForm(young= 1000, poisson=.5, source=[0, 0], plane_stress=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Problem and solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wound_healing = BVP(wound, elastic_response, tensile_forces)\n",
    "wound_healing.solve()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can quickly assess the results by ploting them and save them for further investigation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wound_displacement = wound_healing.solution\n",
    "plot(wound_displacement, size=1)\n",
    "path = '../data/tutorial_results/tuto_6_result_2.xdmf'\n",
    "save(wound_healing, path)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One think that is of particular interest in this case, is the stress field generated, within the medium, in response to the stretching applied at the borders. The `LinearElasticForm` class has a method to compute it from the deformation field solution of the corresponding `BVP`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stress field computation and visualization\n",
    "What would be very interesting is to check the stress field generated by this streching within the medium:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All classes inheriting from the *elastic mother class* `ElasticForm` possess `stress()` and `strain()` methods that computes the corresponding fields from the definitions given in the static methods `_stress()` and `_strain()`. These latters, depict the rheological response one wants to implement through the considered class and are to be implemented when a new elastic class is defined.\n",
    "\n",
    "The implementation of the `_stress()` and `_strain()` methods are conducted by the developper of the corresponding class. Once this is done, the end user only has to call the corresponding *public* methods `stress()` and `strain()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import fenics as fe\n",
    "\n",
    "stress = elastic_response.stress(wound_displacement)\n",
    "isinstance(stress, fe.Function)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see from above the generated  *stress* is a **fenics.Function** object. This means that it can be handled by **bvpy** functions from the `bvpy.utils` module, such as `plot()` and/or `save()`. \n",
    "\n",
    "> **Note:** In the current version of the library, second order tensor visualization has not yet been optimized and we suggest to use a third party software, such as **Paraview(PUT LINK HERE)** in this case. However, in order to do so, one needs first to export the stress field data into a generic format, such as the `.ply` one. This can be done with the `save()` function from the `bvpy.utils.io` sub-module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "save(stress, '../data/tutorial_results/tuto_6_result_3.xdmf')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image(filename=\"../data/tutorial_results/tuto_6_result_3_paraview_snapshot.png\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> The image above is a snapshot taken from **Paraview** that shows the `stress` tenor field (computed above) visualization. Stress tensors have been depicted as small 3D ellipsoid thanks to the **Tensor Glyph** filter within **Paraview**. The color depicts the stress intensity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stretching of a tissue cross-section\n",
    "\n",
    "Let's now consider a data structure describing a two-dimensional cellularized tissue. This data structure is recorded on disk in the `ply` format and can be used to instanciate a **bvpy** domain thanks to the `CustomPolygonalDomain` from the `bvpy.domains.custom_polygonal` sub-module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import CustomPolygonalDomain\n",
    "from bvpy.utils.visu import plot\n",
    "\n",
    "tissue = CustomPolygonalDomain.read(\"../data/example_domain_sepal_2D_slice.ply\",\n",
    "                                    cell_size=.5,\n",
    "                                    dim=2)\n",
    "tissue.discretize()\n",
    "\n",
    "plot(tissue.cdata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What we want to explore here is the ability to use heterogeneous parameter fields within our equations. To illustrate this, let's assume that the elastic properties of the cells composing the considered tissue vary between the epidermis and the inner part of the tissue, respectively in light yellow and black in the figure above.\n",
    "This can be coded that's to the `HeterogeneousParameter` class from the `bvpy.utils.pre_processing` sub-module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.pre_processing import HeterogeneousParameter\n",
    "\n",
    "young_values_by_labels = {0: 100, 1: 300}\n",
    "\n",
    "heterogeneous_young = HeterogeneousParameter(tissue.cdata, young_values_by_labels)\n",
    "heterogeneous_elastic_response = LinearElasticForm(young=heterogeneous_young,\n",
    "                                                   source=[0, 0],\n",
    "                                                   plane_stress=True)\n",
    "\n",
    "heterogeneous_elastic_response.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This time we are going to try another way of imposing external constrains on a structure. Now let's assume that we are imposing a force field normal to the border of the domain. This corresponds to *Neumann boundary conditions* and can be implemented thanks to the `bvpy.boundary_conditions.neumann` sub-module. In particular, we are going to use the  `NormalNeumann` class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.boundary_conditions import NormalNeumann\n",
    "pressure = NormalNeumann(val=1, boundary='all')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prblm = BVP(tissue, heterogeneous_elastic_response, pressure)\n",
    "prblm.solve(linear_solver='gmres', absolute_tolerance=1e-2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.io import save\n",
    "displacement = prblm.solution\n",
    "#plot(displacement, size=1, norm=True)\n",
    "\n",
    "save(displacement, '../data/tutorial_results/tuto_6_result_4.xdmf')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stress field analysis\n",
    "To conclude with this first *bio-inspired* example, it could be interesting to show how quantitative analysis can be performed on simulation results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this perspective, let's compare the stress field basic characteristics (*e.g.* amplitude and anisotropy) between the epidermis and the inner tissues. \n",
    "\n",
    "Let's first compute the stress field and take a look at it (within **Paraview**):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stress = heterogeneous_elastic_response.stress(displacement)\n",
    "save(stress, \"../data/tutorial_results/tuto_6_result_5.xdmf\")\n",
    "Image(filename=\"../data/tutorial_results/tuto_6_result_5_paraview_snapshot.png\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    ">The image above display a snapshot from *Paraview* where the `stress` and the `displacement` fields computed above are displayed together. Stress tensors have been depicted as small 3D ellipsoid thanks to the **Tensor Glyph** filter within **Paraview**. The color depicts the stress intensity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the above visulization, we see that the two zones defined within the tissue and assigned with different values of Young's modulus experience very different stress fields.\n",
    "\n",
    "One might want to quantitatively study the differences between stresses within these two zones.\n",
    "\n",
    "To illustrate such analysis, we will show in the following cells how to extract and process simulation outputs."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One basic analysis we want to performe is to compute the stress field intensity on our structure and compare its values between the two sub-regions *epidermis* and *inner tissue*. These regions are marked within the data structure by different `label` values, respectively 1 and 0. These labels are stored within the `domain.cdata` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.utils.post_processing import SolutionExplorer\n",
    "\n",
    "stress_function = SolutionExplorer(stress)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "epiderm_stress = stress_function.cell_label_filter(tissue.cdata, 1)\n",
    "inner_stress = stress_function.cell_label_filter(tissue.cdata, 0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -- Let's compute the intensity and the anisotropy of the stress tensor field.\n",
    "from bvpy.utils.tensor import intensity, anisotropy\n",
    "\n",
    "epiderm_strs_int = list(map(lambda x: intensity(x), epiderm_stress))\n",
    "inner_strs_int = list(map(lambda x: intensity(x), inner_stress))\n",
    "\n",
    "epiderm_strs_ani = list(map(lambda x: anisotropy(x), epiderm_stress))\n",
    "inner_strs_ani = list(map(lambda x: anisotropy(x), inner_stress))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# -- Let's plot the results\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "fig, (ax0, ax1) = plt.subplots(1, 2, sharey=True, figsize=(15, 5))\n",
    "\n",
    "ax0.hist(epiderm_strs_int, bins=100, color=\"r\", alpha=.5, label=\"Epidermis\")\n",
    "ax0.hist(inner_strs_int, bins=100, color=\"b\", alpha=.5, label=\"Inner tissue\")\n",
    "ax0.legend()\n",
    "ax0.set_title('Stress intensity (A.U.)')\n",
    "\n",
    "ax1.hist(epiderm_strs_ani, bins=100, color=\"r\", alpha=.5, label=\"Epidermis\")\n",
    "ax1.hist(inner_strs_ani, bins=100, color=\"b\", alpha=.5, label=\"Inner tissue\")\n",
    "ax1.legend()\n",
    "ax1.set_title('Stress anisotropy (no units)')\n",
    "\n",
    "    \n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deformation of a pressurized shell\n",
    "In the previous examples, we considered flat 2D structures that were loaded either with displacement (*Dirichlet*) or force (*Neumann*) fields. But all considered fields were only applied on the borders of the structure and within its tangent plane.\n",
    "\n",
    "We are going to extend these cases here by considering 2D structures loaded with (force) fields not restricted either to their outer borders nor contained within their tangent plane."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The continuous case\n",
    "\n",
    "Let's first consider a simple example: The mechanical equilibrium of an hemispherical elastic shell loaded with pressure-induced forces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### The domain\n",
    "The hemisphere is easily generated from the `bvpy.domains.primitive` sub-module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains import HemiSphere\n",
    "dome = HemiSphere(cell_size=.1, clear=True)\n",
    "plot(dome)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### The Vform\n",
    "\n",
    "We are assuming that the considered shell has a linear, isotropic and homogeneous elastic behavior."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the present case, deformations are due to tissue-induced loading forces.\n",
    "\n",
    "Such forces are aligned along the normals of the considererd surface and proportional to the pressure difference between the two sides of the surface, oriented from the highest to the lowest value. In the present example we assume that the inner volume (*i.e.* in the concave half-space delimited by the hemisphere) experiences an over-pressure `P` compared to the outside of the hemisphere.\n",
    "\n",
    "Such a vector field, normal to a given surface with a specific amplitude, can be implemented thanks to the `normal` function defined within the `bvpy.domains.geometry` sub-module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from bvpy.domains.geometry import normal\n",
    "\n",
    "pressure = 1000\n",
    "pressure_force = normal(dome, scale=pressure)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since this definition of the pressure force must apply to the whole surface and not only its border, one can not implement these loading forces as boundary conditions. We need to implement them as *a source term* in the variational formulation itself. This can be done for the `LinearElasticForm` accept such a source term as argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "elastic = LinearElasticForm(young= 1000, poisson=.5, source=pressure_force, plane_stress=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Boundary conditions\n",
    "\n",
    "The only restriction we want to impose to the structure is a fixed center."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because of the symmetry of the problem, this can be implemented by combining three conditions preventing three orthogonal planes to move along their normals. Such a combination of boundary conditions can easily be implemented as a list of *nuclear conditions*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fixed_center = [dirichlet(0, boundary='near(x, 0)', subspace=0),\n",
    "                dirichlet(0, boundary='near(y, 0)', subspace=1),\n",
    "                dirichlet(0, boundary='near(z, 0)', subspace=2)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Problem definition and resolution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pressurized_shell = BVP(dome, elastic, fixed_center)\n",
    "pressurized_shell.solve()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "displacement = pressurized_shell.solution\n",
    "\n",
    "save(displacement, '../data/tutorial_results/tuto_6_result_6.xdmf')\n",
    "\n",
    "plot(displacement, size=.25, norm=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The piecewise polygonal  case\n",
    "\n",
    "We want now to implement a mechanical model on a 2D curved structure that mimicks a multicellular tissue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Domain\n",
    "To that end, we are representing a curved piecewize-polygonal surface from an existing `.ply` file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tissue = CustomPolygonalDomain.read('../data/example_domain_curved_cellularized_tissue.ply')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Vform\n",
    "We describe our tissue as a linear isotropic and homogeneous material, loaded with pressure forces alined, as previously, along the surface normals:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pressure = 1000\n",
    "pressure_force = normal(tissue, scale=pressure)\n",
    "\n",
    "elastic = LinearElasticForm(young= 1000, poisson=.5, source=pressure_force, plane_stress=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Boundary conditions\n",
    "In this specific example, we are going to assume simple fixed boundary conditions on the rim of the structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fixed_rim = dirichlet([0, 0, 0], boundary='all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Problem instanciation and resolution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prblm.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prblm = BVP(tissue, elastic, fixed_rim)\n",
    "prblm.solve()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "displacement = prblm.solution\n",
    "\n",
    "save(displacement, '../data/tutorial_results/tuto_6_result_7.xdmf')\n",
    "\n",
    "plot(displacement, size=1000, norm=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
