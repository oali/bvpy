=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* Florian Gacon, <florian.gacon@inria.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* ALI Olivier <olivier.ali@inria.fr>

.. #}
