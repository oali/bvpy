---
title: 'BVPy: A FEniCS-based Python package to ease the expression and study of boundary value problems in Biology.'
tags:
  - Python
  - Biomechanics
  - Finite Element Method
  - Boundary Value Problem
  - FEniCS
  - Conda
authors:
  - name: Florian Gacon
    orcid: 0000-0002-1873-5246
    affiliation: 1
  - name: Christophe Godin
    orcid: 0000-0002-1202-8460
    affiliation: 1
  - name: Olivier Ali^[Corresponding Author]
    orcid: 0000-0001-7671-7225
    affiliation: 1
affiliations:
  - name: Laboratoire Reproduction et Développement des Plantes, Université de Lyon, Ecole Normale Supérieure de Lyon, UCB Lyon 1, CNRS, INRAE, INRIA;  46, allée d'Italie, 69364 LYON Cedex 07, France.
    index: 1
date: 22 October 2020
bibliography: paper.bib
---

# Summary

BVPy is a python library to easily implement and study numerically Boundary Value Problems (*BVPs*) and Initial Boundary Value Problems (*IBVPs*) through the Finite Element Method (*FEM*). BVPy proposes an intuitive Application Programming Interface (*API*) to harness and combine the core functionalities of three powerful libraries: FEniCS [@Logg:2012] provides the core data structures and solving algorithms; Gmsh [@Geuzaine:2009] defines the domains and their meshing; and Meshio [@Schlomer:2020] handles data reading and writing. Initially built in the context of developmental biology and morphomechanics, its purpose is to enable all users, even with little to none experience in *FEM*, to quickly and efficiently estimate the behavior of a wide variety of fields (scalars, vectors, tensors) on biologically relevant structures, inspired by biophysical and biochemical processes (morphogene patterning, active matter mechanics, active transports...). Despite this biological motivation, the BVPy library has been implemented in an *agnostic* manner that makes it suitable for many other scientific context.

# Statement of need

*FEMs* are becoming an ubiquitous tool in many research areas and the need for a platform accessible to a large audience of non-specialists is growing. Such platforms should: *(i)* provide an easy access to top-tier, open source, existing libraries, usually restricted to expert users; *(ii)* serve as a means for experienced researchers to communicate with and educate novices. BVPy aims to fulfil these needs through a twofold strategy:

* Provide a high-level *API* to soften the learning curve of *FEMs*. So users with little to none knowledge can parametrize, run and monitor simulations based on built-in templates.
* Enable users experienced in *FEM*-based modeling to develop and fully customize *de novo* templates that could, in turn, be used by non-specialists.

#  State of the field

Although an exhaustive inventory of the existing *FEM* solutions is far beyond the reach of this paper, it is worth mentioning a few existing alternatives; especially in the context of computational morphomechanics, in which BVPy has been developed.

Some *FEM*-frameworks provide *GUI* such as Sofa [@faure:hal-00681539], FEBio [@Maas_2012] or MorphoMechanX (see [@Sapala:2018gl] for example of use) the yet-to-be published add-on to the MorphoGraphX software [@deReuille:2015gu]. While such userfriendly solutions are accessible to a wide range of users; their "monolithic" construction reduces versatility and prevents more experienced users to tune them at will. Moreover, their code sources are not always freely available.

Besides these *GUI*-based solutions, some "lighter" and more open frameworks, such as FREEFEM [@hecht_2012] or FEniCS [@Logg:2012] are also available. Such solutions appear more versatile and opensource. But their use is usually restricted to people already familiar with the theory of Finite Elements.

A gap currently exists between "closed", userfriendly, softwares on one hand and "open", technical frameworks on the other. BVPy attempts to fill this gap. Our strategy was to harness FEniCS richness into an intuitive and evolutive *API*. To that end, the library architecture maps the conceptual mathematical components of *BVPs* to abstract classes built on FEniCS and Gmsh core components (see the library general description below). From these abstract classes, experienced users can implement *concrete* classes, dedicated to their very specific need. Once implemented, these *concrete* classes can easily be instantiated and combined in a manner that do not require specific *FEM*-related skills (see the Example of use below). Although the library comes with a few of these *concrete* classes, covering *classic* equations (*e.g.* Poisson's and Helmholtz's equations, linear elasticity and hyperelasticity, reaction-diffusion equations...); the full potential of the library resides in its ability to integrate *de novo* classes addressing genuine equations and problems.



#  Library general description

By definition, a *BVP* corresponds to a (set) differential equation(s) together with a set of constraints defined on the boundaries of the integration domain, see \autoref{eq:bvp}.

\begin{equation}\label{eq:bvp}
\begin{cases}
F(u, \boldsymbol{\nabla}(u) \dots) = 0 & \text{in} \quad \Omega \\
u(\boldsymbol{x}) = u_N(\boldsymbol{x}) & \text{on} \quad \partial\Omega_N \\
\boldsymbol{\nabla}(u(\boldsymbol{x})) = \boldsymbol{j}(\boldsymbol{x}) & \text{on} \quad \partial\Omega_D
\end{cases}
\end{equation}

The above formalization can be extended as follow in the case of first-order *IBVPs* (*i.e.* where time derivatives are limited to first order):

\begin{equation}\label{eq:ibvp}
\begin{cases}
\partial_t u = F(u, \boldsymbol{\nabla}(u) \dots) & \text{in} \quad \Omega \\
u(t_0, \boldsymbol{x}) = u_0(\boldsymbol{x}) & \text{on} \quad \Omega_{t_0}\\
u(t,\boldsymbol{x}) = u_N(t,\boldsymbol{x}) & \text{on} \quad \partial\Omega_N\\
\boldsymbol{\nabla}(u(t,\boldsymbol{x})) = \boldsymbol{j}(t,\boldsymbol{x}) & \text{on} \quad \partial\Omega_D,
\end{cases}
\end{equation}

the integration domain and its boundaries can be split into one-dimension *time-*line and orthogonal *spatial* hyperplanes: $\Omega = [t_0, t_{\infty}]\times\Omega_{t_0}$, $\partial\Omega_N=[t_0, t_{\infty}]\times\partial\Omega_{t_0N}$ and $\partial\Omega_D=[t_0, t_{\infty}]\times\partial\Omega_{t_0D}$. Assuming that the spatial hyperplane $\Omega_{t_0}$ does not evolve with time, such *IBVPs* can be implemented by coupling the resolution of time-dependent *BVPs* with time-integration schemes.


## Scope and range

**In terms of equations:** In its current version (1.0.0), BVPy can handle non-homogeneous second order Partial Differential Equations (*PDEs*) with potentially non-linear first order terms but linear higher order ones. The unknowns within these *PDEs* can be scalar fields, vector fields or second-order tensor fields. Systems of coupled *PDEs* can be implemented. We extended the initial notion of *BVP* to encompass *IBVPs* featuring first-order time derivatives, as described by \autoref{eq:ibvp}.

**In terms of domains:** BVPy provides geometrical primitives to generate simple 2D and 3D domains (rectangles, cubes, ellipoids, torus, ...) as well as basic Constructive Solid Geometry (*CSG*) functionalities (addition, substraction and intersection) to combine these primitives into more complex geometries. The library can also handle triangulated, as well as piecewise-polygonal, meshes generated elsewhere. In the current version, `.txt` , `.ply` and `.obj` files are accepted as inputs. This versatility comes however with a drawback, for the domain generation procedure is not yet compatible with parallelization processes.

**In terms of boundary conditions:** Classic *Dirichlet* and *Neumann* boundary conditions can be implemented as well as combinations of these along the domain boundaries. Periodic boundary conditions can also be defined.

## Organization

BVPy has been developed and organized as close as possible to the *BVP*  and *IBVP* mathematical formulations given in \autoref{eq:bvp} and \autoref{eq:ibvp}. The library is built around the following key components, see \autoref{fig:schema}:

*  Two main classes, named `BVP` and `IBVP`, that respectively encapsulate FEniCS implementations of \autoref{eq:bvp} \& \autoref{eq:ibvp}.
*  Three main modules, `bvpy.domains`, `bvpy.vforms` and `bvpy.boundary_conditions`  emulating the corresponding mathematical components of a *bvp*.
*  A `bvpy.solvers` module where a collection of FEniCS-based linear and non-linear solvers are available.

Besides these main components, the `bvpy.utils` module gathers some useful "housekeeping" functions regrouped thematically into sub-modules, *e.g.* `visu`, `io`... See online documentation.

![Representation of the main modules and submodules forming the BVPy library. \label{fig:schema}](./fig1.png)


## Example of use

The following example is loosely inspired by [@Zhao:2020]. The idea is to estimate the mechanical stress distribution within a 2D cross section of a pressurized plant tissue with heterogeneous rigidity. The goal here is to illustrate how parsimonious, intuitive and yet insightful, BVPy simulations can be.

```python
from bvpy import *

# domain definition and meshing
sepal = CustomPolygonalDomain.read("./sepal.ply", cell_size=.5, dim=2)
sepal.discretize()

# instanciation of an heterogeneous linear elastic model
tissue_rigidities = {0: 100, 1: 300}
young_moduli = HeterogeneousParameter(sepal.cdata, tissue_rigidities)
elastic_response = LinearElasticForm(young=young_moduli, source=[0, 0],
                                     plane_stress=True)

# loading forces implemented as Neumann boundary conditions
inner_pressure = 1
pressure_forces = NormalNeumann(val=inner_pressure, boundary='all')

# problem definition and resolution
prblm = BVP(sepal, elastic_response, pressure_forces)
prblm.solve(linear_solver='gmres', absolute_tolerance=1e-2)

# stress field computation
displacement = prblm.solution
stress = elastic_response.stress(displacement)
```

The recorded results can be processed by third-party software, *e.g.* \autoref{fig:sepal_stress} shows the visualization of the corresponding stress field within the Paraview software.

![Example of a stress field computed from an heterogeneous linear elastic model applied to a piecewise polygonal domain. The epidermis layer has been assumed three times stiffer than the inner tissues. A corresponding distinction can be seen within the computed stress field. \label{fig:sepal_stress}](./fig2.png)

More examples are available in the tutorial section of the online documentation.

# Acknowledgements

The authors would like to thank Guillaume Cerutti and Jonathan Legrand for their numerous advices and their technical guidance during the maturation of this work.

This work was supported by the Inria grant *ADT Gnomon*.


# References
