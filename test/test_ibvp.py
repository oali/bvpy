import unittest

from bvpy import IBVP, logger
from bvpy.domains import Rectangle
from bvpy.vforms import PoissonForm
from bvpy.solvers.time_integration import ExplicitEuler
from bvpy.boundary_conditions import dirichlet
from bvpy.utils.pre_processing import create_expression

import tempfile


class TestPoisson(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)
        self.domain = Rectangle(x=-2, y=-2, length=4, width=4, cell_size=0.05)
        self.vform = PoissonForm(source=0)
        self.bc = dirichlet(0, boundary="all")
        self.dir = tempfile.TemporaryDirectory()

    def tearDown(self):
        """Concludes and closes the test.
        """
        self.dir.cleanup()

    def test_constructor(self):
        ibvp = IBVP(self.domain, self.vform, self.bc)
        self.assertIsNotNone(ibvp.domain.mesh)
        self.assertIsNotNone(ibvp.scheme)

    def test_integrate_with_file(self):
        ibvp = IBVP(self.domain, self.vform, self.bc)
        ibvp.info()
        sol_0 = create_expression('exp(-a*pow(x, 2) - a*pow(y, 2))',
                                  degree=2, a=5)
        ibvp.set_initial_solution(sol_0)
        ibvp.integrate(0, 0.4, 0.02, self.dir.name+'/solution.xdmf')

    def test_integrate_without_file(self):
        ibvp = IBVP(self.domain, self.vform, self.bc)
        sol_0 = create_expression('exp(-a*pow(x, 2) - a*pow(y, 2))',
                                  degree=2, a=5)
        ibvp.set_initial_solution(sol_0)
        ibvp.integrate(0, 0.4, 0.02)

    def test_integrate_explicit(self):
        ibvp = IBVP(self.domain, self.vform, self.bc, scheme=ExplicitEuler)
        sol_0 = create_expression('exp(-a*pow(x, 2) - a*pow(y, 2))',
                                  degree=2, a=5)
        ibvp.set_initial_solution(sol_0)
        ibvp.integrate(0, 0.2, 1e-3)
