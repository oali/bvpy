import unittest

# -- test class

from bvpy.domains import (Rectangle, Disk, Sphere, HemiSphere, Ellipsoid,
                          Torus, CustomPolygonalDomain, Cylinder, Cube)
import fenics as fe

class TestPrimitive(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        pass

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_rectangle(self):
        domain = Rectangle(cell_size=0.1, clear=True)
        domain.discretize()
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 0].max(), 1)

        domain._details

    def test_hemisphere(self):
        domain = HemiSphere(cell_size=0.1, clear=True)
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 2].min(), 0)

    def test_disk(self):
        domain = Disk(cell_size=0.05, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 1].min(), -1, delta=1e-3)

    def test_sphere(self):
        domain = Sphere(cell_size=0.05, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 1].max(), 1, delta=1e-3)

    def test_ellipsoid(self):
        domain = Ellipsoid(cell_size=0.05, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 0].min(), -1, delta=1e-3)

    def test_torus(self):
        domain = Torus(cell_size=0.1, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)
        self.assertAlmostEqual(domain.mesh.coordinates()[:, 0].min(), -3, delta=1e-3)

    def test_cylinder(self):
        domain = Cylinder(open=True, clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)

    def test_cube(self):
        domain = Cube(clear=True)
        domain.discretize()
        self.assertIsNotNone(domain.mesh)

    def test_csg_sub(self):
        d1 = Rectangle(x=0, y=0, length=1, width=1, clear=True)
        d2 = Rectangle(x=0.5, y=0, length=0.5, width=1)
        d = d1 - d2
        d.discretize()
        self.assertEqual(d.mesh.coordinates()[:, 0].max(), 0.5)

    def test_csg_add(self):
        d1 = Rectangle(x=0, y=0, length=0.5, width=1, clear=True)
        d2 = Rectangle(x=0.5, y=0, length=0.5, width=1)
        d = d1 + d2
        d.discretize()
        self.assertEqual(d.mesh.coordinates()[:, 0].max(), 1)

    def test_csg_intersect(self):
        d1 = Rectangle(x=0, y=0, length=1.5, width=1, clear=True)
        d2 = Rectangle(x=1, y=0, length=1, width=1)
        d = d1 & d2
        d.discretize()
        self.assertEqual(d.mesh.coordinates()[:, 0].min(), 1)

    def test_primitive_custompolygonal(self):
        p = [[0, 0, 0],
             [1, 0, 0],
             [1, 1, 0],
             [0, 1, 0],
             [2, 0.5, 0]]
        c = [[0, 1, 2, 3],
             [1, 4, 2]]

        domain = CustomPolygonalDomain(p, c, cell_size=0.05, isoriented=True, clear=True)
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 0].max(), 2)

    def test_orient_custompolygonal(self):
        p = [[0, 0, 0],
             [1, 0, 0],
             [1, 1, 0],
             [0, 1, 0],
             [2, 0.5, 0]]
        c = [[0, 3, 2, 1],
             [1, 2, 4]]

        domain = CustomPolygonalDomain(p, c, cell_size=0.05, isoriented=False, clear=True)
        domain.discretize()
        self.assertEqual(domain.mesh.coordinates()[:, 0].max(), 2)

    def test_markers_custompolygonal(self):
        p = [[0, 0, 0],
             [1, 0, 0],
             [1, 1, 0],
             [0, 1, 0],
             [2, 0.5, 0]]
        c = [[0, 3, 2, 1],
             [1, 2, 4]]

        domain = CustomPolygonalDomain(p, c, markers=[0,1], cell_size=0.05, isoriented=False, clear=True)
        domain.discretize()
        ind=10
        label = domain.cdata.array()[ind]
        p = fe.Cell(domain.mesh, ind).midpoint().array()
        if p[0]<1:
            self.assertEqual(label, 0)
        else:
            self.assertEqual(label, 1)


    def test_domain_repr_str(self):
        dom = Rectangle(verbosity=True)
        self.assertEqual(dom.__repr__()[1:10], 'Rectangle')
        self.assertEqual(dom.__str__()[:22], 'Mesh not generate yet.')

    def test_discretize_square_cube(self):
        sqr = Rectangle(cell_type='line')
        sqr.discretize()
        sqr.info()

    def test_sphere_sub(self):
        big = Sphere(radius=10, cell_type='tetra')
        sml = Sphere(radius=9, cell_type='tetra')
        shl = big - sml
        shl.geometry()
        shl.discretize()

    def test_sphere_add(self):
        sph = Sphere(radius=2, cell_type='tetra')
        sph_2 = Sphere(radius=2, center=[0, 0, 2], cell_type='tetra')
        intsec = sph + sph_2
        intsec.discretize()

    def test_sphere_intersec(self):
        sph = Sphere(radius=2, cell_type='tetra')
        sph_2 = Sphere(radius=2, center=[0, 0, 2], cell_type='tetra')
        intsec = sph & sph_2
        intsec.discretize()
