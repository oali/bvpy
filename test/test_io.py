import unittest
import tempfile

from fenics import UnitSquareMesh
from bvpy import BVP
from bvpy.domains.primitives import Rectangle
from bvpy.vforms.poisson import PoissonForm
from bvpy.boundary_conditions.dirichlet import dirichlet

# -- test class

from bvpy.utils.io import *


class TestIO(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.tmp = tempfile.TemporaryDirectory()

    def tearDown(self):
        """Concludes and closes the test.
        """
        self.tmp.cleanup()

    def test_read_polygonal_data_ply(self):
        path = "data/test_small.ply"
        points, cells, _ = read_polygonal_data(path)

        self.assertEqual(len(points), 10)
        self.assertEqual(len(cells), 1)

    def test_read_polygonal_data_obj(self):
        path = "data/simple_obj_example.obj"
        points, cells, _ = read_polygonal_data(path)

        self.assertEqual(len(points), 6)
        self.assertEqual(len(cells), 2)

    def test_read_polygonal_data_txt(self):
        path = "data/tissue_example.txt"
        points, cells, _ = read_polygonal_data(path)
        self.assertEqual(len(points), 220)
        self.assertEqual(len(cells), 61)

    def test_read_polygonal_data_txt_2(self):
        path = "data/tissue_example_2.txt"
        points, cells, label = read_polygonal_data(path)
        self.assertEqual(label[0], 0)
        self.assertEqual(label[-1], 60)
        self.assertEqual(len(label), 61)

    def test_read_polygonal_data_wrong_type(self):
        path = "data/test_recording_mesh.xdmf"
        read_polygonal_data(path)

    def test_write_ply_mesh(self):
        mesh = UnitSquareMesh(2, 2)
        path = self.tmp.name+'/mesh'
        write_ply(mesh, path)

    def test_write_ply_domain(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/domain.ply'
        write_ply(domain, path)

    def test_write_ply_wrong_type(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/domain.ply'
        write_ply(domain.cdata, path)

    def test_numerize(self):
        self.assertEqual(numerize('3'), 3)
        self.assertEqual(numerize('3.4'), 3.4)
        self.assertEqual(numerize('three'), 'three')

    def test_save_domain(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/save_test.xdmf'
        save(domain, path)

    def test_save_cdata(self):
        domain = Rectangle()
        domain.discretize()
        path = self.tmp.name+'/save_test.xdmf'
        save(domain.cdata, path)

    def test_save_mesh(self):
        domain = Rectangle()
        path = self.tmp.name+'/save_test.xdmf'
        save(domain.mesh, path)

    def test_save_problem(self):
        prblm = BVP(domain=Rectangle(),
                    vform=PoissonForm(),
                    bc=dirichlet(0, 'all'))
        path = self.tmp.name+'/save_test'
        save(prblm, path)

    def test_save_solution(self):
        prblm = BVP(domain=Rectangle(),
                    vform=PoissonForm(),
                    bc=dirichlet(0, 'all'))
        path = self.tmp.name+'/save_test.xdmf'
        save(prblm.solution, path)
