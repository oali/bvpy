import unittest
import shutil
# -- test class

from bvpy.utils.visu import (_cone_plot, _wireframe_plot_mesh,
                             _surface_plot_function, _surface_plot_mesh,
                             _plot_dofs, _plot_dirichlet_bc,
                             _surface_plot_meshfunc, plot, set_renderer)
from bvpy.domains import Rectangle
import fenics as fe

set_renderer('iframe')


class TestVisu(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        self.mesh = fe.UnitSquareMesh(2, 2)
        self.V = fe.FunctionSpace(self.mesh, 'P', 1)

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_cone(self):
        pos = [[0, 0, 0],
               [1, 0, 0]]
        val = [[0, 0, 0.1],
               [0, 0, 0.1]]
        val_2D = [[0, 0.1],
                  [0, 0.1]]
        self.assertIsNotNone(_cone_plot(pos, val))
        self.assertIsNotNone(_cone_plot(pos, val_2D))

    def test_wireframe_mesh(self):
        self.assertIsNotNone(_wireframe_plot_mesh(self.mesh))

    def test_surface_plot_function(self):
        f = fe.project(fe.Constant(0), self.V)
        self.assertIsNotNone(_surface_plot_function(f, "inferno"))

    def test_surface_mesh(self):
        self.assertIsNotNone(_surface_plot_mesh(self.mesh, 'red', opacity=0.1))

    def test_dofs(self):
        self.assertIsNotNone(_plot_dofs(self.V, 10))

    def test_dirichlet(self):
        bc = fe.DirichletBC(self.V, fe.Constant(0), "on_boundary")
        self.assertIsNotNone(_plot_dirichlet_bc(bc, 10))

    def test_meshfunc(self):
        mf = fe.MeshFunction("size_t", self.mesh, 2)
        self.assertIsNotNone(_surface_plot_meshfunc(mf, "inferno"))

    def test_surface_plot_function(self):
        f = fe.project(fe.Constant(0), self.V)
        plot(f)
        shutil.rmtree('iframe_figures')

    def test_plot_dirichlet(self):
        bc = fe.DirichletBC(self.V, fe.Constant(0), "on_boundary")
        plot(bc)
        shutil.rmtree('iframe_figures')

    def test_plot_cone(self):
        f = fe.project(fe.Constant([0,0,0.1]), fe.VectorFunctionSpace(self.mesh, 'P',1, dim=3))
        plot(f)
        plot(f, norm=True)
        shutil.rmtree('iframe_figures')

    def test_plot_surface_mesh(self):
        plot(self.mesh)
        shutil.rmtree('iframe_figures')

    def test_plot_domain(self):
        rec = Rectangle()
        plot(rec)
        shutil.rmtree('iframe_figures')

    def test_plot_meshfunc(self):
        mf = fe.MeshFunction("size_t", self.mesh, 2)
        plot(mf)
        shutil.rmtree('iframe_figures')

    def test_plot_dofs(self):
        plot(self.V, size_frame=[300,300])
        shutil.rmtree('iframe_figures')
