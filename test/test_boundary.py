import unittest
from bvpy.boundary_conditions.boundary import Boundary
import fenics as fe


class TestBoundary(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_and(self):
        b1 = Boundary("near(x, 0)")
        b2 = Boundary("near(y, 0)")
        b3 = b1 & b2
        self.assertEqual(b3._string, "near(x, 0) & near(y, 0)")

    def test_or(self):
        b1 = Boundary("near(x, 0)")
        b2 = Boundary("near(y, 0)")
        b3 = b1 | b2
        self.assertEqual(b3._string, "near(x, 0) | near(y, 0)")

    def test_mark(self):
        b1 = Boundary("near(x, 0)")
        mesh = fe.UnitSquareMesh(2, 2)
        mf = fe.MeshFunction("size_t", mesh, 0, 0)
        b1.markMeshFunction(1, meshfunc=mf)
        self.assertEqual(mf[0], 1)
        self.assertEqual(mf[3], 1)
        self.assertEqual(mf[6], 1)
        mf2 = b1.markMeshFunction(1, type="size_t", mesh=mesh)
        self.assertEqual(mf2[0], 1)
        self.assertEqual(mf2[3], 1)
        self.assertEqual(mf2[6], 1)
