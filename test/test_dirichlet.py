import unittest
from bvpy.boundary_conditions.dirichlet import NormalDirichlet, ZeroDirichlet
from bvpy.boundary_conditions import Boundary
import fenics as fe

from numpy.testing import assert_array_equal


class TestDirichlet(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_normal(self):
        mesh = fe.UnitSquareMesh(2, 2)
        V = fe.VectorFunctionSpace(mesh, 'P', 1)
        diri = NormalDirichlet(boundary='all')
        diri = NormalDirichlet(boundary=Boundary('all'))
        self.assertEqual("<NormalDirichlet object, location: all, value: 1>",
                         diri.__repr__())

        d = diri.apply(V)
        self.assertEqual(d.get_boundary_values()[5], 1)

    def test_zero(self):
        b = Boundary("all")
        diri = ZeroDirichlet(b, shape=3)
        assert_array_equal(diri._val, [0, 0, 0])
