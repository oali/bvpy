import unittest

# -- test class

from bvpy.vforms import HelmholtzForm
from bvpy.domains import Rectangle
from bvpy.utils.pre_processing import create_expression
from bvpy.boundary_conditions import dirichlet, neumann
from bvpy import BVP

from bvpy import logger


class TestHelmholtz(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_bvp_creator(self):
        rect = Rectangle(cell_size=0.1)
        hlmz = HelmholtzForm(linear_coef=50**2)
        hlmz.add_point_source([.5, .5, 0.], 1.)
        diri = dirichlet(0, boundary='all')
        prblm = BVP(domain=rect, vform=hlmz, bc=diri)
        prblm.solve()
