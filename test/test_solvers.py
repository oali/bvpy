# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest
from bvpy import logger
from bvpy.solvers.linear import LinearSolver
from bvpy.solvers.nonlinear import NonLinearSolver
# from bvpy.solvers.parameter import SolverParameter
import fenics as fe

# -- test class


class TestLinearSolver(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        mesh = fe.UnitSquareMesh(8, 8)
        V = fe.FunctionSpace(mesh, 'P', 1)

        # Define boundary condition
        self.u_D = fe.Expression('1 + x[0]*x[0] + 2*x[1]*x[1]', degree=2)

        def boundary(x, on_boundary):
            return on_boundary

        self.bc = [fe.DirichletBC(V, self.u_D, boundary)]

        # Define variational problem
        u = fe.TrialFunction(V)
        v = fe.TestFunction(V)
        f = fe.Constant(-6.0)
        self.a = fe.dot(fe.grad(u), fe.grad(v))*fe.dx
        self.L = f*v*fe.dx

        # Compute solution
        self.u = fe.Function(V)

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_LU(self):
        # SolverParameter.linear['linear_solver'] = 'lu'
        solver = LinearSolver()
        solver.parameters['linear_solver'] = 'lu'
        solver.parameters['krylov_solver']['monitor_convergence'] = False
        solver.set_problem(self.u, self.a, self.L, self.bc, [])
        solver.solve()
        error_L2 = fe.errornorm(self.u_D, self.u, 'L2')
        logger.debug(error_L2)
        assert(error_L2 < 1e-2)

        with self.assertRaises(AttributeError):
            solver.parameters['fake_parameter']

    def test_krylov(self):
        solver = LinearSolver()
        solver.parameters['linear_solver'] = 'gmres'
        solver.parameters['krylov_solver']['monitor_convergence'] = False
        solver.parameters['krylov_solver']['absolute_tolerance'] = 1e-10
        solver.parameters['krylov_solver']['relative_tolerance'] = 1e-10
        solver.set_problem(self.u, self.a, self.L, self.bc, [])
        solver.solve()
        error_L2 = fe.errornorm(self.u_D, self.u, 'L2')
        logger.debug(error_L2)
        assert(error_L2 < 1e-2)


class TestNonLinearSolver(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        def q(u):
            "Return nonlinear coefficient"
            return 1 + u**2

        # Use SymPy to compute f from the manufactured solution u
        import sympy as sym
        x, y = sym.symbols('x[0], x[1]')
        u = 1 + x + 2*y
        f = - sym.diff(q(u)*sym.diff(u, x), x)\
            - sym.diff(q(u)*sym.diff(u, y), y)
        f = sym.simplify(f)
        u_code = sym.printing.ccode(u)
        f_code = sym.printing.ccode(f)
        print('u =', u_code)
        print('f =', f_code)

        # Create mesh and define function space
        mesh = fe.UnitSquareMesh(8, 8)
        V = fe.FunctionSpace(mesh, 'P', 1)

        # Define boundary condition
        u_D = fe.Expression(u_code, degree=2)

        def boundary(x, on_boundary):
            return on_boundary

        self.bc = fe.DirichletBC(V, u_D, boundary)

        # Define point pointSource
        self.ps = fe.PointSource(V, fe.Point([0, 0, 0]), 0)

        # Define variational problem
        self.u = fe.Function(V)  # Note: not TrialFunction!
        v = fe.TestFunction(V)
        trial = fe.TrialFunction(V)
        f = fe.Expression(f_code, degree=2)
        self.F = q(self.u)*fe.dot(fe.grad(self.u), fe.grad(v))*fe.dx\
            - f*v*fe.dx
        # self.F1 = fe.derivative(self.F, self.u, v)
        self.J = fe.derivative(self.F, self.u, trial)

        self.u_e = fe.interpolate(u_D, V)

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_GMRES(self):
        solver = NonLinearSolver()
        solver.parameters['krylov_solver']['monitor_convergence'] = False
        solver.parameters['linear_solver'] = 'gmres'
        solver.set_problem(self.u, self.F, self.J, [self.bc], [self.ps])
        solver.solve()
        error_L2 = fe.errornorm(self.u_e, self.u, 'L2')
        assert(error_L2 < 1e-10), error_L2
