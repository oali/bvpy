import unittest

# -- test class

from bvpy.vforms import PoissonForm
from bvpy.domains import Rectangle
from bvpy.utils.pre_processing import create_expression
from bvpy.boundary_conditions import dirichlet, neumann
from bvpy import BVP

from bvpy import logger


class TestPoisson(unittest.TestCase):
    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_init(self):
        poisson = PoissonForm(source=-6)
        print(poisson._details)
        poisson = PoissonForm(source=create_expression('x*y', degree=2))
        print(poisson._details)
        self.assertEqual(poisson.degree, 1)

    def test_set_element_family(self):
        poisson = PoissonForm(source=-6)
        poisson.set_element_family('DG')
        self.assertEqual(poisson._elements[0].family, "DG")

    def test_bvp_creator(self):
        rectangle = Rectangle(cell_size=0.1, clear=True)
        poisson = PoissonForm(source=-6)
        diri = dirichlet('1 + x*x + 2*y*y', boundary='all')
        bvp = BVP(domain=rectangle, vform=poisson, bc=diri)

    def test_bvp_solve(self):
        rectangle = Rectangle(cell_size=0.1, clear=True)
        source = create_expression('x*x', degree=2)
        poisson = PoissonForm(source=source)
        poisson.add_point_source([0.5, 0.5, 0], 1)
        diri = dirichlet('1 + x*x + 2*y*y', boundary='all')
        BVP(domain=rectangle, vform=poisson, bc=diri).solve()

    def test_neumann(self):
        rectangle = Rectangle(cell_size=0.1, clear=True)
        poisson = PoissonForm(source=-6)
        neum = neumann(100, 'near(y,0)')
        diri = dirichlet('1 + x*x + 2*y*y', boundary='near(x,0)')
        BVP(domain=rectangle, vform=poisson, bc=[neum, diri]).solve()
