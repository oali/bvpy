import unittest

import numpy as np
import fenics as fe

from bvpy.domains import Disk


# -- module to test
from bvpy.domains.geometry import *


class TestGeometry(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        pass

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_boundary_normal(self):
        disk = Disk(cell_size=0.05, clear=True)
        disk.discretize()
        bnd_nmrl = boundary_normal(disk)
        np.testing.assert_allclose(bnd_nmrl(1, 0, 0),
                                   np.array([1, 0, 0]), atol=1e-15)

    def test_normal(self):
        disk = Disk(cell_size=0.05, clear=True)
        disk.discretize()
        n = normal(disk, interior=[0, 0, -1])
        np.testing.assert_allclose(n(1, 0, 0),
                                   np.array([0, 0, 1]), atol=1e-15)

    def test_init_orientation_3D(self):
        mesh = fe.UnitSquareMesh(1, 1)
        orient = Init_orientation_3D(mesh)

    def test_get_boundary_vertex(self):
        ref_pts = [[0, 0], [1, 0], [0, 1], [1, 1]]
        mesh = fe.UnitSquareMesh(1, 1)
        bnd_vtx = get_boundary_vertex(mesh)
        for pts in [list(vtx.point())[:2] for vtx in bnd_vtx]:
            self.assertIn(pts, ref_pts)
