import unittest

from bvpy import BVP, logger
from bvpy.domains import CustomDomain
from bvpy.vforms import LinearElasticForm, HyperElasticForm
from bvpy.boundary_conditions import dirichlet
import fenics as fe


class TestLinearElasticityProblem(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        logger.setLevel(50)
        self.L = 10
        self.W = 1

        mesh = fe.RectangleMesh(fe.Point(0, 0), fe.Point(self.L, self.W),
                                100, 10, "crossed")
        self.domain = CustomDomain(mesh.coordinates(), mesh.cells(), dim=2)

        self.rho_g = 1e-2
        young = 1e5
        self.theo_def_body = 3 * self.rho_g * self.L**4 / 2 / young / self.W**3

    def tearDown(self):
        """Concludes and closes the test.
        """
        pass

    def test_linear(self):
        vform = LinearElasticForm(source=[0, -self.rho_g],
                                  young=1e2,
                                  poisson=.3,
                                  plane_stress=False)
        vform.plane_stress = True
        self.assertEqual(vform._plane_stress, True)

        vform.young = 1e5
        self.assertEqual(vform._parameters['young'], 1e5)

        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve()

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)

    def test_hyper(self):
        vform = HyperElasticForm(source=[0, -self.rho_g],
                                 young=1e5,
                                 poisson=.3,
                                 plane_stress=True)
        print(vform._parameters)
        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(linear_solver='cg')

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)

    def test_hyper_neo_hookean(self):
        vform = HyperElasticForm(source=[0, -self.rho_g],
                                 young=1e5,
                                 poisson=.3,
                                 material_model='neo_hookean',
                                 plane_stress=True)
        diri = dirichlet([0, 0], boundary='near(x, 0)')
        bvp = BVP(self.domain, vform, diri)
        bvp.solve(linear_solver='cg', absolute_tolerance=1e-7)

        comp_def = bvp.solution(self.L, self.W / 2)[1]
        err_rel = abs(self.theo_def_body + comp_def) / self.theo_def_body
        self.assertLess(err_rel, 1e-3)
